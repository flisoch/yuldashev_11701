import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;

public class UnitTests {

    Polinom3 p1 = new Polinom3("res/pol1");

    public UnitTests() throws FileNotFoundException {
    }


    @Test
    public void toStringTest() {
        String s = "x^8y^2z^2 + x^2y^4 + 3xy^4z^2 + 2x^-3yz^4";
        Assert.assertEquals(s,p1.toString());
    }

    @Test
    public void insertTest() throws FileNotFoundException {
        String s = p1.toString();
        p1.insert(1,-6,1,1);
        s += " + x^-6yz";
        Assert.assertEquals(s,p1.toString());
    }

    @Test
    public void deleteTest() {
        p1.delete(1,1,1);       //deleting nonexistent monom
        String s = "x^8y^2z^2 + x^2y^4 + 3xy^4z^2 + 2x^-3yz^4";
        Assert.assertEquals(s,p1.toString());

        p1.delete(8,2,2);
        s = "x^2y^4 + 3xy^4z^2 + 2x^-3yz^4";
        Assert.assertEquals(s,p1.toString());
    }

    @Test
    public void valueTest() {
        int testValue = p1.value(1,1,1);
        int trueValue = 7;
        System.out.println(p1.toString());
        Assert.assertEquals(trueValue, testValue);
    }

    @Test
    public void derivateTest() {
        String s = "2x^8yz^2 + 4x^2y^3 + 12xy^3z^2 + 2x^-3z^4";
        p1.derivate(1);
        String testS = p1.toString();
        Assert.assertEquals(s,testS);
    }

    @Test
    public void addTest() throws FileNotFoundException {
        Polinom3 p2 = new Polinom3("res/pol2");
        System.out.println("p2:     " + p2);
        System.out.println("p1      " + p1);
        p2.add(p1);
        String testS = "x^8y^2z^2 + 8x^7yz^3 + 2x^3y^4z^4 + 4x^3yz + x^2y^4 + 3xy^4z^2 + 2x^-3yz^4 + 5x^-5y^5z^2";
        Assert.assertEquals(testS, p2.toString());
    }
}
