import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;

public class Polinom3 {

    private Node head = null;

    Polinom3(String filename) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(filename));


        while (sc.hasNext()) {

            int coef = sc.nextInt();
            int degX = sc.nextInt();
            int degY = sc.nextInt();
            int degZ = sc.nextInt();

            this.insert(coef, degX, degY, degZ);

        }
    }

    public void insert(int coef, int degX, int degY, int degZ) {
        Summand s = new Summand(coef, degX, degY, degZ);
        Node n = new Node(s);
        Node cursorNode = head;
        if (head == null) {
            head = n;
        }
        else {
             while (cursorNode.next != null && compare(n.value, cursorNode.value) < 0) {
                 cursorNode = cursorNode.next;
             }

            int cmpValue = compare(n.value,cursorNode.value);
            if(cmpValue == 0) {            //если мономы совпали,то заменить. а если только степени, но не коэффициенты совпали?
                cursorNode.value.coef = coef;
            }
            else {
                if (cursorNode == head) {
                    if (cmpValue < 0) {
                        n.next = head.next;
                        head.next = n;

                    }
                    else {
                        Summand value = head.value;
                        head.value = n.value;
                        n.value = value;
                        n.next = head.next;
                        head.next = n;
                    }

                } else {
                    if (cmpValue < 0) {
                        n.next = cursorNode.next;
                        cursorNode.next = n;
                    }
                    else {
                        Summand value = cursorNode.value;
                        cursorNode.value = n.value;
                        n.value = value;
                        n.next = cursorNode.next;
                        cursorNode.next = n;
                    }
                }
            }
        }
    }
    private int compare(Summand a, Summand b) {
        if (a.degs[0] == b.degs[0]) {

            if (a.degs[1] == b.degs[1]) {
                return a.degs[2] - b.degs[2];
            }
            //else:
            return a.degs[1] - b.degs[1];
        }
        //else:
        return a.degs[0] - b.degs[0];
    }

    int value(int i, int i1, int i2) {
        int sum = 0;
        Node cursor = head;

        while (cursor != null) {
            sum += cursor.value.summandValue(i,i1,i2);
            cursor = cursor.next;
        }

        return sum;
    }


    @Override
    public String toString() {

        StringBuilder polinom = new StringBuilder();
        Node cursor = this.head;
        while (cursor != null) {
            polinom.append(cursor.value).append(" + ");
            cursor = cursor.next;
        }
        try {
            polinom = polinom.delete(polinom.length()-3,polinom.length());
        }catch (java.lang.StringIndexOutOfBoundsException e) {
            System.out.println("");
        }
        return polinom.toString();
    }

    void delete(int deg1, int deg2, int deg3) {

        Node n = head;
        while(n.next != null) {
            if(nDegsEqualDegs(n,deg1,deg2,deg3)) {

                Summand value = n.value;
                n.value = n.next.value;
                n.next.value = value;
                n.next = n.next.next;
                break;
            }
            n = n.next;
        }
    }

    private boolean nDegsEqualDegs(Node n, int deg1, int deg2, int deg3) {
        return(
                n.value.degs[0] == deg1 &&
                n.value.degs[1] == deg2 &&
                n.value.degs[2] == deg3
                );
    }

    void derivate(int i) {
        Node cursor = head;
        while(cursor != null) {

            cursor.value.coef *= cursor.value.degs[i];
            cursor.value.degs[i]--;
            cursor = cursor.next;
        }
    }

    void add(Polinom3 p2) {
        Node cursor = p2.head;
        while (cursor!= null) {
            this.insert(cursor.value.coef, cursor.value.degs[0],
                    cursor.value.degs[1],
                    cursor.value.degs[2]);
            cursor = cursor.next;
        }
    }


}
