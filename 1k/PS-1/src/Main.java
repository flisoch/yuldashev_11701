import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Polinom3 p1 = new Polinom3("res/pol1");
        Polinom3 p2 = new Polinom3("res/pol2");

        System.out.println(
                "first polinom(p1):  " + p1 + "\n" + "second polinom(p2): " + p2 + "\n"
        );

        System.out.println("p2 at point(1,1,2):\n " + p2.value(1,1,2) + "\n");

        p1.delete(2,4,0);
        System.out.println("p1 after deleting monome with certain (here: 2,4,0) degs:\n " + p1 + "\n");
        p1.delete(200,400,0);
        System.out.println("p1 after deleting monome that polinom doesn't contain:\n " + p1 + "\n");

        p1.add(p2);
        System.out.println("p1 + p2:\n " + p1 + "\n");

        p2.derivate(2);
        System.out.println("p2 after derivating by certain (here: 'z') variable:\n " + p2);

    }
}
