
public class Summand {

    //object of this class is Monom of Polinom, beyond the "Node/Elem" shell

    int coef = 0;
    int degs[] = new int[3];
    char vars[] = {'x','y','z'};

    Summand(int coef, int degX, int degY, int degZ) {

        this.coef = coef;
        degs[0] = degX;
        degs[1] = degY;
        degs[2] = degZ;
    }

    public int summandValue(int x, int y, int z) {
        int sum = 0;

        sum += pow(x, degs[0]) + pow(y, degs[1]) + pow(z, degs[2]);

        return sum;
    }
    public static int pow(int n, int deg) {
        int s = 1;

        for (int i = 0;i < deg;i++) {
            s *= n;
        }

        return s;
    }

    @Override
    public String toString() {
        StringBuilder summand = new StringBuilder();
        if(coef == 0) {
            return "";
        }
        else if (Math.abs(coef) > 1) {
            summand.append(coef);
        }
        for(int i = 0;i < degs.length;i++) {
            if(degs[i]!=0) {
                summand.append(vars[i]);
                if(Math.abs(degs[i]) > 1) {
                    summand.append('^').append(degs[i]);
                }
            }
        }
        return summand.toString();
    }

}