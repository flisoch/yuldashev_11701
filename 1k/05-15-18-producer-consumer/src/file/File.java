package file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;

public class File {

    private String fileURL;
    private String filename;
    private String destination;
    private boolean used;
    private boolean ready;

    public File(String fileURL, String destination) {
        this.fileURL = fileURL;
        this.filename = getFilename(fileURL);
        this.destination = destination;
        this.used = false;
        this.ready = false;
    }

    public void download() {
        try {
            //
            System.out.println("Download started: " + filename);
            //
            URL url = new URL(fileURL);
            InputStream in = url.openStream();
            FileOutputStream out = new FileOutputStream(new java.io.File(destination + filename));
            byte[] buffer = new byte[4096];
            int data = in.read(buffer);
            while (data != -1) {
                out.write(buffer, 0, data);
                data = in.read(buffer);
            }
            in.close();
            out.close();
            ready = true;
            //
            System.out.println("Download complete: " + filename);
            //
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void copy() {
        try {
            //
            System.out.println("Copying started: " + filename);
            //
            used = true;
            InputStream in = new FileInputStream(destination + filename);
            FileOutputStream out = new FileOutputStream(new java.io.File(destination + "(copy)" + filename));
            byte[] buffer = new byte[4096];
            int data = in.read(buffer);
            while (data != -1) {
                out.write(buffer, 0, data);
                data = in.read(buffer);
            }
            in.close();
            out.close();
            used = false;
            //
            System.out.println("Copying complete: " + filename);
            //
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getFilename(String fileURL) {
        int lastSlash = 0;
        char c;
        for (int i = 0; i < fileURL.length(); i++) {
            c = fileURL.charAt(i);
            if (c == '/') {
                lastSlash = i + 1;
            }
        }

        return fileURL.substring(lastSlash);
    }

    public boolean isUsed() {
        return used;
    }

    public boolean isReady() {
        return ready;
    }
}
