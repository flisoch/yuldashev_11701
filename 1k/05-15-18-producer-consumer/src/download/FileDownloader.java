package download;

import file.File;

import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileDownloader {

    private String link;
    private String extension;
    private String destination;
    private final static String URL_REGEX = "(?:(?:https?|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?!10(?:\\.\\d{1,3}){3})(?!127(?:\\.\\d{1,3}){3})(?!169\\.254(?:\\.\\d{1,3}){2})(?!192\\.168(?:\\.\\d{1,3}){2})(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-z\\x{00a1}-\\x{ffff}0-9]+)(?:\\.(?:[a-z\\x{00a1}-\\x{ffff}0-9]+-?)*[a-z\\x{00a1}-\\x{ffff}0-9]+)*(?:\\.(?:[a-z\\x{00a1}-\\x{ffff}]{2,})))(?::\\d{2,5})?(?:/[^\\s]*)\\.";

    public FileDownloader(String link, String extension, String destination) {
        this.link = link;
        this.extension = extension;
        this.destination = destination;
    }

    public void start() {
        try {
            URL url = new URL(link);
            LineNumberReader lnr = new LineNumberReader(new InputStreamReader(url.openStream()));
            //
            System.out.println("Connected");
            //

            String regex = URL_REGEX + extension;
            Pattern p = Pattern.compile(regex);
            Matcher m;
            String line = lnr.readLine();
            while (line != null) {
                m = p.matcher(line);
                while (m.find()) {
                    File file = new File(m.group(), destination);
                    Downloader producer = new Downloader(file);
                    producer.start();
                    Copier consumer = new Copier(file);
                    consumer.start();
                }
                line = lnr.readLine();
            }
            lnr.close();
            //
            System.out.println("Finished");
            //
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
