package download;

import file.File;

public class Copier extends Thread{

    private final File file;

    public Copier(File file) {
        this.file = file;
    }

    @Override
    public void run() {
        synchronized (file) {
            while (!file.isReady()) {
                try {
                    file.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            file.copy();
            file.notify();
        }
    }
}
