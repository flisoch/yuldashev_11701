import java.util.Scanner;

class br {
	
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int num = sc.nextInt();

		
		int k = 0;
		while (num != 0) {
			if (k > 0) {

				if (num % 2 == 1) {

					System.out.println("* ");
					k = 0;
				}

				else {
					System.out.println(" -");
					k = 0;
				}
			}
				
			else {

				if (num % 2 == 1) {
					System.out.print("* ");
				}
				
				else {
					System.out.print(" - ");
				}
			}
			k += 1;
			num = num >> 1;
		}
		

	}
}