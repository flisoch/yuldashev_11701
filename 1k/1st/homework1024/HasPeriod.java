import java.util.regex.*;

public class HasPeriod {

	public static void main(String[] args) {
		
		//
		//"-?([1-9]\\d*|0),\\d*(\(\\d+))"

		Pattern p = Pattern.compile("-?([1-9]\\d*|\\d)\\.\\d*(\\(\\d+\\))");
		Matcher m;

		for (String it : args) {
			m = p.matcher(it);
			System.out.println(m.matches());
		}
	}
}
