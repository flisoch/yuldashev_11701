public class ValidBrackets {
	
	public static void main(String[] args) {
		
		String[] strings = {"asd((qwe)((qwe))","asd((qwe)((qwe)))",")(()"};
		
		for (String s: strings) {
			
			System.out.println(s + "\n" + Checker(s) + "\n\n");
			
		}
	}

	public static boolean Checker(String s) {
		int k = 0;
		for (int i = 0; i < s.length(); i++) {

				char word = s.charAt(i);

				switch (word) {
					case '(':
						k += 1;
						break;
					case ')':
						k -= 1;
						break;
				}


				if (k < 0) {
					break;
				}
			}
			
		return k == 0;

	}
}