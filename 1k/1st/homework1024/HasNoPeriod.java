import java.util.regex.*;

public class HasNoPeriod {

	public static void main(String[] args) {
		
		Pattern p = Pattern.compile("-?([1-9]\\d*|\\d)(\\.\\d*)?");
		for (String it : args) {
			Matcher m = p.matcher(it);
			System.out.println(m.matches());

		}
	}
}