public class ValidAssign {

	public static void main(String[] args) {
		
		String[] assigns = {"a3d= as;","asd = 3", "v = -asd", "1 = fd", "qwe = qwe;" ,"qwe = qwe", "a& = 12;", " a = r a;", "asd = qwe;w"};
		String[] states = {"word", "space", "assign", "spAftAssign", "wordAfterAssign", "digitAfterAssign", "final"};

		

		
		for (String assign:assigns) {
			boolean flag = true;
			String state = "start";
			System.out.println(assign);

			for (int i = 0;i < assign.length();i++) {
				char word = assign.charAt(i);
				
				switch (state) {

					case "start":
						if (Character.isLetter(word)) {
							state = "word";
							
						}
						else {
							flag = false;
						}
						break;

					case "word":
						if (word == ' ') {
							state = "space";
							
						}
						else if (Character.isLetterOrDigit(word)) {
							
							break;
						}
						else if (word == '=') {
							state = "assign";

						}
						else {
							flag = false;
						}
						break;
					
					case "space":
						if (word == '=') {
							state = "assign";
						}
						else if (word == ' ') {
							break;
						}
						else {
							flag = false;
						}
						break;

					case "assign":
						if (Character.isLetter(word)) {
							state = "wordAfterAssign";
						}
						else if (Character.isDigit(word)) {
							state = "digitAfterAssign";
						} 
						else if (word == ' ') {
							break;
						}
						else {
							flag = false;
						}

						break;
					case "wordAfterAssign":

						if (Character.isLetterOrDigit(word)){
							
							break;
						}
						else if (word == ';') {
							state = "final";
						}
						
						else {
							flag = false;
						}
						break;

					case "digitAfterAssign":
						if (Character.isDigit(word)) {
							break;
						}
						else if (word == ';') {
							state = "final";
						}
						else {
							flag = false;
						}
						break;

					case "final":
						if (word != ' ') {
							flag = false;
						}
						break;

				}
				// System.out.println(state + " " + flag);
			}
			System.out.println(finalState(flag, state) + "\n\n");
		}

	}

	public static boolean finalState(boolean flag, String state) {
		if ((state == "final") && flag) {
			return true;
		}
		return false;
	}
}	