import java.util.regex.*;

public class CorrectPath {

	public static void main(String[] args) {

		Pattern p = Pattern.compile("(c|C)(\\\\[A-Za-z0-9]+)*\\.pdf");

		for (String it: args) {

			Matcher m = p.matcher(it);
			System.out.println(m.matches());
		}
	}
}