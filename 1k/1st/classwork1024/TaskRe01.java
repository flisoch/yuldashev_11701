
import java.util.regex.*;

public class TaskRe01{
	
	public static void main(String[] args) {
		
		Pattern p = Pattern.compile("^M[a-z]*");
		
		Matcher m;
		for (String it : args) {
			m = p.matcher(it);
			System.out.println(m.matches());
		}
	}
}
