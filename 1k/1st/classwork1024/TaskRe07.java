// Регулярное выражение для телефонного номера в формате +7-XXX-XXX-XX-XX

import java.util.regex.*;

public class TaskRe07{
	
	public static void main(String[] args) {
		
		// Pattern p = Pattern.compile("\\+7(-([0-9]{3})){2}(-([0-9]{2}))");
		// Pattern p = Pattern.compile("\\+7-[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]");
		// Pattern p = Pattern.compile("\\+7-([0-9]{3})-([0-9]{3})-([0-9]{2})-([0-9]{2})");
		Pattern p = Pattern.compile("\\+7(-([0-9]{3})){2}(-([0-9]{2})){2}");


		Matcher m;
		for (String it : args) {
			m = p.matcher(it);
			System.out.println(m.matches());
		}
	}
}
