
import java.util.regex.*;

public class TaskRe02{
	
	public static void main(String[] args) {
		
		Pattern p = Pattern.compile("(^-?[1-9]\\d*)|0");
		
		Matcher m;
		for (String it : args) {
			m = p.matcher(it);
			System.out.println(m.matches());
		}
	}
}
