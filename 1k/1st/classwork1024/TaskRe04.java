import java.util.regex.*;

public class TaskRe04 {
	
	// 4. Регулярное выражение для промежутка лет с 1432 до 2375 года

	public static void main(String[] args) {

		Pattern p = Pattern.compile("1(4(3[2-9]|[4-9]\\d)|[5-9]\\d{2})|2([0-2]\\d{2}|3([0-6]\\d|7[0-5]))");

		for (String s:args) {

			Matcher m = p.matcher(s);
			System.out.println(m.matches());
		}

	}
		

}


// Pattern p = Pattern.compile( 
// "1(4(3[2-9]|[4-9]\\d)|[5-9]\\d{2})|2([0-2]\\d{2}|3([0-6]\\d|7[0-5]))" 
// );

//4 и 6 