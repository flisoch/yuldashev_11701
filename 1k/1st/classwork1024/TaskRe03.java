
import java.util.regex.*;

public class TaskRe03{
	
	public static void main(String[] args) {
		
		Pattern p = Pattern.compile("^1[0-9]{3}");
		
		Matcher m;
		for (String it : args) {
			m = p.matcher(it);
			System.out.println(m.matches());
		}
	}
}
