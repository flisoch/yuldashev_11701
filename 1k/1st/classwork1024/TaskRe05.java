//20:15:23

import java.util.regex.*;

public class TaskRe05{
	
	public static void main(String[] args) {
		
		Pattern p = Pattern.compile("(([0-1][0-9])|2[0-3])(:[0-5][0-9]){2}");
		
		Matcher m;
		for (String it : args) {
			m = p.matcher(it);
			System.out.println(m.matches());
		}
	}
}
