/**
* @author Daler Yuldashev
* 11-701
* Problem Set 3 Task 04
*/

public class RationalFraction {
	private int num;
	private static int denom;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RationalFraction f1 = new RationalFraction(1,4);
		RationalFraction f2 = new RationalFraction(3,4);
		RationalFraction f3 = new RationalFraction(1,3);
		RationalFraction f4 = add(f1,f2);
		RationalFraction f5 = sub(f1,f2);
		RationalFraction f6 = mult(f1, f2);
		RationalFraction f7 = div(f1, f2);
		double valueF2 = f2.value();
		
//		RationalFraction f8 = new RationalFraction(1, 0);   // ����� ���������, ��� ������������� ����������
//		System.out.println(f8.value());

		System.out.println(f1.toString());
		System.out.println(f2.toString());
		System.out.println(f3.toString());
		System.out.println(f4.toString());
		System.out.println(f5.toString());
		System.out.println(f6.toString());
		System.out.println(f7.toString());
		System.out.println(valueF2);
	}
	public RationalFraction() {
		num = 0;
		denom = 1;
	}
	public static RationalFraction sqrt(RationalFraction r) {
		try {
			int denom = (int) Math.sqrt(r.getDenom());
		}
		catch(Exception e) {
			System.out.println(e);
		}
		return (new RationalFraction((int)Math.sqrt(r.getNum()), denom));
	} 
	
	public RationalFraction(int num, int denom) throws ArithmeticException {
		if (denom ==0) {
			throw new ArithmeticException("division by zero");
		}
		
		this.num = num;
		this.denom = denom;
	}
	public void reduce() {
		num = this.getNum();
		denom = this.getDenom();
		
		for (int count = Math.abs(num)+Math.abs(denom)/2
				;count>1 ; count-- ) {
			if(num%count == 0 && denom%count == 0) {
				this.setNum(num / count);
				this.setDenom(denom/count); 
			}
		}

	}

	private void setDenom(int i) {
		this.denom = i;
		
	}
	private void setNum(int i) {
		this.num = i;
		
	}
	private int getDenom() {
		
		return this.denom;
	}
	public static RationalFraction add(RationalFraction f1, RationalFraction f2) {
		int num1 = f1.getNum();
		int num2 = f2.getNum();
		int denom1 = f1.getDenom();
		int denom2 = f2.getDenom();
		RationalFraction f3 = new RationalFraction();
		if(denom1 == denom2) {
			f3.setDenom(denom1);
			f3.setNum(num1+num2);
		}
		else{
			f3.setDenom(denom1*denom2);
			f3.setNum(num1*denom2+num2*denom1);
		}
		f3.reduce();
		return f3;
	}

	public static RationalFraction sub(RationalFraction f1, RationalFraction f2) {
		int num1 = f1.getNum();
		int num2 = f2.getNum();
		int denom1 = f1.getDenom();
		int denom2 = f2.getDenom();
		RationalFraction f3 = new RationalFraction();
		if(denom1 == denom2) {
			f3.setDenom(denom1);
			f3.setNum(num1-num2);
		}
		else{
			f3.setDenom(denom1*denom2);
			f3.setNum(num1*denom2-num2*denom1);
		}
		f3.reduce();

		return f3;
	}

	private int getNum() {
		
		return this.num;
	}
	
	public static RationalFraction mult(RationalFraction f1, RationalFraction f2) {
		int num1 = f1.getNum();
		int num2 = f2.getNum();
		int denom1 = f1.getDenom();
		int denom2 = f2.getDenom();
		RationalFraction f3 = new RationalFraction(num2*num1, denom1*denom2);
		f3.reduce();

		return f3;
	}
	public static RationalFraction div(RationalFraction f1, RationalFraction f2) {
		int num1 = f1.getNum();
		int num2 = f2.getNum();
		int denom1 = f1.getDenom();
		int denom2 = f2.getDenom();
		RationalFraction f3 = new RationalFraction(num1*denom2, num2*denom1);
		f3.reduce();

		return f3;
	}

	public String toString() {
		String f = ""+this.getNum()+"/"+this.getDenom();
		return  f;
	}

	public double value() {
		double value = 1.0* this.getNum() / this.getDenom();
		return  value;
	}
	
}
