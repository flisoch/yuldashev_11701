import java.util.Scanner;

public class Game {

	

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println(" 1 - start game \n"
						 + " 0 - exit" );
		int choice = sc.nextInt();

		while (choice != 0) {

			if (choice == 1) {
				startGame();
			}
			else {
				System.out.println("Invalid command");
			}
			choice = sc.nextInt();
			System.out.println(choice);
		}

		System.out.println("Bye");

	}

	static void startGame() {

		Player p1 = new Player("Player 1");
		Player p2 = new Player("Player 2");

		while (checkEnd(p1, p2)) {

			p1.beat(p2);

			if (checkEnd(p1, p2)) {
				p2.beat(p1);
			}
		}
		System.out.println("Winner is " + winner(p1, p2));
	}


	static boolean checkEnd(Player p1, Player p2) {

		return p1.getHp() > 0 && p2.getHp() > 0 ;
	}


	static String winner(Player p1, Player p2) {

		String winner;
		if (p1.getHp() > 0) {
			winner = p1.getName();
		}
		else {
			winner = p2.getName();
		}

		return winner;

	}

}