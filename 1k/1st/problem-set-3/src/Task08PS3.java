import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

/**
* @author Daler Yuldashev
* 11-701
* Problem Set 3 Task 08
*/

public class Task08PS3  {



    public static void main(String[] args) throws FileNotFoundException{

        Scanner sc = new Scanner(new File("res/Browser_Speed.csv"));

        String[] s5 = sc.nextLine().split(",");
        String sTest = sc.nextLine();
        String[] s1 = sc.nextLine().split(",");
        DBase chrome = new DBase(s1[0], Float.parseFloat(s1[1]), s1[2],
                Float.parseFloat(s1[3]),Float.parseFloat(s1[4]),Float.parseFloat(s1[5]),
                Float.parseFloat(s1[6]),Float.parseFloat(s1[7]),Float.parseFloat(s1[8]),
                Float.parseFloat(s1[9]),Float.parseFloat(s1[10]),Float.parseFloat(s1[11]));

        String[] s2 = sc.nextLine().split(",");
        DBase internetExplorer = new DBase(s2[0], Float.parseFloat(s2[1]), s2[2],
                Float.parseFloat(s2[3]),Float.parseFloat(s2[4]),Float.parseFloat(s2[5]),
                Float.parseFloat(s2[6]),Float.parseFloat(s2[7]),Float.parseFloat(s2[8]),
                Float.parseFloat(s2[9]),Float.parseFloat(s2[10]),Float.parseFloat(s2[11]));

        String[] s3 = sc.nextLine().split(",");
        DBase firefox = new DBase(s3[0], Float.parseFloat(s3[1]), s3[2],
                Float.parseFloat(s3[3]),Float.parseFloat(s3[4]),Float.parseFloat(s3[5]),
                Float.parseFloat(s3[6]),Float.parseFloat(s3[7]),Float.parseFloat(s3[8]),
                Float.parseFloat(s3[9]),Float.parseFloat(s3[10]),Float.parseFloat(s3[11]));

        String[] s4 = sc.nextLine().split(",");
        DBase safari = new DBase(s4[0], Float.parseFloat(s4[1]), s4[2],
                Float.parseFloat(s4[3]),Float.parseFloat(s4[4]),Float.parseFloat(s4[5]),
                Float.parseFloat(s4[6]),Float.parseFloat(s4[7]),Float.parseFloat(s4[8]),
                Float.parseFloat(s4[9]),Float.parseFloat(s4[10]),Float.parseFloat(s4[11]));


        System.out.println(fastestSite(chrome, internetExplorer, firefox, safari));
        System.out.println(fastestBrowser(chrome,internetExplorer, firefox, safari));
        System.out.println(averageTime(chrome,internetExplorer,firefox, safari));
    }

    private static float averageTime(DBase chrome, DBase internetExplorer, DBase firefox, DBase safari) {

        float chromeAverTime = time(chrome) / 9;
        float erAverTime =  time(internetExplorer) /9;
        float firefoxAverTime = time(internetExplorer) / 9;
        float safariAverTime = time(safari) /9;
        return (chromeAverTime + erAverTime + firefoxAverTime + safariAverTime)/ 4;
    }

    private static String fastestBrowser(DBase chrome, DBase internetExplorer, DBase firefox, DBase safari) {
        DBase[] browsers = {chrome, internetExplorer, firefox, safari};
        float timeChrome = time(chrome);
        float timeInternetExplorer = time(internetExplorer);
        float timeFirefox = time(firefox);
        float timeSafari = time(safari);
        float[] times = { timeChrome, timeInternetExplorer, timeFirefox, timeSafari};
        float min = times[0];
        for (int i = 1; i < browsers.length;i++) {
            if (times[i] < min) {
                min = times[i];
            }
        }
        if(min == timeChrome) { return "chrome"; }
        else if(min == timeFirefox) {
            return "firefox";
        }
        else if(min == timeInternetExplorer) {
            return "internetExplorer";
        }
        else if(min == timeSafari) {
            return "safari";
        }
        return null;
    }

    private static float time(DBase browser) {
        return browser.amazon + browser.apple + browser.ebay + browser.microsoft +
                browser.myscpace + browser.pcworld + browser.wikipedia +
                browser.yahoo + browser.youtube;
    }

    public static String fastestSite(DBase chrome, DBase internetExplorer, DBase firefox, DBase safari) {
        float amazon = chrome.amazon + internetExplorer.amazon + firefox.amazon + safari.amazon;
        float apple = chrome.apple + internetExplorer.apple + firefox.apple + safari.apple;
        float ebay = chrome.ebay + internetExplorer.ebay + firefox.ebay + safari.ebay;
        float microsoft = chrome.microsoft + internetExplorer.microsoft + firefox.microsoft + safari.microsoft;
        float myscpace = chrome.myscpace + internetExplorer.myscpace + firefox.myscpace + safari.myscpace;
        float pcworld = chrome.pcworld + internetExplorer.pcworld + firefox.pcworld + safari.pcworld;
        float wikipedia = chrome.wikipedia + internetExplorer.wikipedia + firefox.wikipedia + safari.wikipedia;
        float yahoo = chrome.yahoo + internetExplorer.yahoo + firefox.yahoo + safari.yahoo;
        float youtube = chrome.youtube + internetExplorer.youtube + firefox.youtube + safari.youtube;
        float sites[] = {amazon,apple,ebay,microsoft,myscpace,pcworld,wikipedia,yahoo,youtube};
        float min = sites[0];
        for ( int i = 1;i<9;i++) {
            if(sites[i] < min) {
                min = sites[i];
            }
        }
        if(min == amazon) { return "amazon"; }
        else if(min == apple) {
            return "apple";
        }
        else if(min == ebay) {
            return "ebay";
        }
        else if(min == microsoft) {
            return "microsoft";
        }
        else if(min == myscpace) {
            return "myspace";
        }
        else if(min == pcworld) {
            return "pcworld";
        }
        else if(min == wikipedia) {
            return "wikipedia";
        }
        else if(min == yahoo) {
            return "yahoo";
        }
        else if(min == youtube) {
            return "youtube";
        }

        return null;
    }

    public static class DBase {

        String browser;
        float Average;
        String empty;
        float amazon;
        float apple;
        float ebay;
        float microsoft;
        float myscpace;
        float pcworld;
        float wikipedia;
        float yahoo;
        float youtube;

        public DBase(String browser,
                     float Average,
                     String empty,
                     float amazon,
                     float apple,
                     float ebay,
                     float microsoft,
                     float myscpace,
                     float pcworld,
                     float wikipedia,
                     float yahoo,
                     float youtube) {

            this.browser = browser;
            this.Average = Average;
            this.empty = empty;
            this.amazon = amazon;
            this.apple = apple;
            this.ebay = ebay;
            this.microsoft = microsoft;
            this.myscpace = myscpace;
            this.pcworld = pcworld;
            this.wikipedia = wikipedia;
            this.yahoo = yahoo;
            this.youtube = youtube;
        }
    }

}