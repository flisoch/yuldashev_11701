
public class RationalMatrix2x2 {

	RationalFraction[][] matrix = new RationalFraction[N][N] ;
	static int N =2;
	
	public static void main(String[] args) {
		

		RationalMatrix2x2 m1 = new RationalMatrix2x2(new RationalFraction(4,6));
		System.out.println(m1.toString() + "\n");
		RationalMatrix2x2 m2 = new RationalMatrix2x2(new RationalFraction(4, 5));	
		System.out.println(m2.toString() + "\n");
		
		RationalMatrix2x2 m3 = m1.mult(m2);
		RationalMatrix2x2 m4 = m1.add(m2);

		System.out.println(m1.det().toString() );

		System.out.println(m3.toString() + "\n"+ m4.toString());

	}	



	public RationalMatrix2x2( RationalFraction r) {
		for (int i = 0; i < N ;i++ ) {
			for (int j = 0; j < N ;j++ ) {
				matrix[i][j] = r;
				//System.out.println(matrix[i][j].toString() +" "+ r.toString());
			}
			
		}
		System.out.println("asd");

		
	}
	
	
	public RationalMatrix2x2() {
	
		for (int i = 0; i < N ;i++ ) {
			for (int j = 0; j < N ;j++ ) {
				matrix[i][j] =  new RationalFraction();
			}
			
		}
	}
	public RationalMatrix2x2 add (RationalMatrix2x2 other) {
		RationalMatrix2x2 newMatrix = new RationalMatrix2x2();
		for (int i = 0;i< N ;i++ ) {
			for (int j = 0;j<N ;j++ ) {
				RationalFraction temp = RationalFraction.add(this.matrix[i][j],other.matrix[i][j]);
				newMatrix.matrix[i][j] = temp;
				

				
			}
		}
		return newMatrix;
	}


	public RationalVector2D multVector (RationalVector2D v) {
		RationalVector2D newVector= new RationalVector2D();
		newVector.x = RationalFraction.add(
			RationalFraction.mult(this.matrix[0][0], v.x), RationalFraction.mult(this.matrix[0][1], v.y)
			);
		newVector.y = RationalFraction.add(
			RationalFraction.mult(this.matrix[1][0], v.x), RationalFraction.mult(this.matrix[1][1], v.y)
			);
		return newVector;

	}
	

	public RationalMatrix2x2 mult (RationalMatrix2x2 other) {
	
		
		RationalMatrix2x2 c = new RationalMatrix2x2();

		for (int i = 0; i < N ;i++ ) {
			for (int j = 0 ;j < N;j++ ) {

				RationalFraction temp = new RationalFraction();
				for (int m = 0;m < N;m++) {
					temp = RationalFraction.add(temp, RationalFraction.mult(this.matrix[i][m], other.matrix[m][j]));
				}
				c.matrix[i][j] = temp;
				
			}
			
		}
		return c;
		
	}
	

	
	public RationalFraction det() {
		RationalFraction det = new RationalFraction();
		det = RationalFraction.sub(RationalFraction.mult(this.matrix[0][0],this.matrix[1][1]),
		RationalFraction.mult(this.matrix[0][1],this.matrix[1][0]));
		
		return det;
	}


	
	public String toString(){
		String str = "";

		for (int i = 0;i<N ;i++ ) {
			for (int j = 0;j<N ;j++ ) {

				str += this.matrix[i][j].toString() + " ";
				
			}
			str += "\n";
		}
		return str;	
	}
}