
/**
* @author Daler Yuldashev
* 11-701
* Problem Set 3 Task 05
*/



public class Matrix2x2 {
	double[][] value = new double[2][2];
	
	public static void main (String[] args) {
		double[][] matrix1 = {
				{1,1},
				{2, 2}
			
			};
		double[][] matrix2 = {
				{1, -1},
				{2, 2}
			};
		Matrix2x2 m1 = new Matrix2x2(matrix1);
		Matrix2x2 m2 = new Matrix2x2(matrix2);
		sout(m1);
		sout(m2);
		m1.add(m2);
		sout(m1);
		m1.mult(2);
		sout(m1);
		Matrix2x2 m3 = mult(m1,m2);
		sout(m3);
		m3.transpond();
		sout(m3);
		System.out.println(m3.det());
		Vector2D v = new Vector2D(1,1);
		Vector2D v2 = multVector(m2, v);
		System.out.println(v2.toString());
		
	}
	public Matrix2x2 inverseMatrix() {
		Matrix2x2 matrix = new Matrix2x2();
		
		if (this.det() == 0) {
			
		}
		
		matrix.value[0][0] = this.value[1][1];
		matrix.value[0][1] = - this.value[1][0];
		matrix.value[1][0] = - this.value[0][1];
		matrix.value[1][1] = this.value[0][0];

		return matrix;
	}
	public static Vector2D multVector(Matrix2x2 a, Vector2D v) {
		
		
		
		double x = a.value[0][0]* v.getX() + a.value[0][1]*v.getX();
		double y = a.value[1][0]* v.getY() + a.value[1][1]*v.getY();
		Vector2D vector = new Vector2D(x,y);
		return vector;
	}
	
	public Matrix2x2 () {
		for (int i =0;i<2;i++) {
			for(int j = 0;j<2;j++) {
				this.value[i][j] = 0;
				
			}
		}
	}
	public Matrix2x2 (double num) {
		for (int i =0;i<2;i++) {
			for(int j = 0;j<2;j++) {
				this.value[i][j] = num;
				
			}
		}
	}
	public static void sout(Matrix2x2 m) {
	
		for (double[] a: m.value) {
			for (double num :a) {

				System.out.print(num + " ");
			}
			System.out.println();
		}
		System.out.println();
		
	}
	
	public Matrix2x2 (double[][] matrix) {
		for (int i =0;i<matrix.length;i++) {
			for(int j = 0;j<matrix[0].length;j++) {
				this.value[i][j] = matrix[i][j];
				
			}
		}
	}
	public void add (Matrix2x2 matrix) {
		
		for (int i =0;i<2;i++) {
			for(int j = 0;j<2;j++) {
				this.value[i][j] += matrix.value[i][j];
				
			}
		}
	}
	public void sub (Matrix2x2 matrix) {
			
		for (int i =0;i<2;i++) {
			for(int j = 0;j<2;j++) {
				this.value[i][j] -= matrix.value[i][j];
				
			}
		}
	}
	public void mult (double num) {
		
		for (int i =0;i<2;i++) {
			for(int j = 0;j<2;j++) {
				this.value[i][j] *= num;
				
			}
		}
	}
	public static Matrix2x2 mult(Matrix2x2 a, Matrix2x2 b) {
		
		int bcolumns = b.value.length;
		int acolumns = a.value[0].length;
		int blines = b.value[0].length;
		int alines = a.value.length;
		
		double[][] arr = new double[alines][bcolumns];
		Matrix2x2 c = new Matrix2x2(arr);

		for (int i = 0; i < alines ;i++ ) {
			for (int j = 0 ;j < bcolumns;j++ ) {


				int temp = 0;
				for (int m = 0;m < acolumns;m++) {
					temp += a.value[i][m] * b.value[m][j];
				}
				c.value[i][j] = temp;
				
			}
			
		}
		return c;
		
	}
	
	public double det() {
		double a = this.value[0][0]; 
		double b = this.value[0][1];
		double c = this.value[1][0];
		double d = this.value[1][1];


		
		return a*d-b*c;
	}
	
	public void transpond() {
		for(int i=0;i<2;i++) {
			for(int j =i;j<2;j++) {
				double temp = this.value[i][j];
				this.value[i][j] = this.value[j][i];
				this.value[j][i] = temp;
				
			}
		}
	}
}
