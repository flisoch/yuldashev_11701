import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
* @author Daler Yuldashev
* 11-701
* Problem Set 3 Task 06
*/



public class RationalVector2D implements Comparable<RationalVector2D> {
	RationalFraction x;
	RationalFraction y;

	public int compareTo(RationalVector2D other) {
		if(this.length() - other.length() > 0) {
			return 1;
		}
		else if(this.length() - other.length() < 0) {
			return -1;
		}
		else {
			return 0;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RationalFraction x1 = new RationalFraction(2,5);
		RationalFraction y1 = new RationalFraction(4,6);
		RationalVector2D v1 = new RationalVector2D(x1,y1);

		RationalFraction x2 = new RationalFraction(5,5);
		RationalFraction y2 = new RationalFraction(1,3);
		RationalVector2D v2 = new RationalVector2D(x2,y2);
		v1 = v1.add(v2);
		System.out.println(v1.toString());
		System.out.println(v2.toString());

		System.out.println(v1.length());
		System.out.println(scalarProduct(v1, v2));

		List<RationalVector2D> arr = new ArrayList<RationalVector2D>();
		arr.add(v1);
		arr.add(v2);
		arr.add(v1);
		System.out.println(arr);
		Collections.sort(arr);
		System.out.println(arr);


	}
	
	public RationalVector2D() {
		x = new RationalFraction();
		y = new RationalFraction();
	}
	
	public  RationalVector2D(RationalFraction x, RationalFraction y) {
		this.x = x;
		this.y = y;
	}


	public double length () {
		RationalFraction x = RationalFraction.mult(this.x, this.x);
		RationalFraction y = RationalFraction.mult(this.y, this.y);
		double length = Math.sqrt(x.value())+ Math.sqrt(y.value());
		return length;
		
	}
	public static double scalarProduct (RationalVector2D v1, RationalVector2D v2) {


		double scalarProduct=  v1.x.value()*v2.x.value() + v1.y.value()*v2.y.value();
		return scalarProduct;
		
	}


	public String toString(){
		return "("+this.x.toString()+", "+ this.y.toString()+")";
	}
	public RationalVector2D add (RationalVector2D v) {

		RationalFraction x = RationalFraction.add(this.x, v.x);
		RationalFraction y = RationalFraction.add(this.y, v.y);
		return (new RationalVector2D(x,y));
	}
}
