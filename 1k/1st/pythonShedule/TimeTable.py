from  DaysAndSubjects import DaysAndSubjects


class TimeTable(DaysAndSubjects):


    def __init__(self, name):
        super(TimeTable, self).__init__()
        self.name = name
        self.table = [['-' for sub in range(6)] for day in range(7)]

    def addd(self, day, args):

        i = 0
        try:
            for tyype in args:
                self.table[day][i] = tyype
                i += 1
        except:
            print('too many classes, i\'m tired')

    def compare(self, Object):
        builder = "Breaks in common:\n"

        for day in self.DAYS:
            builder += ' \n' + self.DAY_TO_STR[day]
            for i in range(5):
                if self.table[day][i] == Object.table[day][i] and self.table[day][i] == 'BREAK':
                    builder += ' ' + str(i+1) + ": " + self.BREAK + ''
        print(builder)

    def add(self, day, classCount, tyype):
        self.table[day][classCount] = tyype



    def copy(self, object):
        self.table = object.table


    def toString(self):

        builder = self.name +"'s " + "Schedule:\n"

        for day in self.DAYS:
            builder += self.DAY_TO_STR[day] + ":\n"

            builder += "\t"
            for i in range(5):
                builder += str(self.table[day][i]) + " "

            builder += "\n"

        print(builder)
