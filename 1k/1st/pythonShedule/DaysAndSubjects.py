
class DaysAndSubjects():

	def __init__(self):
         self.SUNDAY = 0
         self.TUESDAY = 2
         self.MONDAY = 1
         self.WEDNESDAY	= 3
         self.THURSDAY = 4
         self.FRIDAY = 5
         self.SATURDAY = 6

         self.DAYS = [0, 1, 2, 3, 4, 5, 6]

         self.DAY_TO_STR = ['SUNDAY',
                            "MONDAY",
                            "TUESDAY",
                            "WEDNESDAY",
                            "THURSDAY",
                            "FRIDAY",
                            "SATURDAY"]

         self.BREAK = "Break"
         self.LECTURE = "Lecture"
         self.CLASS = "Class"

