import java.util.Scanner;

class maxshift {
	public static double maxInArray(double[] array) {
		double max = array[0];
		for (double it: array) {
			max = it > max ? it : max;

		}
		return max;
	}

	public static void shift(double[] array) {
		// O(N)
		double head = array[0];
		for (int i = 1; i < array.length; i++) {
			double temp = array[i - 1];
			array[i - 1] = array[i];
			array[i] = temp;
		}
		array[array.length - 1] = head; 
	}

	public static void shiftby(int k, double[] array){
		//O(N * K)
		for (int i = 0; i < k; i++) {
			shift(array);

		}
	}



	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();

		double[] array = new double[n];
		for(int i = 0; i < n; i++) {
			array[i] = sc.nextDouble();
		}
		System.out.println(maxInArray(array));
		shift(array);
		for (double it: array) {
			System.out.print(it + " ");
		}

		shiftby(sc.nextInt(), array);
		for (double it: array) {
			System.out.print(it + " ");
		}
			
		

	}
}