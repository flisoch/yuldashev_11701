/**
* @author Daler Yuldashev
* 11-701
* Problem Set 2 Task 12-13
*/

package problemSet2;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

//fitnessFunction, sorting

public class Task1213PS2 {
	final static int POPULATION_LENGTH = 6; //even number
	final static int D = 10;
	final static int WORD_LENGTH = 5;
	static Random r = new Random();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] population = createPopulation(POPULATION_LENGTH);
		sout(population);

		System.out.println(hasRequiredWord(population));
		int k = 0;												//k to leave loop if it's
		while(!hasRequiredWord(population) && k< 100*WORD_LENGTH) {     //impossible to create appropriate population
			crossingover(population);
			sout(population);
			sorting(population);
			sout(population);
			k++;
		}
		if (k == 100*WORD_LENGTH) {
			System.out.println("unfortunate combination of first population");
		}
		
	}

	private static void sorting(String[] population) {
		Arrays.sort(population, FitnessIndexComparator);
		
	}
	public static Comparator<String> FitnessIndexComparator = new Comparator<String>() {

		@Override
		public int compare(String s1, String s2) {

			return (int) (fitnessFunction(s2) - fitnessFunction(s1)); // reverse order
		}

	};
	private static double fitnessFunction(String word) {
		double index = 100;
		for(int i = 1; i < WORD_LENGTH; i++) {
			if (Math.abs(word.charAt(i)-word.charAt(i - 1)) < D) {
				index -= 100 / (WORD_LENGTH - 1);
			}
		}

		return index;
	}
	
	private static boolean hasRequiredWord(String[] population) {
		for (int i = 0; i < POPULATION_LENGTH;i++) {
			if (check(population[i])) {
				return true;
			}
		}
		return false;
	}

	private static boolean check(String word) {
		for(int i = 1; i < WORD_LENGTH; i++) {
//			System.out.println(Math.abs(word.charAt(i)-word.charAt(i-1)));
			if (Math.abs(word.charAt(i)-word.charAt(i - 1)) < D) {
				return false;
			}
		}
	
		
		return true;
	}

	private static void crossingover(String[] population) {
		for (int i = 0;i < POPULATION_LENGTH; i += 2) {
			String newWord = cross(population[i],population[i+1]);
			population[POPULATION_LENGTH + i/2] = newWord;
		}
		
	}

	private static String cross(String word1, String word2) {
		String newWord = "";
		for (int i = 0; i < WORD_LENGTH;i++) {
			int k = r.nextInt(2);
			newWord += (k == 0 ? word1.charAt(i) : word2.charAt(i));
		}
		return newWord;
	}

	private static String[] createPopulation(int n) {
		String[] population = new String [n+n/2];
		for (int i = 0; i < n;i++) {
			String word = "";
			for(char j = 0;j<WORD_LENGTH;j++) {
				word += (char)(r.nextInt(26) + 'a');
			}
			population[i] = word;
		}
		return population;
	}
	
	public static void sout(String[] arr) { 
		for (String a:arr) {
			System.out.print(a + " ");
		}
		System.out.println();
	}
	
}
