/**
* @author Daler Yuldashev
* 11-701
* Problem Set 2 Task 08
*/


package problemSet2;

public class Task08PS2 {
	public static final int N = 3;

	public static void main(String[] args) {
		start(0);
	}

	public static void start(int number) {

//		
//		boolean test = number %(10*n) >=1;
//		System.out.println(test);
//		
		if ( (number/(Math.pow(10,N-1)))>=1) {
			System.out.println(number);
		} 
		else if (number==0) {
			for (int i = number+1;i<=9;i++) {
				start(i);
			}
		}
		else {
			for (int i = 0;i<=9;i++) {
				
				if (simple(number %10, i)) {
					start(number*10+i);

				}
			}
		}
	}

	public static boolean simple(int n, int m) {
		if (n ==m) {
			return false;
		}
		
		if (n > m) {
			int temp = n;
			n =m;
			m = temp;
		}
		for (int i = 2;i<=n ;i++ ) {

			if (n%i == 0 && m%i ==0) {
				return false;
			}
		}
		return true;
	}




}