/**
* @author Daler Yuldashev
* 11-701
* Problem Set 2 Task 09
*/


package problemSet2;

public class Task09PS2 {
	public static final int L = 3;
	public static final int D = 10;

	public static void main(String[] args) {
		String s = "";
		start(s);		
	}

	public static void start(String s) {

		if(s.length() == 0) {
			for (char c = 'a'; c <= 'z';c++ ) {

				start(s+c);
			}
		}
		else if (s.length() == L) {
			System.out.println(s);
		}
		else {
			for (char c = 'a';c<='z';c+=1) {
				if(Math.abs(s.charAt(s.length()-1) - c) >=D) {

					start(s+c);	
				}
			}
		}
	}
}