/**
* @author Daler Yuldashev
* 11-701
* Problem Set 2 Task 10
*/


package problemSet2;

public class Task10PS2 {
	
	public static void main(String[] args) {
		String str1 = "asd";
		String str2 = "asdq";
		System.out.println(compare(str1,str2));
	}
	public static int compare(String a, String b) {
		
		
		int k = 0;
		int alen = a.length();
		int blen = b.length();
		String min;
		int diff = alen - blen;
		if (diff == 0) {
			min = a;
		}
		else if (diff < 0) {
			min = a;
		}
		else {
			min = b;
		}
		
		while(k < min.length()) { 
			if (a.charAt(k) < b.charAt(k)) {
				
				return -1;
			}
			else if (a.charAt(k)> b.charAt(k)) {
				return 1;
			}
			k++;
		}
		if (diff ==0) {
			return 0;
		}
		else {
			if(min == a) {
				return -1;
			}
			else {
				return 1;
			}
		}
	}
}
