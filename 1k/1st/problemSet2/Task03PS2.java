/**
* @author Daler Yuldashev
* 11-701
* Problem Set 2 Task 03
*/


package problemSet2;

import java.util.regex.*;
public class Task03PS2 {
	public static void main(String[] args) {
		String[] str= { "1","11","111111","10","101","100","1010","0","00","000","01","01010","001",
				"1110", "0001010"
		};
		Pattern p = Pattern.compile(
			"1(01)*|1(01)*0|1*|0(10)*|0(10*)1|0*"

			);
		Matcher m;
		for (String s:str) {
			m = p.matcher(s);
			System.out.println(m.matches());
		}
		

	}
}
