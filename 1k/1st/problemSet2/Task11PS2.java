package problemSet2;
/**
* @author Daler Yuldashev
* 11-701
* Problem Set 2 Task 11
*/

import java.util.Arrays;
import java.util.Comparator;

public class Task11PS2 {

	public static void main(String[] args) {
		String[] words = {"asd","qwe","ggg", "gggg", "ggga"};
		
		Arrays.sort(words, WordsComparator);
		for (String word : words) {
			System.out.println(word);
		}

	}
	
	public static Comparator<String> WordsComparator = new Comparator<String>() {
		
	  @Override
	  public int compare(String s1, String s2) {
	    return (Task10PS2.compare(s1, s2));
	  }
		
	};
}
