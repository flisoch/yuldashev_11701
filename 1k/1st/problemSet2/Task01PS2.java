/**
* @author Daler Yuldashev
* 11-701
* Problem Set 2 Task 01
*/

package problemSet2;
           
import java.util.regex.*;
// MM/DD/YYYY/HH:MM
// 03(march) /06 1237 12:00 - 02(Febr) / 27 1978 21:35

public class Task01PS2 {                                   //doesn't work

	public static void main(String[] args) {
		String DD6 = "06/";
		String DD7to31 = "0[7-9]|(1|2)\\d|3(0|1)/";
		String DD1to30 = "0[1-9]|(1|2)\\d|30/";
		String DD1to31 = "0[1-9]|(1|2)\\d|3(0|1)/";
		String DD1to27 = "0[1-9]|1\\d|2[0-27]/";
		
		String MM03 = "03/";
		String MM02 = "02/";
		String MM0to12 = "(0[1-9]|1[0-2]/)";
		String MM04to12 = "0[4-9]|1[0-2]/";
		
		String YYYY1237 = "1237/";
		String YYYY1238to1977 = "1(2(3[8-9]|[4-9]\\d)|[3-8]\\d\\d|9([0-6]\\d|7[0-7]))/";
		
		
		String HHMM0000to2359 = "((0|1)\\d|2[0-3]):[0-5][0-9]";
		String HHMM0000to2135 = "((0|1)\\d|20):[0-5][0-9]|21:([0-2]\\d|3[0-5])";
		String HHMM1200to2359 = "[0-1]([0-9]|2[0-3]):[0-5][0-9]";

		Pattern p = Pattern.compile(
			"("+MM03+"("+DD6+YYYY1237+HHMM1200to2359+")|("+DD7to31+YYYY1237+HHMM0000to2359+"))|"
			+MM04to12
			);
//		Matcher m = p.matcher("11/01/1888 12:00");
		Matcher m = p.matcher("00:00");
		System.out.println(m.matches());
	}
}