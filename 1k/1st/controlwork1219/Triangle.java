package controlwork1219;

public class Triangle implements Measurable {
	private double a;
	private double b;
	private double c;
	
	public Triangle(double a,double b , double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	@Override
	public double getP() {
		
		return a+b+c;
	}

	@Override
	public double getS() {
		double p = (a+b+c)/2;
		double s = Math.sqrt(p*(p-a)*(p-b)*(p-c));
		
		return s;
	}
	
}
