package controlwork1219;

public interface Measurable {
	public double getP();
	public double getS();
}