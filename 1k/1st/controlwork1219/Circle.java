package controlwork1219;

public class Circle implements Measurable{
	
	double r;
	
	public Circle(double r) {
		this.r = r;
	
	}

	@Override
	public double getP() {
		
		return 2*Math.PI*r;
	}

	@Override
	public double getS() {
		
		return Math.PI*r*r;
	}
}
