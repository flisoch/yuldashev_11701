
/**
* @author Daler Yuldashev
* 11-701
* Control Work 2
* Variant 1
*/

package controlwork1219;

public class Main {

	public static void main(String[] args) {
		int n = 5;
		/*Circle c1 = new Circle(3);
		Circle c2 = new Circle(4);
		Circle c3 = new Circle(10);
		Circle c4 = new Circle(1);
		
		Triangle t1 = new Triangle(3,4,5);
		Triangle t2 = new Triangle(1,1,1);
		Triangle t3 = new Triangle(6,8,10);
		Triangle t4 = new Triangle(7,5,8); 
		Measurable[] figures = {t1,t2,t3,t4,c1,c2,c3,c4}; */
		
		Measurable[] figures = new Measurable[n];
		for (int i = 0; i<n/2;i++) {
			figures[i] = new Circle(i+1);	
		}
		for (int i = n/2; i<n;i++) {
			figures[i] = new Triangle(i+1,i+1,i+1);	
		}
		
		double sumP = 0;
		double sumS = 0;
		
		for(Measurable figure : figures) {
//			System.out.println(figure.getS());
//			System.out.println(figure.getP());
			sumP += figure.getP();
			sumS += figure.getS();
			
		}
		System.out.println("Total P sum: "+sumP + "\nTotal S sum: " + sumS);

		
	}

}
