/**
* @author Daler Yuldashev
* 11-701
* Control Work  Task 02
* Variant 2
*/

// x2 + a[i]! – 2^i + x*i! = 0

import java.util.Scanner;

public class Task02 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		// int n = sc.nextInt();
		int faq = 1;

		int[] a = {1, 2, 3, 14, 20, 0, 5, 13, 11, 12};
		int n = a.length;

		int k;

		for (int i = 0; i < a.length; i++) {
			if (checkFunc(a[i], i) ) {
				k++;
			}
		}

		System.out.println(check(k));

	}
	
	public static boolean checkFunc(int ai, int i) {
		
		int a = 1;
		int b = faq(i);

		int c = faq(ai) - degTwo(i);
		// f = x * x + b * x + c;
		int d = b * b - 4 * a * c;

		if (d <= 0) {
			return false;
		}

		return true;


	}

	public static int degTwo(int x) {
		int i = 2;
		degX = 1;  
		while (i <= x) {
			degX *= 2;
		}
		return degX;
	}

	public static int faq( int x) {

		int i =2;
		faqX = 1;  
		while (i <= x) {
			faqX *= i;
		}
		return faqX;
	}
	

	public static boolean checkOdd(int k) {
		boolean flag = true;
		while (k >= 0 && flag) {
			if ((k % 10) % 2 ==0) {
				flag = false;
			}
			k /= 10;
		}
		return flag;
	}
	
}