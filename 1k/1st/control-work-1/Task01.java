/**
* @author Daler Yuldashev
* 11-701
* Control Work  Task 01
* Variant 2
*/


import java.util.Scanner;

public class Task01 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int[] a = new int[n];

		// int [] a = {{5,5,5,5}, { 5, 5, 5, 5}, {}};
		// int [] a = {5,25,125,625,5,5,5,5,5,5,5,6};
		// int [] a = {1};
		// int n = a.length;

		int k = 0;
		/*
		fill array somehow
		*/
		for (int i = 0; i < n; i++) {
			if (a[i] % 5 ==0) {
				k++;
			}
		}
		System.out.println(check(k));
	}

	public static boolean check(int k) {
		boolean flag = false;
		while (k >= 0 && !flag) {
			if ((k % 10) % 2 ==0) {
				flag = true;
			}
			k /= 10;
		}
		return flag;
	}
}	
