/**
* @author Daler Yuldashev
* 11-701
* Control Work  Task 03
* Variant 2
*/
import java.util.Scanner;

public class Task03 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int[] a = new int[n];

		// int[] a = { 1, 5, 6, 2, 3, 4};
		// int n = a.length;

		int m = max(a);
		char [][] b = new char [n][m];

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				if (a[j] >= m-i) {
					b[i][j] = '0';

				}
				else {
					b[i][j] = ' ';
				}
				System.out.print(b[i][j] + " ");
			}
			System.out.println();
		}

	}

	public static int max(int[] a) {
		int m = a[0];
		for (int i = 1; i < a.length; i++) {
			if (a[i] > m) {
				m = a[i];
			}
		}
		return m;
	}


}