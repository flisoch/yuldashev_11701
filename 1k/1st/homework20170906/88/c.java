import java.util.Scanner;

class c {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int b = sc.nextInt();
		int k = 0;
		int s = 0;
		while (b > 0) {
			b /= 10;
			k++;
		}
		
		s = n % 10 * Math.pow(10, (k-1)) + Math.pow((n % 10), (k-1)) - n % 10 + n / Math.pow(10, (k - 1));
		
		System.out.print(s);
	}
}

/*
n = b = int(input())
k = 0
s = 0
while b > 0:
    b //= 10
    k += 1
s = n%10 * 10**(k-1) + n%10 ** (k-1) -n%10 + n//10**(k-1)
print(s)
*/
