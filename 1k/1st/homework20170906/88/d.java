import java.util.Scanner;

class d {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int b = sc.nextInt();
		int k = 0;
		int s = 0;

		while (b > 0) {
			b /= 10;
			k++;
		}

		System.out.print((Math.pow(10, k + n) * 10 + 1);
	}
}

/*
n = b = int(input())
k = 0
s = 0
while b > 0:
    b //= 10
    k += 1
print((10**k + n)*10 + 1)
*/

