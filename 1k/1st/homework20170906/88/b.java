import java.util.Scanner;

class b {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int b = sc.nextInt();
		int k = 0;
		int s = 0;
		while (b > 0) {
			b /= 10;
			k++;
		}
		for (int i = k - 1; i >= 0; i--) {

			s += (Math.pow(10, i) * (n % 10);			
			n /= 10;
			
		}

		System.out.print(s);
	}
}

/*
n = b = int(input())
k = 0
s = 0
while b > 0:
    b //= 10
    k += 1
for i in range(k-1, -1, -1):
    s += (10 ** i) * (n % 10)
    n //= 10
print(s)
*/