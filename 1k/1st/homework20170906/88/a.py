# print('3' in str(n*n))

def e3(n):
    n2 = n * n
    while n2 > 0:
        if n2 % 10 == 3:
            return True
        n2 //= 10
    return False

n = int(input())
print(e3(n))





