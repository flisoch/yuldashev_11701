import java.util.Scanner;
class a{

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();
		int y = sc.nextInt();
		int z = sc.nextInt();
		int a = x + y + z;
		int b = x * y * z;
		if (a > b) {
			System.out.println(a);
		}
		else {
			System.out.println(b);
		}
	}	
}

/*
x,y,z= [int(x) for x in (input().split())]

a= x+y+z
b=x*y*z
if a>b:
    print(a)
else:
    print(b)
*/