import java.util.Scanner;

class c {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		int b = sc.nextInt();
		int s = 1;
		for (int i = 0; i < b; i++) {
			s *= a;
		}

		System.out.print(s);
	}
}

/*
# fail
a = int(input())
n = int(input())
s = 1/a
k=1
for i in range(n+1):
    for j in range(i):
        k = a*(a+j)
    s += 1/k

print(s)
*/