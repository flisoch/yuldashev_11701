import java.util.Scanner;

class b {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		int n = sc.nextInt();
		int s = 1;
		for (int i = 0; i < n; i++) {
			s *= a + i;
		}

		System.out.print(s);
	}
}

/*
a = int(input())
n = int(input())
s = 1
for i in range(n):
    s *= a+i
print(s)
*/
