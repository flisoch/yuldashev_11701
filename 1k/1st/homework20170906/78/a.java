import java.util.Scanner;

class a {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		int b = sc.nextInt();
		int s = 1;
		for (int i = 0; i < b; i++) {
			s *= a;
		}

		System.out.print(s);
	}
}

/*a = int(input())
b = int(input())
s = 1
for i in range(b):
    s *= a
print(s)
*/

