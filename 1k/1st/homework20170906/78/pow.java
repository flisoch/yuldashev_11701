import java.util.Scanner;

class pow {

	public static long pow(int a, long n) {

		long s = 1;
		while (n > 1) {

			if (n % 2 == 0) {
				a *= a ;
				n /= 2;

				s *= a;
				
			}
		}
		return s;
	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		int n = sc.nextInt();
		
		System.out.print(pow(a, n));
	}
}