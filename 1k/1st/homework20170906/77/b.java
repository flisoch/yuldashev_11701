import java.util.Scanner;

class b {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int k = 2;
		int s = 1;
		while (k != n + 1) {
			s *= k;
			k++;
		}

		System.out.print(s);
	}
}

/*n=int(input())
k=2
s=1
while k!=n+1:
    s=s*k
    k+=1
print(s)
*/