import java.util.Scanner;

class d {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double s = 1.0;
		for (int i = 2; i < n + 1; i++) {
			s += 1 / (Math.sin(1) + Math.sin(n));
		}

		System.out.print(s);
	}
}

/*from math import sin
n = int(input())
s = 1/sin(1)
for i in range(2, n+1):
    s += 1/(sin(1)+sin(n))
print(s)
*/