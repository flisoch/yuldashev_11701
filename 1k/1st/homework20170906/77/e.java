import java.util.Scanner;

class e {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double s = 0;
		for (int i = 0; i < n; i++) {
			s = Math.sqrt(2 + s);
		}

		System.out.print(s);
	}
}

/*from math import sqrt

n = int(input())
s=0

for i in range(n):
    s = sqrt(2+s)
print(s)
*/