import java.util.Scanner;

class g {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double s = 0;
		for (int i = n; i > 0; i--) {
			s = Math.sqrt( 3 * i + s);
		}

		System.out.print(s);
	}
}

/*from math import sqrt
n= int(input())
s=0
for i in range(n,0,-1):
    s =sqrt(3*i +s)
print(s)
*/