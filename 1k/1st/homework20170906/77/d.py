from math import sin
n = int(input())
s = 1/sin(1)
for i in range(2, n+1):
    s += 1/(sin(1)+sin(n))
print(s)
