import java.util.Scanner;

class c {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double s = 1.0;
		for (int i = 1; i < n + 1; i++) {
			s *= 1 + 1 / (i*i);
		}

		System.out.print(s);
	}
}

/*
n = int(input())
s = 1
for i in range(1, n+1):
    s *= 1 + 1 / i**2
print(s)
*/
