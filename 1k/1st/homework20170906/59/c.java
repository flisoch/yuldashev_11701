import java.util.Scanner;

class c {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();
		int y = sc.nextInt();
		System.out.print(Math.abs(x) <= 1 && Math.abs(y) <= 1);
		
	}	
}

/*
from math import fabs
x = float(input())
y = float(input())
print( fabs(x)<= 1 and fabs(y)<=1)
*/