x = float(input())
y = float(input())
if x <= 0:
    print(y - 0.5*x <= 1 and y + 0.5*x <= 1)
else:
    print(x*x + y*y <= 1)