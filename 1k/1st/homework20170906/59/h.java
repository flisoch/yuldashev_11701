import java.util.Scanner;

class h {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double y = sc.nextDouble();
		
		System.out.print(Math.abs(y) <= 1 && y >= -2 && y <= Math.abs(x));
		
	}	
}

/*from math import fabs
x = float(input())
y = float(input())
print( fabs(x) <=1 and y >= -2 and y <= fabs(x))
*/