from math import fabs
x = float(input())
y = float(input())
if fabs(x) <= 1:
    print(y >= fabs(x))
else:
    print(y >= 1)