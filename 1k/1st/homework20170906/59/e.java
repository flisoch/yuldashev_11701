
import java.util.Scanner;

class e {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double y = sc.nextDouble();
		System.out.print((y + 2 * x <= 1) && (y + 2 * x >= -1) && (y - 2 * x >= -1) && (y - 2 * x <= 1));
		
	}	
}

/*
x = float(input())
y = float(input())
print((y+2*x <= 1) and (y+2*x >= -1) and (y-2*x >= -1) and (y-2*x <= 1))
*/