import java.util.Scanner;

class r9d1 {

	// r9d (x^k) / (k+1)   k = 0, k-> inf
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();   //abs(x) < 1
		double s = 1;
		double e = 1e-6;
	
		double degx = 1;
		double denominator = 1;
		double slag = degx / denominator;

		
		while (Math.abs(slag) > e) {

			degx *= x;
			denominator++;
			slag = degx / denominator;
			s += slag;

		}

		
		System.out.println(s);

	}
}