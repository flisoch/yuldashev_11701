
class r9d4 {

	// r9d  (-1)^k * ( k! / (2k + 1)!! )   k = 0, k-> inf
	 
	public static void main(String[] args) {

	
		double s = 1;
		double e = 1e-8;
		int k = 1;
		double numerator = 1;
		double denominator = 1;
		double slag = numerator / denominator;

		
		while (Math.abs(slag) > e) {

			numerator *= k; 
			denominator *=  2 * k + 1;
			slag = numerator / denominator;

			if (k % 2 == 1) {
				slag = -slag;
			}
			
			k++;
			s += slag;
		}

		
		System.out.println(s);

	}
}