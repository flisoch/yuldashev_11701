import java.util.Scanner;

class r9d3 {

	// r9d ( (2^k) * x^(2k) ) / k!!   k = 0, k-> inf
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();   //abs(x) < 1
		double s = 1;
		double e = 1e-8;
		int k = 2;
		double numerator = 1;
		double evenfaq = 1;
		double oddfaq = 1;
		double slag = numerator / oddfaq;

		
		while (Math.abs(slag) > e) {

			numerator *= x * x * 2; 
			
			if (k % 2 == 1) {

				oddfaq *=  k;
				slag = numerator / oddfaq;
			}

			else {
				evenfaq *=  k;
				slag = numerator / evenfaq;
			}

			k++;
			s += slag;

		}

		
		System.out.println(s);

	}
}