import java.util.Scanner;

class r9d2 {

	// r9d (x^(2k) / (2k)!   k = 0, k-> inf
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();   //abs(x) < 1
		double s = 1;
		double e = 1e-8;
		int k = 2;
		double numerator = 1;
		double denominator = 1;
		double slag = numerator / denominator;

		
		while (Math.abs(slag) > e) {

			numerator *= x * x;
			denominator *= (k-1) * k;
			k += 2;
			slag = numerator / denominator;
			s += slag;

		}

		
		System.out.println(s);

	}
}