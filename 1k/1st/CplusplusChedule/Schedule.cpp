#include "Schedule.h"

#include <iostream>

Schedule::Schedule() {
    this->clear();
}

void Schedule::clear() {
    this->sch.clear();
}

Schedule::~Schedule() {
    this->clear();
}


void Schedule::fillWith(StudyType t) {
    this->sch = {t, t, t, t, t};
}

void Schedule::add(StudyType t) {
    this->sch.push_back(t);
}


const std::vector<StudyType>& Schedule::get() {
    return this->sch;
}



std::ostream& Schedule::operator<<(std::ostream& os, const Schedule& schedule) {
    for (auto st : schedule.get()) {
        switch (st) {
        case CLASS:
            os << "CLASS, "; 
            break;
        case LECTURE:
            os << "LECTURE, ";
            break;
        case BREAK:
            os << "BREAK, ";
        break;
        }
    }
    
    return os;
}



