#include "TimeTable.h"
#include "Schedule.h"

#include <iostream>

#include <string>
using std::string;

TimeTable::TimeTable() {

}

TimeTable::TimeTable(const TimeTable& other) {
    this->table = other.table;
}

TimeTable::~TimeTable() {
    this->table.clear();
}


TimeTable::add(const std::vector<string> line) {
    Day d;
    auto dayStrIt = line.begin();

    if (*dayStrIt == "SUNDAY") {
        d = SUNDAY;
    } else if (*dayStrIt == "MONDAY") {
        d = MONDAY;
    } else if (*dayStrIt == "TUESDAY") {
        d = TUESDAY;
    } else if (*dayStrIt == "WEDNESDAY") {
        d = WEDNESDAY;
    } else if (*dayStrIt == "THURSDAY") {
        d = THURSDAY;
    } else if (*dayStrIt == "FRIDAY") {
        d = FRIDAY;
    } else if (*dayStrIt == "SATURDAY") {
        d = SATURDAY;
    }

    Schedule s;

    for (dayStrIt++; dayStrIt != line.end(); dayStrIt++) {
        switch ( (*dayStrIt)[0] ) {
        case 'L':
            s.add(LECTURE);
            break;
        
        case 'C':
            s.add(CLASS);
            break;
        
        case 'B':
            s.add(BREAK);
            break;
        }
    }

    this->table[d] = s;
}

string TimeTable::compare(TimeTable other) {
    string result = "Breaks in common\n";
    for (int day = SUNDAY; day <= SATURDAY; day++) {
        
        for (int i = 0; i < 5; i++) {
            if (this->table[day][i] == other.table[day][i]
                    && other.table[day][i] == BREAK) {
                result += WEEK_DAYS[day] + " : " + BREAK;
            }
        }
    }
    return result;
}

const std::vector<Schedule>& TimeTable::get(Day d) {
    return this->table[d];
}



std::ostream& TimeTable::operator<<(std::ostream& os, const TimeTable& timeTable) {
    os << "____\n";

    // OUPUT

    for (int day = SUNDAY; day <= SATURDAY; day++) {
        os << WEEK_DAYS[day] << ": ";
        os << timeTable.get(day);
    }
    
    os << "____\n";

    return os;
}


