#pragma once

#include <vector>
#include <string>

#include <iostream>


#include "Schedule.h"

using std::string;

enum Day {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
};

const std::vector<string> WEEK_DAYS = {
    "SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"
};

class TimeTable {
public:
    TimeTable();
    TimeTable(const TimeTable&);

    ~TimeTable();

    add(const std::vector<string>);
    //compare(<TimeTable>,<TimeTable>);

    const std::vector<Schedule>& get(Day);

	friend std::ostream& operator<<(std::ostream&, const TimeTable&);
private:
    std::vector<Schedule> table;
};



