package classwork1202;

public class SecondStrings {

	public final static int L = 5;
	public final static int D = 4;

	public static void main(String[] args) {
		String s = "";
		long t1 = System.nanoTime();
		backTracking(s);
		long t2 = System.nanoTime();
		System.out.println(t2 - t1);
	}

	public static void backTracking(String s) {
		if (s.length() == L) {
			System.out.println(s);
		} else if (s.length() == 0) {
			for (char c = 'a'; c <= 'z'; c += 1) {
				backTracking(s + c);
			}
		}

		else {
			for (char c = 'a'; c <= 'z'; c += 1) {
				char c2 = s.charAt(s.length() - 1);
				if (c - c2 > D) {
					backTracking(s + c);
				}
			}
		}
	}
}
