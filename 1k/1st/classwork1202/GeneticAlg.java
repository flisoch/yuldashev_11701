package classwork1202;

import java.util.Random;
import java.util.Arrays;
import java.util.Comparator;

public class GeneticAlg {

	public final static int D = 5;
	public final static int L = 5;
	public final static Random r = new Random();
	public final static int POPULATION = 8;  // ������ �����
	
	public static void sout(String[] arr, boolean with, boolean descendants ) {   // ���������� ������� ��������� � ����������� ������������/�����������/ ��-���-�����-��� 
		
		for (int i = 0; i < POPULATION; i++) {
			System.out.print(arr[i] + " " + fitnessFunction(arr[i]) + "  ");
			
		}
		if (with) {
			if (!descendants) {
				System.out.print("weak words: ");
			}
			else {
				System.out.print("Descendants: ");
			}
		
			
			for (int i = POPULATION; i < arr.length;i++) {
				System.out.print((arr[i]) +" " + fitnessFunction(arr[i]) + "  ");
			}
		}	
		System.out.println();
	}	
	
	public static void main(String[] args) {
		
		System.out.println( "Conditions : "
						+ "distance between letters is more than " + D 
						+ "; Length of word is " + L 
						+ "; number of words is " + POPULATION 
						+ "\n");
		
		System.out.println("New Population : ");     
		String [] population = createPopulation(POPULATION);
		sout(population, false, false );
		System.out.println();
		
		
		while (!isIdealPopulation(population)) {
			
			crossingover(population);
			sout(population, true, true);  				// shows population before sorting
			sorting(population);
			sout(population, true, false);
		}
		
		
		System.out.print("\nSuitable words: ");
		sout(population, false, false);
		
		
	}
	
	public static boolean isIdealPopulation(String [] population) {
		for (int i = 0; i < POPULATION;i++) {
			if (fitnessFunction(population[i]) != 100) {
				return false;
			}
		}
		return true;
	}
	
	public static String[] createPopulation(int n) {
		
		String[] population = new String[n + n/2];
		for (int i = 0;i < n ;i++ ) {
			String word = "";
			for (int j = 0;j < L ;j++ ) {
				word += (char)(r.nextInt(26) + 'a');
			}
			population[i] = word;
		}
			
		return population;
	}

	public static double fitnessFunction(String word){     // returns % of correct neighborhoods
		
		double index = 100;

		for (int i = 1; i < L ; i++ ) {
			if (Math.abs(word.charAt(i) - word.charAt(i - 1)) < D) {
				index -= 100 / (L - 1);
			}
			
		}
		return index;
	}

	public static void crossingover(String[] population) {
		
		for (int i = 0;i < POPULATION ; i += 2) {
			String newWord = crossing(population[i], population[i + 1]);
			population[POPULATION + i/2] = newWord;
			
		}

	}
	
	public static void sorting(String[] population) { 
		
		Arrays.sort(population, FitnessIndexComparator);
	}
	
	public static String crossing(String word1, String word2) {

    	String newWord = "";
    	for (int i = 0;i < L ;i++ ) {
    		int k = r.nextInt(2);
    		newWord += (k == 0 ? word1.charAt(i) : word2.charAt(i));
    	}
    	return newWord;
    }
	
	public static Comparator<String> FitnessIndexComparator = new Comparator<String>() {
		 
        @Override
        public int compare(String s1, String s2) {
            return (int)(fitnessFunction(s2) - fitnessFunction(s1) );	// reverse order
        }
    };
}