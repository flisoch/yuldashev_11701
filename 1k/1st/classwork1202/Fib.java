package classwork1202;

public class Fib {

	public static void main(String[] args) {
		int n = 5;
		long t1 = System.nanoTime();

		int f = fib(n);
		long t2 = System.nanoTime();
		System.out.println(t2 - t1);
		System.out.println(f);
	}

	public static int fib(int n) {

		int m1 = 0;
		int m2 = 0;
		int cur = 0;
		for (int i = 0; i < n; i++) {
			if (i == 0) {
				m1 = 1;
			} else if (i == 1) {
				m2 = 1;
			}

			cur = m1 + m2;
			m1 = m2;
			m2 = cur;

		}
		return cur;
	}
}