package classwork1202;

public class CorrectFib {

	public static void main(String[] args) {

		int n = 5;
		long t1 = System.nanoTime();

		int f = fib(n);
		long t2 = System.nanoTime();
		System.out.println(t2 - t1);
		System.out.println(f);

	}

	public static int fib(int n) {

		int[] a = new int[n + 1];
		fibonacciRec(n, a);
		return a[n];
	}

	public static void fibonacciRec(int n, int[] a) {

		if (n == 0) {
			a[n] = 1;
		} else if (n == 1) {
			fibonacciRec(n - 1, a);
			a[n] = 1;
		}

		else {
			fibonacciRec(n - 1, a);
			a[n] = a[n - 1] + a[n - 2];
		}
	}
}
