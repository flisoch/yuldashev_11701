/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 14
*/
import java.util.Scanner;

public class Task14PS1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		int k = sc.nextInt(); // from 2 to 9
		int n = sc.nextInt(); // every digit < k

		int m = n % 10;
		n /= 10;

		while (n > 0) {

			m += (n % 10) * k;
			k *= k;
			n /= 10;


		}

		System.out.println(m);
	}
}
