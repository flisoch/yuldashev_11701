/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 19
*/
import java.util.Scanner;

public class Task19PS1 {

	public static void main(String[] args) {
		Sout s = new Sout();
		// Scanner sc = new Scanner (System.in);
		int [][] a = { 
					{1, 2, 3, 4, 5},
					{6, 7, 8, 9, 1},
					{1, 2, 3, 4, 5},
					{6, 7, 8, 9, 1},
					{1, 2, 3, 4, 5}
				};

		s.sout(changed(a));
		
	}

	public static int[][] changed(int [][] arr) {
		
		int n = arr.length;
		for ( int i = 0; i < n/2; i ++) {
			for (int j = i+1; j < n - i - 1; j++) {

				arr[i][j] = 0;
			}

		}
		for ( int i = n/2 + 1 ; i < n; i ++) {
			for (int j = n - i; j < i; j++) {

				arr[i][j] = 0;
			}

		}
		
		return arr;
	}

	
		
}