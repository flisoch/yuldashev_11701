/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 07
*/

import java.util.Scanner; 

public class Task07PS1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner (System.in);
		int r = 10;

		printInYan(r);
		
	}

	public static void printInYan(int r) {

		for (int y = 0;y <= 2 * r  ;y++ ) {
			for (int x = 0; x <= 2 * r ; x++) {

				if ((y-r)*(y - r ) + (x - r)*(x - r) > r * r) {
					System.out.print("  ");
				}
				else if( (y - r)*(y - r ) + (x - r/2)*(x - r/2) <= r/2 * r/2  ||

					((y - r)*(y - r ) + (x - 3*r/2)*(x - 3*r/2) >= r/2 * r/2)&& y >= r

					) {
					System.out.print("* ");
				} 
				else {
					System.out.print ("0 ");
				}
				
			}
			System.out.println();
		}
		
	}


}