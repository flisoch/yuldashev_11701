/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 05
*/
import java.util.Scanner;

public class Task05PS1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double denomFaq = 2;
		int numeratorFaq = 1;
		double s = 0.0;

		for (int m = 2; m <= n; m++) {
			numeratorFaq *= m - 1;
			denomFaq *= 2 * m * (2 * m - 1);
			s += (numeratorFaq*numeratorFaq) / denomFaq;


			System.out.println("numeratorFaq = " + numeratorFaq + "numerator" + numeratorFaq*numeratorFaq
							 + "denomFaq = " + denomFaq + "s = " + s );
			System.out.println("slagaemoe =" + ((numeratorFaq*numeratorFaq) / denomFaq) );
		} 
		System.out.println(s);
	}
}