/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 06
*/

import java.util.Scanner; 

public class Task06PS1 {

	public static void main(String[] args) {
		Sout s = new Sout();
		Scanner sc = new Scanner (System.in);
		int r = sc.nextInt();

		printCircle(r);
		
	}

	public static void printCircle(int r) {

		for (int y = 0;y <= 2 * r ;y++ ) {
			for (int x = 0; x <= 2 * r; x++) {

				if( (y - r)*(y - r) + (x - r)*(x - r) > r*r ) {
					System.out.print("* ");
				} 
				else {
					System.out.print ("0 ");
				}
				
			}
			System.out.println();
		}
		
	}


}