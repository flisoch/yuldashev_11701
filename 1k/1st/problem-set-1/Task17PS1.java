/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 17
*/

// import java.util.Scanner;

public class Task17PS1 {

	public static void main(String[] args) {
		Sout s = new Sout();
		// Scanner sc = new Scanner (System.in);

		int [] a = {321, 210, 55555, 987 };
		int [] b = { 431, 321, 321 , 321};
		int [] c = { 111, 111, 0 , 22, 33};

		s.sout(reverse(a));
		s.sout(reverse(b));
		s.sout(reverse(c));
		
		
	}

	public static int[] reverse(int [] arr) {
		int len = arr.length ;

		for (int i = 0; i < len / 2; i++) {
			int temp = arr[i];
			arr[i] = arr[len - 1 - i];
			arr[len - 1- i] = temp;
		}
		return arr;
	}


}