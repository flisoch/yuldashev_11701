/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 10
*/

public class Task10PS1 {

	public static void main(String[] args) {
		boolean flag = false;
		int[][] numbers = {
						{1,2,3,2,4000,3},
						{0,0,0,0},
						{1,2,33333,2,1},
						{-1,20000,-1,0,1,0},
						{5,5,777777,6,888888,7}
					};
		
		for (int[] num : numbers) {

			System.out.println(check (num));
		}

	}

	public static boolean check(int[] numbers) {
		for (int i = 1; i < numbers.length; i++) {
			if ( numbers[i] > numbers[i - 1] && numbers[i] > numbers[i + 1] ) {
				if (numbers[i] % 2 == 0) {
					return true;
					
				}
				i++;
			}	
		}
		return false;
	}
}