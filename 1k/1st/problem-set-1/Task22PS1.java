/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 22
*/
// import java.util.Scanner;

public class Task22PS1 {

	public static void main(String[] args) {

		Sout s = new Sout();
		

		// Scanner sc = new Scanner (System.in);
		int [][] a = { 
					{5, 6, 3, 4},
					{4, 5, 6, 3},
					{3, 4, 5, 6},
					{6, 3, 4, 5}
					
				};
		int n = a.length;
		int[] but = new int[n];

		fill(but, a);

		s.sout(but);
		s.sout(countMax(but));
	}

	public static void fill (int[] arr, int[][] matrix) {
		int n = matrix.length;

		for (int i = 0; i < n ; i++ ) {
			for (int j = 0;j < n ;j++ ) {
				int coord = ((n - j + i) % n);
				// System.out.println("coord = "+coord + " matrix[i][j]  "+ matrix[i][j]);

				arr[coord] += matrix[i][j]; 
				
			}
			
		}


	}
	public static int countMax(int[] arr) {
		int max = arr[0];
		for (int a:arr) {
			if(a > max) {
				max = a;
			}
		}
		return max;
	}


}