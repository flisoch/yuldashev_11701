/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 11
*/

public class Task11PS1 {

	public static void main(String[] args) {
		boolean flag = false;
		int[][] numbers = {
						{1,2,3,2,4000,3},
						{0, 10, 0, 10, 0},
						{1, 0 ,222, 0 ,1, 222, 1, 222, 1},
						{-1,20000,-1,0,100000,0},
						{5,5,777777,6,777777,7}
					};
		
		for (int[] num : numbers) {

			System.out.println(check (num));
		}

	}

	public static boolean check(int[] numbers) {
		int k = 0;
		for (int i = 1; i < numbers.length; i++) {

			if ( numbers[i] > numbers[i - 1] && numbers[i] > numbers[i + 1] ) {
				if (numbers[i] % 2 == 0) {
					k++;
				}

				i++;
			}

			if (k > 2) { 
				return false;
			}

		}
		return k == 2;
	}
}