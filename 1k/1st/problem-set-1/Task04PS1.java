/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 04
*/

import java.util.Scanner;

public class Task04PS1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		
		for (int k = 2; n > 1; k++) {    
			while (n % k == 0) {
				n /= k;
				System.out.print(k + " ");
			}
		}
				
	}

}