/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 16
*/
import java.util.Scanner;

public class Task16PS1 {

	public static void main(String[] args) {

	
		int [] a = {321, 210, 55555, 987 };
		int [] b = { 431, 321, 321 , 321};
		int [] c = { 111, 111, 0 , 321};
		
		System.out.println(checkIfThreeFit(a) + "\n" 
						+ checkIfThreeFit(b) + "\n"
						+ checkIfThreeFit(c)) ;
		
		// System.out.println(descendingOrder(55555));
	}


	public static boolean checkIfThreeFit(int[] arr) {
		
		int k = 0;

		for (int i = 0; i < arr.length; i++) {
			if (descendingOrder(arr[i])) {
				k++;
			}
			if (k > 3) {
				return false;
			}
		}
		
		return k == 3;

	}

	public static boolean descendingOrder(int n) {
		boolean flag = true;
		int previous = n % 10;
		n /= 10;

		while (n > 0 && flag) {

			if ((n % 10) <= previous) {
				flag = false;
			}
			previous = n % 10;
			n /= 10;
		}

		return flag;
	}


}
