/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 23
*/

public class Task23PS1 {

	public static void main(String[] args) {
		
		int[][] matrix1 = {
						{5,8, -4},
						{6, 9, -5},
						{4, 7, -3}
					
					};
		int[][] matrix2 = {
						{3, 2, 5},
						{4, -1, 3},
						{9, 6, 5}
						
					
		};
		


		sout(multiply(matrix1, matrix2));
		

	}

	public static int[][] multiply(int[][] a, int[][] b) {
		int bcolumns = b.length;
		int acolumns = a[0].length;
		int blines = b[0].length;
		int alines = a.length;
		
		int[][] c = new int[alines][bcolumns];


		for (int i = 0; i < alines ;i++ ) {
			for (int j = 0 ;j < bcolumns;j++ ) {


				int temp = 0;



				for (int m = 0;m < acolumns;m++) {
					temp += a[i][m] * b[m][j];
				}
				c[i][j] = temp;
				sout(c);
			}
			
		}
		return c;
	}

	public static void sout(int[][] arr) {
		for (int[] a: arr) {
			for (int num :a) {

				System.out.print(num + " ");
			}
			System.out.println();
		}
		System.out.println();
	}	

}