/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 15
*/
import java.util.Scanner;

public class Task15PS1 {

	public static void main(String[] args) {
		Sout s = new Sout();
		// Scanner sc = new Scanner (System.in);
		int [] a = {1, 222, 55555, 0 };
		int [] b = { 1, 222, 23222, 0};
		
		s.sout(checkIfTwoFit(a));
		s.sout(checkIfTwoFit(b));
	}

	public static int lenOfNumber(int n) {
		int k = 0;
		while (n > 0) {
			n /= 10;
			k++;
			if (k > 5) {
				break;
			}
		}
		return k;
	}

	public static boolean hasOnlyEvenDigits(int n) {
		boolean flag = true;
		while (n > 0 && flag) {
			if ((n % 10) % 2 !=0) {
				flag = false;
			}
			n /= 10;
		}
		return flag;
	}

	public static boolean hasOnlyOddDigits(int n) {
		boolean flag = true;
		while (n > 0 && flag) {
			if ((n % 10) % 2 ==0) {
				flag = false;
			}
			n /= 10;
		}
		return flag;
	}

	public static boolean checkIfTwoFit(int[] arr) {
		
		int k = 0;

		for (int i = 0; i < arr.length; i++) {
			if ((lenOfNumber(arr[i]) == 3 || lenOfNumber(arr[i]) == 5) && (hasOnlyOddDigits(arr[i]) || hasOnlyEvenDigits(arr[i]))) {
				k++;
			}
			if (k > 2) {
				return false;
			}
		}
		return k == 2;

	}

}
