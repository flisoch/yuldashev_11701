/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 20
*/

								
public class Task20PS1 {					
												// DO SMT WITH ZERO columns/lines !
	public static void main(String[] args) {


		Sout s = new Sout();
		int [][] a = {                  //doesn't work correctly
					{0, 1, 2},
					{0, 4, 5},
					{0, 7, 8},
					
				};

		int [][] b = { 					// everything alright
					{1, 2, 3},
					{4, 5, 6},
					{7, 8, 8},
					
				};
		s.sout(a);
		s.sout(triangleView(a));
		s.sout(b);
		s.sout(triangleView(b));
	}

	public static int[][] triangleView(int [][] a) {
		
		int n = a.length;
		for ( int minor = 0; minor < n  ; minor ++) {

			int first = a[minor][minor];

			for (int line = minor; line < n - 1 ; line++) {

				int tempFirst = a[line + 1][minor];

				for (int m = minor; m < n  ;m++ ) {
					
					a[line + 1][m] = tempFirst * a[minor][m] - first * a[line + 1][m]; 

					
				}

					
			}
			Sout.sout(a);
		}
		
		return a;
	}

	
}

