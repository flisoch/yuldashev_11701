/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 08
*/
import java.util.Scanner;

public class Task08PS1 {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double denomFaq = 1;
		double numerator = x;
		int denomMult = 1;
		double s = x;
		double e = 1e-9;
		double curSlag = x;
		int k = 1;
		System.out.println(curSlag);
		while (abs(curSlag) > e) {
			numerator *= -1 * x * x * x * x;
			denomFaq *= 2*k * (2 * k - 1);
			denomMult += 4;
		

			
			k++;
			curSlag = numerator / (denomFaq * denomMult);
			s += curSlag;
			// System.out.println("denomFaq = " + denomFaq + " denomMult =  " + denomMult
			// 				 + "numerator = " + numerator + "s = " + s );
			// System.out.println("curslag =   " + curSlag);

		}

		System.out.println(s);
	}

	public static double abs(double x) {
		if (x < 0) {
			return -x ;
		}
		return x;
	}
} 