/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 02
*/

import java.util.Scanner;

public class Task02PS1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int k = sc.nextInt();
		int m = sc.nextInt();

		int num = start(k);
		while (num <= m) {
			System.out.println(num);
			num += 3;
		}
	}
	
	
	public static int start(int k) {

		switch (k % 3) {
				
			case 1:
				k += 2;
				break;
				
			case 2:
				k += 1;
				break;
			case 0:
				break;
			
		}
		return k;

	}



}