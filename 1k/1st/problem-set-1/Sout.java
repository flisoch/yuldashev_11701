/**
* @author Daler Yuldashev
* 11-701
* for Problem Set 1 Tasks : 15, 17-25
*/



public class Sout {

	public static void sout(int[][] arr) {
		
		for (int[] a: arr) {
			for (int num :a) {

				System.out.print(num + " ");
			}
			System.out.println();
		}
		System.out.println();
	}	

	public static void sout(int[] arr) {
		for (int a: arr) {
			System.out.print(a + " ");
		}
		System.out.println();
	}	

	public static void sout(int n) {
		System.out.println(n);
	}
	public static void sout(boolean n) {
		System.out.println(n);
	}
}