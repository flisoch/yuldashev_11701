/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 24
*/
import java.util.Scanner;

public class Task24PS1 {

	public static void main(String[] args) {

		Sout s = new Sout();

		int [][][] arr = {
			{
				{1, 3}, {6, 9, 6}
			},

			{
				{4,5,6}, {6, 6, 6}, {9 ,3, 5}
			},

			{
				{1,2,3,4,5}, {21,3,24,6}
			},

			{
				{3, 3, 3}
			}
		
		};
		int [][][] arr1 = {
			{
				{1, 1}, {1, 1, 1}
			},

			{
				{4,5,6}, {6, 6, 6}, {9 ,3, 5}
			},

			{
				{1,2,3,4,5}, {21,3,24,6}
			},

			{
				{3, 3, 3}
			}
		
		};

		s.sout(check(arr));
		s.sout(check(arr1));
	}

	public static boolean check(int[][][] arr) {

		for (int i = 0; i < arr.length ;i++ ) {
			if (!hasDemandedLine(arr[i])) {
				return false;
			}
			
		}
		return true;
	}

	public static boolean hasDemandedLine(int[][] arr) {

		for (int i = 0; i < arr.length ;i++ ) {
			if (onlyDivededByThreeIn(arr[i])) {
				return true;
			}
			
		}
		return false;
	}

	public static boolean onlyDivededByThreeIn(int[] arr) {

		for (int i = 0; i < arr.length ;i++ ) {
			if (!ThreeDivides(arr[i])) {
				return false;
			}
			
		}
		return true;
	}

	public static boolean ThreeDivides(int n) {

		if (n % 3 == 0){

			return true;
		}
		return false;
	}

}