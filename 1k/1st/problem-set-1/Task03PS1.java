/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 03
*/



import java.util.Scanner;

public class Task03PS1 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		double num = sc.nextInt();
		double e = 1e-6;
		double lBorder = 1;
		double rBorder = num;
		

		while (abs(lBorder - rBorder) > e) {

			double curNum = (rBorder - lBorder) / 2;

			if (curNum * curNum > num) {
				rBorder = curNum; 
			}

			else if (curNum * curNum < num) {

				lBorder = curNum;	
			}

		}

		System.out.println(curNum);
	}	

	public static double abs(double x) {
		if (x < 0) {
			return -x ;
		}
		return x;
	}

}