/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 18
*/

import java.util.Scanner;

public class Task18PS1 {

	public static void main(String[] args) {

		Sout s = new Sout();
		Scanner sc = new Scanner(System.in);
		int [] a = {1, 2, 3, 4, 5, 6, 7, 8 ,9, 10 };
		
		// int [] b = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
		// int [] c = { 111, 111, 222 , 333, 444};
		int k = sc.nextInt();
		int n = a.length;
		s.sout(a);
		reverse(a, 0, n);
		reverse(a, 0, n - k);
		reverse(a, n - k , n);
		s.sout(a);
		// sout(shift(b, k));
		// sout(shift(c, k));
		
		
	}

	public static void reverse(int [] arr, int from, int to) {
		
		int m = 0;
		for (int i = from; i < (from + to) / 2; i++) {
			int temp = arr[i];
			arr[i] = arr[to - 1 - m];
			arr[to - 1- m] = temp;
			m++;
		}
	}
	

	
}