/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 01
*/


// без ифов


import java.util.Scanner;
public class Task01PS1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		triangle(n);
		twoTriangles(n);
	}

	public static void triangle(int n)  {

		for (int y = 0;y < n ;y++ ) {
			for (int x = 0;x < 2*(2 *n)  ;x++ ) {
				if(y >= 2*n - x - 1 && y >= x + 1- 2*n) {
					System.out.print("*");
				}
				else {
					System.out.print (" ");
				}
				
			}
			System.out.println();
		}
		System.out.println("\n" );
	}
	public static void twoTriangles(int n) {

		for (int y = 0;y < n ;y++ ) {
			for (int x = 0;x < 2*(2 *n)  ;x++ ) {
				if(y >= n - x - 1 && y >= x + 1 - n ||
					y -n > -x +2*(n-1) && y -n > x - 2*(2*n) 
					) {
					System.out.print("*");
				}
				else {
					System.out.print (" ");
				}
				
			}
			System.out.println();
		}
	}
	
}