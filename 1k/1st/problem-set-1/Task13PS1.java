/**
* @author Daler Yuldashev
* 11-701
* Problem Set 1 Task 13
*/
import java.util.Scanner;

public class Task13PS1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		int n = sc.nextInt();
		// int n = 4;

		int s = 1;
		int slag = 1;

		for (int i = 1; i < n; i++) {
			slag = (slag * 10) + 1;
			s += slag;
		}

		System.out.println(s);
	}
}