
class Avtomat {
	public static void main(String[] args) {
		int[] finalStates =  {1};
		int[] word = {0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0};
		int[][] arr = new int[][] {
			{1, 0, 2}, {2, 1, 2}
		};

		int state = 0;

		// for (int i = 0;i < word.length; i++) {
		// 	state = arr[word[i]][state];
		// }

		for(int x:word) {

			state = arr[x][state];
		}
		System.out.println(isFinal(state,finalStates));

	}

	public static boolean isFinal(int state, int[]finalStates) {
		for (int it: finalStates) {
			if (state == it) {
			return true;
				
			}
		}
		
		return false;
	}
}