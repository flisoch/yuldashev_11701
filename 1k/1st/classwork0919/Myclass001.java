
public class Myclass001 {

	public static int countNonZeroDigits(int x) {
		int result = 0;
		while (x != 0) {
			if (x % 10 != 0) {
				result++;
			}
			x /= 10;
			
		}
		return result;

	}

	public static void main(String [] args) {

		int x = Integer.parseInt(args[0]);
		int y = countNonZeroDigits(x);
		System.out.println(y);
	}
}