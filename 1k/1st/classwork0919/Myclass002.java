import java.util.Arrays
public class Myclass002 {

	public static void main(String [] args) {
		int n = 10;
		int [] a = new int[n];

		for (int i = 0; i < n; i++) {

			a[i] = i;
		}
		/*
		for (int i = 0; i < n; i++) {
			System.out.print(a[i] + " ");
		}

		for (int x: a) {
			System.out.print(x + " ");
		}
		*/
		System.out.println(Arrays.toString(a));
	}
}
