import java.util.Arrays;
import java.util.Random;
public class Myclass003 {

	public static void randomArray(int [] arr) {
		Random r = new Random();
		for (int i = 0; i < arr.length; i++) {
			arr[i] = r.nextInt(100);
		}
	}
	public static void main(String [] args) {
		
		int [] a = {1, 6, 7, 5};
		/*
		int [] b = a;
		b[2] = 100; */
		randomArray(a);
		System.out.println(Arrays.toString(a));

	}

}