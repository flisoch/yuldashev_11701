class CheckEvenDigits {

	public static boolean checkEvenDigit(int num) {

		boolean flag = true;
		while (num > 0 && flag) {

			if ((num % 10) % 2 != 0) {
				flag = false;
				return flag;
			}
			
			num /= 10;
		}
		return flag;


		
	}

	public static boolean checkEvenNums(int[] arr) {
		
		for (int num : arr) {
			if (checkEvenDigit(num) == false) {
				return false;
			}

		}
		return true;
		
	}

	public static void main(String[] args) {
		int[] arr = {22, 44, 66,88};

		int[] arr1 = {21, 44};
		System.out.println(checkEvenNums(arr));
		System.out.println(checkEvenNums(arr1));
	}
}