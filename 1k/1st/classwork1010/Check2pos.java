class Check2Pos {

	public static boolean has2Positive(int[] arr) {
		int k = 0;
		for (int num: arr) {
			if (num > 0) {
				k++;
			}
			if (k > 2) {
				return false;
			}
		}	
		return k == 2;
		
	}

	public static void main(String[] args) {
		int[] arr = {1, -1, -1, -2, -4, -1};



		
		System.out.println(has2Positive(arr));
		
	}
}