class CheckZeroes {

	public static boolean checkZero(int[] line) {

		for (int num : line) {
			if (num != 0) {
				return false;
			}

		}
		return true;


		
	}

	public static boolean checkEvenNums(int[][] arr) {
		
		for (int[] line : arr) {
			if (checkZero(line) == false) {
				return false;
			}

		}
		return true;
		
	}

	public static void main(String[] args) {
		int[][] arr = {{0, 0, 0},{0, 0, 0}};

		int[][] arr1 = {{0, 0, 0},{0, 1, 0}};
		System.out.println(checkEvenNums(arr));
		System.out.println(checkEvenNums(arr1));
	}
}