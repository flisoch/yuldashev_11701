public class Node {

    int key;
    Node left;
    Node right;
    int height;

    public Node(int key){
        this.key = key;
        height = 0;
    }
}
