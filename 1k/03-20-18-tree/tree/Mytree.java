package tree;

import queue.LinkedQueue;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class Mytree<E> {

    private Node<E> root;

    public Mytree(int n) {
        root = new Node<>();
        createTree(root, n);
    }

    public static void leftRootRight(Node root) {
        //меняешь 3 части метода местами и получаешь другой обход

        if (root.getLeft() != null) {
            leftRootRight(root.getLeft());
        }

        root.setValue(++n);

        if (root.getRight() != null) {
            leftRootRight(root.getRight());
        }

    }

    static int n = 0;

    public void widthQueueTraversal() {
        System.out.println("WIDTHH TRAVERSAL WITH QUEUE: \n\n");
        Queue<Node> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            Node p = queue.poll();
            n++;

            p.setValue(n);
            if (p.getLeft() != null) {
                queue.offer(p.getLeft());
            }
            if (p.getRight() != null) {
                queue.offer(p.getRight());
            }

        }
        n = 0;

    }

    public void depthStackTraversal() {

        System.out.println("DEPTH TRAVERSAL WITH STACK: \n\n");
        Stack<Node> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            Node p = stack.pop();
            ++n;

            p.setValue(n);
            if (p.getLeft() != null) {
                stack.push(p.getLeft());
            }
            if (p.getRight() != null) {
                stack.push(p.getRight());
            }
        }
        n = 0;
    }

    private static void createTree(Node root, int n) {
        root.setValue(n);
        if (n > 1) {
            int nl = n / 2;
            int nr = n - 1 - nl;
            if (nl > 0) {
                root.setLeft(new Node());
                createTree(root.getLeft(), nl);

            }
            if (nr > 0) {
                root.setRight(new Node());
                createTree(root.getRight(), nr);

            }
        }
    }


    public static void printTree(Node node, int h) {
        if (node != null) {
            printTree(node.getRight(), h + 1);
            for (int i = 0; i < h; i++) {
                System.out.print("  ");
            }
            System.out.println(node.getValue());
            printTree(node.getLeft(), h + 1);
        }

    }


    public void print() {
        printTree(root, 0);
        System.out.println("\n\n");
    }

    public void LeftRootRightRecursiveTraversal() {
        System.out.println("LEFTROOTRIGHT Recursively: \n\n");
        leftRootRight(root);
        this.print();
        n = 0;
    }

    public static void main(String[] args) {
        Mytree t = new Mytree(15);
        t.print();
        t.LeftRootRightRecursiveTraversal();
        t.widthQueueTraversal();
        t.print();
        t.depthStackTraversal();
        t.print();

    }
}
