package tree;

public class Node<T> {
    private T value;
    private Node right;
    private Node left;

    public void setLeft(Node<T> left) {
        this.left = left;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Node getLeft() {
        return left;
    }

    public void setRight(Node<T> right) {
        this.right = right;
    }

    public Node getRight() {
        return right;
    }

    public T getValue() {
        return value;
    }
    
    /*public Node(T value) {
        this.value = value;
    }*/
}