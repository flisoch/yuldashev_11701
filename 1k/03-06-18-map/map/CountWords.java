package map;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

public class CountWords {
    public static void main(String[] args) throws FileNotFoundException {
        HashMap<String,Integer> counter = new HashMap<>();

        Scanner sc = new Scanner(new File("res/words.txt"));
        while(sc.hasNext()) {
            String line = sc.next().toLowerCase();
            counter.put(line,
                    counter.get(line) == null ? 1:counter.get(line)+ 1);
        }

        Set<String> keys = counter.keySet();

        for(String key:keys) {
            System.out.print(key + " ");
            System.out.println(counter.get(key));
        }
    }
}
