package map;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

public class CountChars {
    public static void main(String[] args) throws FileNotFoundException {
        HashMap<Character,Integer> counter = new HashMap<>();
        Scanner sc = new Scanner(new File("collections/res/data.txt"));
        while(sc.hasNextLine()) {
            String line = sc.nextLine();
            for (int i = 0;i < line.length();i++) {
                counter.put(line.charAt(i),
                        counter.get(line.charAt(i))== null ? 1:counter.get(line.charAt(i))+ 1);
            }
        }
        Set<Character> keys = counter.keySet();
        for(Character key:keys) {
            System.out.print(key + " ");
            System.out.println(counter.get(key));
        }
    }
}
