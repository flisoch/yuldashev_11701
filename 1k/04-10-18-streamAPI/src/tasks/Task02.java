package tasks;

import collection.singletons.CarMakerCollectionSingleton;
import collection.singletons.ContinentCollectionSingleton;
import entities.CarMaker;
import entities.Continent;

import java.util.*;
import java.util.stream.Collectors;

public class Task02 {

    public static void main(String[] args) {

        Continent continent = ContinentCollectionSingleton.getInstance()
                .stream()
                .filter((cnt)-> cnt.getId() == 2)
                .findAny()
                .get();


        List<CarMaker> carMakersFromEurope = carMakersFromContinentStreamAPI(continent);

        System.out.println("Carmakers from " + continent.getName() + ":\n");

        for (CarMaker cm:carMakersFromEurope){
            System.out.println(cm.getName());
        }


    }

    public static List<CarMaker> carMakersFromContinent(Continent continent){
        List<CarMaker> cm = CarMakerCollectionSingleton.getInstance();
        List<CarMaker> carMakersFromContinent = new ArrayList<>();

        for (CarMaker carMaker : cm) {
            if (carMaker.getCountry().getContinent().getId() == continent.getId()) {
                carMakersFromContinent.add(carMaker);
            }
        }
        return carMakersFromContinent;
    }


    public static List<CarMaker> carMakersFromContinentStreamAPI(Continent continent){
        List<CarMaker> cm = CarMakerCollectionSingleton.getInstance();

        return cm.stream()
                .filter(carMaker -> carMaker.getCountry().getContinent().getId() == continent.getId())
                .collect(Collectors.toList());

    }
}
