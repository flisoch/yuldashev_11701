package tasks;

import collection.singletons.CarMakerCollectionSingleton;
import collection.singletons.ModelCollectionSingleton;
import entities.CarMaker;
import entities.Model;

import java.util.List;
import java.util.stream.Collectors;

public class Task05 {
    public static void main(String[] args) {
        List<CarMaker> carMakers = CarMakerCollectionSingleton.getInstance();
        List<Model> models = ModelCollectionSingleton.getInstance();

        // не все модели существуют в базе, поэтому не подсчитается для какого-то мейкера/модели ср.Хорспава

        firstPrintMethod(carMakers,models);
        secondPrintMethod(carMakers,models);

    }


    private static void firstPrintMethod(List<CarMaker> carMakers, List<Model> models) {
        System.out.println("on MAKERS");
        for(CarMaker carMaker: carMakers) {
            System.out.print(carMaker.getName() + " ");
            System.out.println(carMaker.averageHorsePower());
        }
        System.out.println("on MODELS:\n");

        for(Model m:models){
            System.out.print(m.getModel() + " ");
            System.out.println(m.averageHorsePower());
        }
    }


    private static void secondPrintMethod(List<CarMaker> carMakers, List<Model> models) {
        System.out.println("\naver HP on carmakers");
        System.out.println(
                carMakers.stream()
                        .collect(
                                Collectors.toMap(
                                        x -> x.getFullName(),
                                        CarMaker::averageHorsePower
                                )
                        )
        );
        System.out.println("\naver HP on models");
        System.out.println(
                models.stream()
                        .collect(
                                Collectors.toMap(
                                        Model::getModel,
                                        Model::averageHorsePower
                                )
                        )
        );
    }


}
