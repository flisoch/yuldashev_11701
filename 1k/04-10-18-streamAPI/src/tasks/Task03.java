package tasks;

import collection.singletons.CountryCollectionSingleton;
import entities.CarMaker;
import entities.Country;
import entities.Model;

import java.util.List;



public class Task03 {
    public static void main(String[] args) {
        List <Country> countries = CountryCollectionSingleton.getInstance();

        //1 способ
        long t1 = System.currentTimeMillis();

        for (Country country:countries) {
            System.out.println(country.countModels());
        }

        long t2 = System.currentTimeMillis();
        System.out.println("ВЕРМЯ: " + (t2 - t1) + "\n\n");

        //2 способ
        t1 = System.currentTimeMillis();
        showModelsAmountForEachCountry(countries);
        t2 = System.currentTimeMillis();
        System.out.println("ВРЕМЯ:" + (t2 - t1));
    }

    private static void showModelsAmountForEachCountry(List<Country>countries) {
        for(Country country:countries) {
            List<CarMaker> carMakers = country.getCarMakersList();
            int sum = 0;
            for(CarMaker carMaker: carMakers) {
                List<Model> models = carMaker.getModelList();
                sum += models.size();
            }
            System.out.println(sum);
        }
    }



}
