package tasks;

import collection.singletons.CarMakerCollectionSingleton;
import entities.CarMaker;

import java.util.List;

public class Task04 {
    public static void main(String[] args) {

    List<CarMaker> carMakers = CarMakerCollectionSingleton.getInstance();
    System.out.println(
            carMakers.stream()
                    .filter(carMaker -> carMaker.countModelsBeforeYear(1971) > 0)
                    .count());

    }
}
