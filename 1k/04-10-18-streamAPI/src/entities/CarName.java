package entities;

public class CarName {
    private int id;
    private Model model;
    public String make;

    public Model getModel() {
        return model;
    }

    public CarName(int id, Model model, String make) {
        this.id = id;
        this.model = model;
        this.make = make;
    }

    public int getId() {
        return id;
    }
}
