package entities;

import collection.singletons.CarMakerCollectionSingleton;
import collection.singletons.ModelCollectionSingleton;

import java.util.List;
import java.util.stream.Collectors;


public class Country {
    private int id;
    private String name;
    private Continent continent;


    public Country(int id, String name, Continent continent) {
        this.id = id;
        this.name = name;
        this.continent = continent;
    }

    public int getId() {
        return id;
    }

    public Continent getContinent() {

        return continent;
    }

    public int countModels(){
        int count = (int) ModelCollectionSingleton.getInstance()
                .stream()
                .filter(model -> model.getMaker().getCountry().getId() == this.id)
                .count();
        return count;
    }



    private List<CarMaker> carMakersList;
    public List<CarMaker> getCarMakersList() {
        if (carMakersList == null) {
            carMakersList = CarMakerCollectionSingleton.getInstance()
                    .stream()
                    .filter(carMaker -> carMaker.getCountry().getId() == this.id)
                    .collect(Collectors.toList());

        }
        return carMakersList;

    }


}
