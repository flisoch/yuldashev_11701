package entities;

public class WordData {
    public String word;
    public int count;

    public WordData(String word, int count) {
        this.word = word;
        this.count = count;
    }
}
