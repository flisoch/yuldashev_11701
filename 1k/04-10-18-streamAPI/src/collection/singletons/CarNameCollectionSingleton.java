package collection.singletons;

import collection.Reader;
import entities.Car;
import entities.CarName;

import java.util.List;

public class CarNameCollectionSingleton {
    private static final String FILE_NAME = "res/CARS/car-names.csv";
    private static List<CarName> instance;

    public static List<CarName> getInstance() {
        if (instance == null) {
            instance = Reader.readCarNames(FILE_NAME);
        }
        return instance;
    }
}
