package intCollections;

import java.util.Collection;
import java.util.Iterator;
import Iterators.ArrayIntIterator;
import java.util.List;
import java.util.ListIterator;

public class IntArrayCollection implements List<Integer> {
    private int CAPACITY = 100;
    private Integer[] arr = new Integer[CAPACITY];
    private int n = 0;


    @Override
    public boolean add(Integer i) {
        n++;

        if(n > CAPACITY){
            CAPACITY *= 1.25;
            Integer[] newArr = new Integer[CAPACITY];
            copy(newArr,arr);
            arr = newArr;
        }
        arr[n] = i;

        return true;
    }

    @Override
    public void add(int index, Integer element) {
        if(index > n){
            if(index > CAPACITY){
                CAPACITY *= 1.25;
                Integer[] newArr = new Integer[CAPACITY];
                copy(newArr,arr);
                arr = newArr;
            }
            n = index;
        }
        arr[index] = element;
    }

    @Override
    public int size() {
        return n;
    }

    @Override
    public boolean isEmpty() {
        return n == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (o instanceof Integer) {
            Integer i = (Integer) o;
            for (Integer x : arr) {
                if (x == i) {
                    return true;
                }
            }
            return false;
        } else {
            return false;
        }
    }

    @Override
    public boolean containsAll(Collection<?> coll) {
        for (Object o : coll) {
            if (!this.contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        boolean pr = false;
        for (Integer i : c) {
            pr = this.add(i) || pr;
        }
        return pr;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new ArrayIntIterator(arr, n);
    }

    @Override
    public boolean remove(Object o) {
        //Todo:remove method

        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {

        Iterator it = c.iterator();
        while (it.hasNext()){
            this.remove(it.next());
        }
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (Object elem: c ) {
            add(((Integer) elem));
        }
        return false;
    }

    @Override
    public void clear() {

        CAPACITY = 100;
        arr = new Integer[CAPACITY];
        n = 0;
    }

    @Override
    public Integer get(int index) {
        return arr[index];
    }

    @Override
    public Integer set(int index, Integer element) {

        arr[index] = element;

        return 0;
    }


    private void copy(Integer[] newArr, Integer[] arr) {

        for(int i = 0; i<arr.length;i++){
            newArr[i] = arr[i];
        }
    }

    @Override
    public Integer remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        return null;
    }

    @Override
    public ListIterator<Integer> listIterator(int index) {
        return null;
    }

    @Override
    public List<Integer> subList(int fromIndex, int toIndex) {
        return null;
    }



    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {
        return false;
    }


    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

}