package LinkedList;

public class LinkedList<T> {

	public class Node {
		T value;
		Node next;

		public Node(T value) {
			this.value = value;

		}
		public T getValue() {
			return value;
		}
	}


	public Node head;
	public Node tail;
	public int len = 0;

	public void addAtFront(T value) {

		Node node = new Node(value);
		node.next = head;
		head = node;
		len++;
	}

	public void addAtEnd(T value) {
		Node node = new Node(value);
		if(head == null) {
			head = node;
			tail = node;
		}
		else if(this.len == 1){
			head.next = node;
			tail = node;
		}
		else {
			tail.next = node;
			tail = node;
		}

		len++;
	}

	public void showList() {
		Node cursor = head;
		while (cursor != null) {
			System.out.print(cursor.value + "->");
			cursor = cursor.next;
		}
		System.out.println();
	}

	public void insertAt(int id, T value) throws Exception {
		Node node = new Node(value);
		int curId = 0;
		Node cursor = head;
		Node newNode;

		if(id >= len - 1 ) {
			throw new ListIndexOutOfBoundException();
		}
		else {
			if (id == 0) {
				node.next = head;
				head = node;
			}

			else {

				while (curId != id - 1) {
					cursor = cursor.next;
					curId++;
				}	
				newNode = cursor.next;
				node.next = newNode;
				cursor.next = node;


			}
			
		}
	}

	public void remove(int id) throws Exception {
		Node cursor = head;
		int curId = 0;
	

		if(id >= len - 1 ) {
			throw new ListIndexOutOfBoundException();
		}
		else {
			if (id == 0) {
				head = head.next;
			}
			else {
				while(curId!= id - 1) {
				cursor = cursor.next;
			}
			cursor.next = cursor.next.next; 
			}
		}
	}

	public T frontPop() throws ListIndexOutOfBoundException {
		if(head == null) {
			throw new ListIndexOutOfBoundException("Your list is empty!");
		}
		T value = head.value;
		head = head.next;
		len--;
		return value;
	}


	public static void main(String[] args) throws Exception {
		LinkedList<Integer> list = new LinkedList<Integer>();

		list.addAtEnd(5);
		list.addAtEnd(8);
		list.addAtFront(9);

		list.showList();
		list.remove(0);
		list.showList();
		list.insertAt(1, -1);

		list.showList();
	}
	
}


