package LinkedList;

public class ListIndexOutOfBoundException extends Exception {

	public ListIndexOutOfBoundException() {

	}

	public ListIndexOutOfBoundException(String message) {
		super(message);
	}
	
}