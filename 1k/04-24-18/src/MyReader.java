

public class MyReader {

    public static void main(String[] args) {
        Thread firstThread = new Thread(new ReaderRunnable("src/files/file.txt"));
        Thread secondThread = new Thread(new ReaderRunnable("src/files/file2.txt"));

        firstThread.start();
        secondThread.start();
    }
}
