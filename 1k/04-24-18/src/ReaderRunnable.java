

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ReaderRunnable implements Runnable{
    String fileName;

    public ReaderRunnable(String filename) {
        this.fileName = filename;
    }

    @Override
    public void run() {
        FileInputStream in = null;
        try {
            in = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(fileName.split("\\.")[0] + "Copy.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        byte[] buffer = new byte[48];

        try {
            while (in.available() > 0) {
                in.read(buffer);
                out.write(buffer);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

