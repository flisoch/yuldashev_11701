import java.io.*;


public class FileCloner {


    public static void copy(String fileName) throws IOException {
        FileInputStream in = new FileInputStream(fileName);
        FileOutputStream out = new FileOutputStream(fileName.split("\\.")[0] + "Copy.txt");

        byte[] buffer = new byte[48];

        while (in.available() > 0) {
            in.read(buffer);
            out.write(buffer);
        }

    }
}
