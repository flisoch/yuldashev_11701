package factory_method.creators;

import factory_method.creatures.Creature;
import factory_method.creatures.Nekochan;

public class NekochanCreator extends Creator {

    @Override
    public Creature create() {
        return new Nekochan();
    }

}
