package factory_method.creatures;

public class Nekochan implements Creature {

    @Override
    public void makeMove() {
        System.out.println("*Vilyaet hvostikom*");
    }
}