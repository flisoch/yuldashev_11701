package facade;



import facade.controller.ManorController;

public class Demo {

    public static void main(String[] args) {

        ManorController controller = new ManorController();
        controller.checkStable();
        controller.checkKitchen();
        controller.checkGreenhouse();
    }
}
