package adapter.adapter;

import adapter.models.Servant;

public class ReceptionAdapter implements ReceptionInterface {

    Servant servant;

    public ReceptionAdapter(Servant servant){
        this.servant = servant;
    }

    @Override
    public void makeDinner(){
        servant.cookDinner();
        servant.setTheTable();

    }

}
