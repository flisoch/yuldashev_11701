package proxy.proxy;

import proxy.auxiliaryClasses.Application;

public interface Component {
    void acceptRequestFromStudent(Application application);

}
