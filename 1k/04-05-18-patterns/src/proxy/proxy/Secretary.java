package proxy.proxy;

import proxy.auxiliaryClasses.*;

import java.util.Random;


public class Secretary implements Component {
    Dean dean = DeanSingleton.getInstance();

    @Override
    public void acceptRequestFromStudent(Application application) {
        if (checkRequest(application)) {
            dean.acceptRequestFromStudent(application);
        } else {
            System.out.println("some problems");
        }
    }

    private boolean checkRequest(Application application) {
        int num = new Random().nextInt(10);
        return num == 7 ? true : false;
    }
}
