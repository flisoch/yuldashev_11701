package proxy;

import proxy.auxiliaryClasses.Application;
import proxy.proxy.Component;
import proxy.proxy.Secretary;


public class Demo {
    //декан-объект, секретарь-его прокси.заявление до декана просто так не дойдёт

    public static void main(String[] args) {

        Component secretary = new Secretary();
        Application application = new Application();
        secretary.acceptRequestFromStudent(application);

    }

}
