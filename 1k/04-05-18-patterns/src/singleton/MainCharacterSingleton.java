package singleton;

public class MainCharacterSingleton {

    private static MainCharacter mainCharacter;

    public static MainCharacter getInstance() {
    	
        if(mainCharacter == null) {
            mainCharacter = new MainCharacter("Marry Sue");
        }
        return mainCharacter;
    }


}
