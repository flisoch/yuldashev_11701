package proxy.auxiliaryClasses;

import proxy.proxy.Dean;

public class DeanSingleton {
    static Dean dean;

    public static Dean getInstance(){
        if(dean == null) {
            dean = new Dean();
        }
        return dean;
    }
}
