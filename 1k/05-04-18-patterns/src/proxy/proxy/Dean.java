package proxy.proxy;

import proxy.auxiliaryClasses.Application;
import proxy.proxy.Component;

public class Dean implements Component {

    @Override
    public void acceptRequestFromStudent(Application application) {

    }
}
