package factory_method.creators;

import factory_method.creatures.Creature;
import factory_method.creatures.Person;

public class PersonCreator extends Creator {

    @Override
    public Creature create() {
        return new Person();
    }
}
