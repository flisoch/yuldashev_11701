package factory_method.creators;

import factory_method.creatures.Creature;

public abstract class Creator {

    public abstract Creature create();


}
