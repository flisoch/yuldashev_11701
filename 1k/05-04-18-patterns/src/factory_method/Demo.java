package factory_method;


import factory_method.creators.Creator;
import factory_method.creators.NekochanCreator;
import factory_method.creators.PersonCreator;
import factory_method.creatures.Creature;

public class Demo {


    public static void main(String[] args) {
        Creature creature1 = creator("nekochan").create();
        Creature creature2 = creator("person").create();

        creature1.makeMove();
        creature2.makeMove();
    }

    //конкретный создатель конфигурируется извне, не в конструкторе каком-нибудь
    public static Creator creator(String creature) {

        if(creature.equals("nekochan")){
            return new NekochanCreator();
        }
        else if(creature.equals("person")) {
            return new PersonCreator();
        }
        return null;
    }

}
