package factory_method.creatures;

public interface Creature {

    void makeMove();

}