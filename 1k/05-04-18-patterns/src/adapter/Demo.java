package adapter;

import adapter.adapter.ReceptionAdapter;
import adapter.adapter.ReceptionInterface;
import adapter.models.Nobleman;
import adapter.models.Servant;

public class Demo {
    // дворянин Андрейка устраивает приём. он хочет приготовить ужин, но не умеет

    public static void main(String[] args) {

        Nobleman Andreyka = new Nobleman();
        Servant babulya = new Servant();
        ReceptionInterface adapter = new ReceptionAdapter(babulya);

        Andreyka.setAdapter(adapter);
        Andreyka.adapter.makeDinner();


    }
}
