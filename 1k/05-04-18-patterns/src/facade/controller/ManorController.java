package facade.controller;

import facade.classes.*;

public class ManorController {

    public void checkStable(){
        Stable.checkHorses();
        Stable.checkFeed();
    }
    public void checkKitchen(){
        Kitchen.checkCleanliness();
        Kitchen.checkSupply();

    }

    public void checkGreenhouse(){
        Greenhouse.checkPlants();
        Greenhouse.checkTemperature();
    }

}
