package singleton;

public class MainCharacterSingleton {

    MainCharacter mainCharacter;

    public MainCharacter getInstance() {
        if(mainCharacter == null) {
            mainCharacter = new MainCharacter("Marry Sue");
        }
        return mainCharacter;
    }


}
