import java.util.ArrayList;

public class ArrayListCombSort {

    public static int sort(ArrayList<Integer> array) {
        int counter = 0;
        int n = array.size();
        int gap = n;
        boolean swapped = true;


        while(gap != 1 || swapped) {

            gap = defineGap(gap);
            swapped = false;
            for(int i = 0;i < n - gap;i++){
                counter++;
                if(array.get(i) > array.get(i+gap)){
                    swap(i,i+gap,array);
                    swapped = true;
                }
            }

        }
        return counter;
    }

    private static void swap(int i1, int i2, ArrayList<Integer> array) {
        int temp = array.get(i1);
        array.set(i1,array.get(i2));
        array.set(i2, temp);
    }

    private static int defineGap(int gap) {
        gap /= 1.247;
//        System.out.println(gap);
        if(gap < 1) {
            return 1;
        }
        return gap;
    }

}


