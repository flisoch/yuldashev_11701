import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class IntCombSort {

    public static int sort(int[] array) throws IOException {

        int iterations = 0;
        int n = array.length;
        int gap = n;
        boolean swapped = true;

        while(gap != 1 || swapped) {
            gap = defineGap(gap);
            swapped = false;

            for (int i = 0; i < n - gap; i++) {
                iterations++;
                if (array[i] > array[i + gap]) {
                    swap(i, i + gap, array);
                    swapped = true;
                }
            }
        }
        return iterations;
    }

    private static void swap(int i1, int i2, int[] array) {
        int temp = array[i1];
        array[i1] = array[i2];
        array[i2] = temp;
    }

    private static int defineGap(int gap) {
        gap /= 1.247;
//        System.out.println(gap);
        if(gap < 1) {
            return 1;
        }
        return gap;
    }

}
