import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class LinkedListCombSort {


    public static int sort(LinkedList<Integer> list) {
        int n = list.size();
        int gap = n;
        boolean swapped = true;
        int counter = 0;
        while(gap != 1 || swapped) {

            gap = defineGap(gap);
            swapped = false;

            ListIterator<Integer> j = list.listIterator(gap);
            ListIterator<Integer> i = list.listIterator();

            while (j.hasNext()) {
                counter++;

                if(j.next() < i.next()) {
                    j.previous();
                    i.previous();
                    swap(i,j);
                    i.next();
                    j.next();
                    swapped = true;
                }
            }

        }
        return counter;
    }

    private static void swap(final ListIterator<Integer> i,
                             final ListIterator<Integer> j) {
        int tmp = i.next();
        i.previous();
        i.set(j.next());
        j.previous();
        j.set(tmp);
    }

    private static int defineGap(int gap) {
        gap /= 1.247;
//        System.out.println(gap);
        if(gap < 1) {
            return 1;
        }
        return gap;
    }
}
