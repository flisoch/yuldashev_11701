import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class NumberGenerator {
    public static void main(String[] args) throws IOException {
        FileWriter fw = new FileWriter(new File("res/andrey/arrays.txt"));
        Random random = new Random();
        int length;
        for (int i = 0;i < 75;i++) {
            length = random.nextInt(99_900)+100;
            for (int j = 0;j<length;j++) {
                fw.write(random.nextInt() + " ");
            }
            fw.write("\n");
        }
        fw.flush();
        fw.close();
    }
}
