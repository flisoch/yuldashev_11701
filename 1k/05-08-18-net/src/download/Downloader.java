package download;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.*;
import java.util.regex.Pattern;

public class Downloader {

    private String url;
    private String extension;
    private final static String RE_TEMPLATE_FOR_URL = "";
    private final String reForUrl;


    private String buildPatternForUrl() {
        return "(?<url>(https?|ftp)://[^\\s/$.?#].\\S*\\." +
                extension +
                ")";
    }

    public Downloader(String url,String extension){
        this.url = url;
        this.extension = extension;
        this.reForUrl = builtReForURL();

    }

    public void start() {
        Pattern p = Pattern.compile(this.reForUrl);

        try {
            URL urlObj = new URL(this.url);
            LineNumberReader reader = new LineNumberReader(new InputStreamReader(urlObj.openStream()));

            // for every line look for matches
            String line = reader.readLine();
            while (line != null) {
                Matcher m = p.matcher(line);
                // for every match download file
                while (m.find()){
                    String fileURL = m.group();
                    (new FileDownloader(fileURL)).download();
                }

                line = reader.readLine();
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }



    }

}
