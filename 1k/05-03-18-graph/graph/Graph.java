package graph;

public class Graph {


    public static void main(String[] args) {
        AdjMatrixGraph adjMatrix = new AdjMatrixGraph("collections/graph/graphAsMatrix.txt");
        AdjListGraph adjList = new AdjListGraph("collections/graph/graphAsList.txt");
        System.out.println(adjMatrix);
        System.out.println(adjMatrix.asAdjList());

        System.out.println(adjList);
        System.out.println(adjList.asAdjMatrix());
    }
}
