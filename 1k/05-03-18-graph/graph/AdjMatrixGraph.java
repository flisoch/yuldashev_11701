package graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

public class AdjMatrixGraph {
    private int vertexAmount;
    private int [][] matrix;

    public AdjMatrixGraph(String fileName){
        try {
            Scanner sc = new Scanner(new File(fileName));
            vertexAmount = sc.nextInt();
            matrix = new int[vertexAmount][vertexAmount];
            fillInMatrix(sc);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public AdjMatrixGraph(int[][] matrix){
        this.matrix = matrix;
    }

    private void fillInMatrix(Scanner sc) {

        for(int i = 0; i < vertexAmount;i++){
            for (int j = 0;j < vertexAmount; j++){
                matrix[i][j] = sc.nextInt();
            }
        }
    }

    public AdjListGraph asAdjList(){
        LinkedList[] arrOfVertexes = new LinkedList[vertexAmount];

        for(int i = 0; i < matrix.length;i++) {
            LinkedList<Vertex> ll =  new LinkedList<>();
            for(int j = 0; j < matrix.length;j++){
                if(matrix[i][j] == 1){
                    ll.add(new Vertex(j));
                }
            }
            arrOfVertexes[i] = ll;
        }
        return new AdjListGraph(arrOfVertexes);
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < matrix.length;i++) {
            for(int j = 0; j < matrix.length;j++){
                sb.append(matrix[i][j]);
                sb.append(' ');
            }
            sb.append('\n');
        }

        return sb.toString();
    }
}
