package graph;

import java.io.*;
import java.util.LinkedList;
import java.util.Scanner;

public class AdjListGraph {
    private int vertexAmount;
    private LinkedList<Vertex>[] arrOfVertexes;

    public AdjListGraph(String fileName){
        try {
            Scanner sc = new Scanner(new File(fileName));
            vertexAmount = sc.nextInt();
            System.out.println(sc.nextLine());
            arrOfVertexes = new LinkedList[vertexAmount];
            fillInArrOfVertexes(sc);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public AdjListGraph(LinkedList<Vertex>[] arrOfVertexes){
        this.arrOfVertexes = arrOfVertexes;
    }

    private void fillInArrOfVertexes(Scanner sc) {
        int i = 0;
        while(sc.hasNext()){
            LinkedList<Vertex> ll = new LinkedList<>();
            String[] neighboursOfCurrentVertex = sc.nextLine().split(" ");

            if(neighboursOfCurrentVertex.length > 0) {
                for(String s: neighboursOfCurrentVertex){
                    if(!s.equals("")){
                        ll.add(new Vertex(Integer.parseInt(s)));
                    }
                }
                arrOfVertexes[i] = ll;
                i++;
            }

        }
    }

    public AdjMatrixGraph asAdjMatrix(){
        int[][] matrix = new int[vertexAmount][vertexAmount];
        for(int i = 0; i < vertexAmount;i++){
            for(int j = 0; j < vertexAmount; j++){
                matrix[i][j] = 0;
            }
        }
        for(int i = 0;i < arrOfVertexes.length;i++){
            for(Vertex v: arrOfVertexes[i]){
                matrix[i][v.getIndex()] = 1;
            }
        }
        return new AdjMatrixGraph(matrix);
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < arrOfVertexes.length;i++){
            for(Vertex v:arrOfVertexes[i]){
                sb.append(v.getIndex());
                sb.append(' ');
            }
            sb.append('\n');
        }
        return sb.toString();
    }
}
