package queue;

public class StackOnQueues<T> {

    LinkedQueue<T> q1 = new LinkedQueue<>();
    LinkedQueue<T> q2 = new LinkedQueue<>();


    public static void main(String[] args) throws Exception {
        StackOnQueues<Integer> st = new StackOnQueues<>();
        st.add(1);
        st.add(2);
        st.add(3);
        st.add(4);
        System.out.println(st.pop());       //output 4
        st.add(5);
        System.out.println(st.pop());       //output 5
        st.add(6);
        st.add(7);
        System.out.println(st.pop());       //output 7
        System.out.println(st.pop());       //output 6
        System.out.println(st.pop());       //output 3
        System.out.println(st.pop());       //output 2
        System.out.println(st.pop());       //output 1
        System.out.println(st.pop());       //exception,cause it's empty
    }

    private T pop() throws Exception {

        while (q1.length() != 1) {
            q2.offer(q1.pull());

        }
        T value = q1.pull();

        LinkedQueue<T> q = new LinkedQueue<>();
        q1 = q2;
        q2 = q;
        return value;
    }

    private void add(T value) {
        q1.offer(value);

    }
}
