package queue;

public class Divisors235 {

    public static void main(String[] args) throws Exception {
        getNumsLessThanAThatAreDividedByBCD(100,2,3,5);
    }

    private static void getNumsLessThanAThatAreDividedByBCD(int ceiling, int d1, int d2, int d3) throws Exception {

        LinkedQueue<Integer> q2 = new LinkedQueue<>();
        q2.offer(d1);
        LinkedQueue<Integer> q3 = new LinkedQueue<>();
        q3.offer(d2);
        LinkedQueue<Integer> q5 = new LinkedQueue<>();
        q5.offer(d3);
        int num = 0;
        while (num < ceiling) {
            num = popMin(q2, q3, q5);
            q2.offer(d1 * num);
            q3.offer(d2 * num);
            q5.offer(d3 * num);
        }
    }

    public static int popMin(LinkedQueue<Integer> q2, LinkedQueue<Integer> q3, LinkedQueue<Integer> q5) throws Exception {
        int min = 0;
        if (q2.peek() <= q3.peek() && q2.peek() <= q5.peek()) {
            min = q2.pull();
        } else if (q3.peek() <= q2.peek() && q3.peek() <= q5.peek()) {
            min = q3.pull();
        } else if (q5.peek() <= q2.peek() && q5.peek() <= q3.peek()) {
            min = q5.pull();
        }

        //удаляем повторяющиеся, но в пустой очереди конечно ничего нет,нечему повторяться

        if(!q2.isEmpty()){
            if (q2.peek() == min) {
                q2.pull();
            }
        }

        if(!q5.isEmpty()){
            if (q5.peek() == min) {
                q5.pull();
            }
        }

        if(!q3.isEmpty()){
            if (q3.peek() == min) {
                q3.pull();
            }
        }

        System.out.println(min);
        return min;
    }
    
}
