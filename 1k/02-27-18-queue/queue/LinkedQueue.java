package queue;
import LinkedList.*;

public class LinkedQueue<T> {

    LinkedList<T> ll = new LinkedList<T>();

    public void offer(T value) {       //nea,tail,   gde head?
       ll.addAtEnd(value);
    }

    public T pull() throws Exception {

        return ll.frontPop();
    }

    // element, peek, - узнать, pull, remove - узнать и забрать
    public T peek() throws Exception {

        T popped = ll.frontPop();
        ll.addAtFront(popped);
        return popped;
    }

    public boolean isEmpty() {
        return ll.head == null;
    }

    public static void main(String[] args) throws Exception {
        LinkedQueue<Integer> liq = new LinkedQueue<>();
        liq.offer(5);
        liq.offer(6);
        liq.offer(7);
        liq.offer(8);
        liq.offer(9);
        System.out.println(liq.pull());
        System.out.println(liq.pull());
        System.out.println(liq.pull());
        System.out.println(liq.pull());
        System.out.println(liq.pull());

        System.out.println(liq.isEmpty());



    }

    public int length() {
        return ll.len;
    }

}
