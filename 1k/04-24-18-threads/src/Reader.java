
import java.io.IOException;

public class Reader implements Runnable {
    private String src;
    private String fileName;
    private String dest;

    public Reader(String src, String fileName, String dest) {
        this.src = src;
        this.fileName = fileName;
        this.dest = dest;
    }


    @Override
    public void run() {
        try {
            FileCloner.copy(src, fileName, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

