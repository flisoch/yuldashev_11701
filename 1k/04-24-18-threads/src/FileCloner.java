import java.io.*;


public class FileCloner {


    public static void copy(String src, String fileName, String dest) throws IOException {
        FileInputStream in = null;
        try {
            in = new FileInputStream(src + fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(new File(dest + fileName.split("\\.")[0] + "Copy.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        byte[] buffer = new byte[48];

        try {
            while (in.available() > 0) {
                int i = in.read(buffer);
                out.write(buffer, 0, i);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
