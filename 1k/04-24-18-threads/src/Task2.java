import java.util.ArrayList;
import java.util.List;

public class Task2 {

    public static void main(String[] args) {
        List<String> fileNames = new ArrayList<>();
        fileNames.add("file.txt");
        fileNames.add("file2.txt");
        fileNames.add("file3.txt");
        fileNames.add("file4.txt");
        String dir = "res/copied/";
        String src = "res/";
        copy(src, fileNames, dir);

    }

    private static void copy(String src, List<String> fileNames, String dir) {
        for (String fileName : fileNames) {
            new Thread(new Reader(src, fileName, dir)).start();

        }
    }
}
