package Stack;

import LinkedList.*;

public class LinkedStack<T> {

    LinkedList<T> ll = new LinkedList<T>();

    public void push(T value) {
      ll.addAtEnd(value);
    }

    public T pop() throws ListIndexOutOfBoundException {
       return ll.frontPop();
    }


    public boolean isEmpty() {
        return ll.head == null;
    }
}
