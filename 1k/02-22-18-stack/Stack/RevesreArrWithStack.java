package Stack;

import LinkedList.ListIndexOutOfBoundException;

public class RevesreArrWithStack {
    public static void main(String[] args) throws ListIndexOutOfBoundException {
        int[] numbers = {0,1,2,3,4,5,6,7,8,9};
        LinkedStack<Integer> lis = new LinkedStack<>();


        showNumbers(numbers);

        for (int num: numbers) {
            lis.push(num);
        }

        for (int i = 0;i < numbers.length;i++) {
            numbers[i] = lis.pop();
        }
        showNumbers(numbers);

    }

    public static void showNumbers(int[] numbers) {
        for (int num: numbers) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
}
