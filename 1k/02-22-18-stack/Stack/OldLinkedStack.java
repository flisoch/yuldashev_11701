package Stack;

public class OldLinkedStack<T> {

    class Node{
        T value;
        Node next;

        Node(T value) {
            this.value = value;
        }
    }

    Node head;

    public void push(T value) {
        Node n = new Node(value);
        n.next = head;
        head = n;
    }

    public T pop() {
        T x = head.value;
        head = head.next;
        return x;
    }


    public boolean isEmpty() {
        return head == null;
    }
}
