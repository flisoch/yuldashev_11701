package Stack;


import LinkedList.ListIndexOutOfBoundException;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

// works only with correct inputs

public class PostfixCalculating {
    static List<String> operations = Arrays.asList("+","-","/","*");

    public static void main(String[] args) throws IOException, ListIndexOutOfBoundException {
        String[] expressions = {"2,5,4,+,*",                //(5+4)*2         => 18
                                "2,6,4,+,/,8,3,+,*,",       //(3+8)*((4+6)/2) => 55
                                "2,5,4,3,+,+,/"             //(3+4+5)/2       => 6
        };

        for(String expr: expressions) {
            System.out.println(execute(expr));
        }

    }

    private static double execute(String expr) throws IOException, ListIndexOutOfBoundException {
        LinkedStack<String> lcs = new LinkedStack<>();
        String[] pieces = expr.split(",");

        for(int i = 0; i< pieces.length;i++) {
            String curPiece = pieces[i];
            if(operations.contains(curPiece)) {
                double a = Double.parseDouble(lcs.pop());
                double b = Double.parseDouble(lcs.pop());
                switch (curPiece) {
                    case "+":
                        lcs.push(""+ (a + b));
                        break;
                    case "-":
                        lcs.push(""+ (a - b));
                        break;
                    case "*":
                        lcs.push(""+ (a * b));
                        break;
                    case "/":
                        lcs.push(""+ (a / b));
                        break;


                }
            }
            else {
                lcs.push(curPiece);
            }

        }

        return Double.parseDouble(lcs.pop());
    }


}