package Stack;

import LinkedList.ListIndexOutOfBoundException;

public class bracketsArrangement {
//нужно и описывать ошибку(каакя скобка не та)

    public static void main(String[] args) throws ListIndexOutOfBoundException {
        String[] testStrings = {"([)]", "(){()[()]}", "{}{[]([])"};   //false,true,false
        for (String s : testStrings) {
            System.out.println(correctness(s));
        }

    }

    private static boolean correctness(String s) throws ListIndexOutOfBoundException {

        LinkedStack<Character> st = new LinkedStack<>();
        for(int i =0;i < s.length();i++) {

            char chr = s.charAt(i);

            if(chr == '(' || chr == '{' || chr == '[') {
                st.push(chr);
            }
            else if(chr == ')' || chr == '}' || chr == ']') {

                char popped = st.pop();
                switch (chr) {
                    case ')':
                        if (popped != '(') {return false;}
                        break;
                    case ']':
                        if (popped != '[') {return false;}
                        break;
                    case '}':
                        if (popped != '{'){return false;}
                        break;
                }

            }
        }
        return st.isEmpty();
    }


}
