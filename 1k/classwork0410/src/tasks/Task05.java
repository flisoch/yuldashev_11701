package tasks;

import collection.singletons.CarMakerCollectionSingleton;
import collection.singletons.ModelCollectionSingleton;
import entities.CarMaker;
import entities.Model;

import java.util.List;
import java.util.stream.Collectors;

public class Task05 {
    public static void main(String[] args) {
        List<CarMaker> carMakers = CarMakerCollectionSingleton.getInstance();
        List<Model> models = ModelCollectionSingleton.getInstance();

        for(CarMaker carMaker: carMakers) {
            System.out.println(carMaker.getName());
            System.out.println(carMaker.averageHorsePower());
        }

        System.out.println(
                carMakers.stream()
                .collect(
                        Collectors.toMap(
                                x -> x.getFullName(),
                                CarMaker::averageHorsePower
                        )
                )
        );

        System.out.println(
                models.stream()
                .collect(
                        Collectors.toMap(
                                Model::getModel,
                                Model::averageHorsePower
                        )
                )
        );




    }
}
