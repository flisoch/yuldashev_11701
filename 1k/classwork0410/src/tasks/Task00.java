package tasks;

import collection.singletons.WordDataCollectionSingleton;
import entities.WordData;

import java.util.List;

public class Task01 {
    public static String getWordWithMaxUsageLoop() {
        List<WordData> wdc = WordDataCollectionSingleton.getInstance();
        if (wdc.size() == 0) {
            return null;
        }

        WordData max = wdc.get(0);
        for (WordData wd : wdc) {
            if (wd.count > max.count) {
                max = wd;
            }
        }

        return max.word;
    }

    public static String getWordWithMaxUsage() {
        return WordDataCollectionSingleton.getInstance()
                .stream()
                .reduce((wd1, wd2) -> wd1.count > wd2.count ? wd1 : wd2)
                .orElse(new WordData("", 0))
                .word
        ;
    }

    public static void main(String[] args) {
        System.out.println(getWordWithMaxUsage());
    }
}
