package tasks;

import collection.singletons.CountryCollectionSingleton;
import entities.CarMaker;
import entities.Country;
import entities.Model;

import java.util.List;



public class Task03 {
    public static void main(String[] args) {
        List <Country> countries = CountryCollectionSingleton.getInstance();

        for (Country country:countries) {
            System.out.println(country.countModels());
        }

        showModelsAmountForEachCountry(countries);
    }

    private static void showModelsAmountForEachCountry(List<Country>countries) {
        for(Country country:countries) {
            List<CarMaker> carMakers = country.getCarMakersList();
            for(CarMaker carMaker: carMakers) {
                List<Model> models = carMaker.getModelList();
                System.out.println(models.size());
            }
        }
    }



}
