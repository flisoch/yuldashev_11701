package tasks;

import collection.singletons.CarMakerCollectionSingleton;
import collection.singletons.ContinentCollectionSingleton;
import entities.CarMaker;
import entities.Continent;

import java.util.*;

public class Task02 {

    public static void main(String[] args) {

        Continent continent = ContinentCollectionSingleton.getInstance()
                .stream()
                .filter((cnt)-> cnt.getId() == 2)
                .findAny()
                .get();


        List<CarMaker> carMakersFromEurope = carMakersFromContinent(continent);
        for (CarMaker cm:carMakersFromEurope){
            System.out.println(cm.getName());
        }

    }

    public static List<CarMaker> carMakersFromContinent(Continent continent){
        List<CarMaker> cm = CarMakerCollectionSingleton.getInstance();
        List<CarMaker> carMakersFromContinent = new ArrayList<>();

        for (CarMaker carMaker : cm) {
            if (carMaker.getCountry().continent.getId() == continent.getId()) {
                carMakersFromContinent.add(carMaker);
            }
        }
        return carMakersFromContinent;
    }
}
