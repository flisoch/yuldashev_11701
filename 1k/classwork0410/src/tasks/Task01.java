package tasks;

import collection.singletons.ContinentCollectionSingleton;
import entities.Continent;

import java.util.List;

public class maxCountries {
    public static void main(String[] args) {
        System.out.println(biggestContinent().getName());
    }

    public static Continent biggestContinent(){
        List<Continent> continents = ContinentCollectionSingleton.getInstance();

        Continent biggestContinent = continents.get(0);
        for(Continent continent: continents) {
            if(continent.CountriesAmount() > biggestContinent.CountriesAmount()) {
                biggestContinent = continent;
            }
        }
        return biggestContinent;
    }

    public static Continent biggestContinentWithStreamAPI(){
        List<Continent> continents = ContinentCollectionSingleton.getInstance();

        Continent biggestContinent;

        biggestContinent = continents
                .stream()
                .reduce((s1, s2) -> s1.CountriesAmount() > s2.CountriesAmount() ? s1:s2)
                .get();

        return biggestContinent;
    }
}
