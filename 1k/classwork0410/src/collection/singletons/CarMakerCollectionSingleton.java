package collection.singletons;

import collection.Reader;
import entities.CarMaker;

import java.util.List;

public class CarMakerCollectionSingleton {
    private static final String FILE_NAME = "res/CARS/car-makers.csv";
    private static List<CarMaker> instance;

    public static List<CarMaker> getInstance() {
        if (instance == null) {
            instance = Reader.readCarMakers(FILE_NAME);
        }
        return instance;
    }
}
