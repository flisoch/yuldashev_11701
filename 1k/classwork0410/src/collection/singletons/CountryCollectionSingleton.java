package collection.singletons;

import collection.Reader;
import entities.Continent;
import entities.Country;

import java.util.List;

public class CountryCollectionSingleton {
    private static final String FILE_NAME = "res/CARS/countries.csv";
    private static List<Country> instance;

    public static List<Country> getInstance() {
        if (instance == null) {
            instance = Reader.readCountries(FILE_NAME);
        }
        return instance;
    }
}
