package collection.singletons;

import collection.Reader;
import entities.Model;

import java.util.List;

public class ModelCollectionSingleton {
    static List <Model> models;


    public static List<Model> getInstance() {
        if (models == null) {
            models = Reader.getModelList("res/CARS/model-list.csv");
        }
        return models;
    }
}
