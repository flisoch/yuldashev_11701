package collection.singletons;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

import entities.WordData;

public class WordDataCollectionSingleton {
    private static List<WordData> wordDataList;
    private static final String FILE_NAME = "res/en_v1.dic";

    public static List<WordData> getInstance() {
        if (wordDataList == null) {
            wordDataList = readWordDataList();
        }
        return wordDataList;
    }

    private static List<WordData> readWordDataList() {
        List<WordData> list;
        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILE_NAME)
                    )
            );
            list = new LinkedList<>();

            String line = bufferedReader.readLine();
            while (line != null) {
                try {
                    String[] strings = line.split(" ");
                    wordDataList.add(
                            new WordData(
                                    strings[0],
                                    Integer.parseInt(strings[1])
                            )
                    );
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            list = null;
            e.printStackTrace();
        }
        return list;
    }

}
