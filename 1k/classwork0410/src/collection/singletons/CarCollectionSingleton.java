package collection.singletons;

import collection.Reader;
import entities.Car;
import entities.CarMaker;

import java.util.List;

public class CarCollectionSingleton {
    private static final String FILE_NAME = "res/CARS/cars-data.csv";
    private static List<Car> instance;

    public static List<Car> getInstance() {
        if (instance == null) {
            instance = Reader.readCars(FILE_NAME);
        }
        return instance;
    }
}