package collection;

import collection.singletons.*;
import entities.*;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class Reader {
    private static String trim(String str) {
        return (String) Stream.of(str.split("'"))
                .filter((str1) -> !str1.equals(""))
                .toArray()[0]
                ;
    }

    public static void main(String[] args) {

        getModelList("res/CARS/model-list.csv");
        readCarNames("res/CARS/car-names.csv");
    }

    public static List<Continent> readContinents(final String FILE_NAME) {
        List<Continent> continents;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILE_NAME)
                    )
            );
            continents = new LinkedList<>();

            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                try {
                    String[] strings = line.split(",");
                    continents.add(
                            new Continent(
                                    Integer.parseInt(strings[0]),
                                    trim(strings[1])
                            )
                    );
                } catch (Exception e) {
                    e.printStackTrace();
                }
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            continents = null;
            e.printStackTrace();
        }
        return continents;
    }

    public static List<Country> readCountries(final String FILE_NAME) {
        List<Country> countries;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILE_NAME)
                    )
            );
            countries = new LinkedList<>();

            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                try {
                    String[] strings = line.split(",");

                    int id = Integer.parseInt(strings[0]);
                    String name = trim(strings[1]);

                    Continent continent = null;
                    if (!strings[2].equals("null")) {
                        int continentId = Integer.parseInt(strings[2]);
                        // find continent by id
                        continent = ContinentCollectionSingleton.getInstance().stream().filter((continent1 -> continent1.getId() == continentId)).findAny().get();
                    }

                    Country country = new Country(id, name, continent);
                    countries.add(country);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            countries = null;
            e.printStackTrace();
        }
        return countries;
    }

    public static List<CarMaker> readCarMakers(final String FILE_NAME) {
        List<CarMaker> carMakers;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILE_NAME)
                    )
            );
            carMakers = new LinkedList<>();

            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                try {
                    String[] strings = line.split(",");

                    int id = Integer.parseInt(strings[0]);

                    Country country = null;
                    if (!strings[3].equals("null")) {
                        int countryId = Integer.parseInt(strings[3]);
                        // find country by id
                        country = CountryCollectionSingleton.getInstance().stream().filter((country1 -> country1.id == countryId)).findAny().get();
                    }
                    CarMaker carMaker = new CarMaker(id, trim(strings[1]), trim(strings[2]), country);
                    carMakers.add(carMaker);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            carMakers = null;
            e.printStackTrace();
        }
        return carMakers;
    }

    public static List<Model> getModelList(final String FILE_NAME) {
        List<Model> models = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILE_NAME)));

            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                String[] strings = line.split(",");

                int id = Integer.parseInt(strings[0]);
                int makerId = Integer.parseInt(strings[1]);
                CarMaker carmaker;
                carmaker = CarMakerCollectionSingleton
                        .getInstance()
                        .stream()
                        .filter(carMaker -> carMaker.getId() == makerId)
                        .findAny()
                        .get();
                String model = trim(strings[2]);

                Model object = new Model(id, carmaker, model);
                models.add(object);

                line = bufferedReader.readLine();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return models;
    }

    public static List<Car> readCars(final String FILE_NAME) {
        List<Car> cars = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILE_NAME)));

            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();

            while (line != null) {
                String[] strings = line.split(",");
                int id = Integer.parseInt(strings[0]);

                double MPG = 0;
                if (!strings[1].equals("null")) {
                    MPG = Double.parseDouble(strings[1]);
                }
                int cylinders = Integer.parseInt(strings[2]);
                double edispl = 0;
                if (!strings[3].equals("null")) {
                    edispl = Double.parseDouble(strings[3]);         //null
                }
                int horsepower = 0;
                if (!strings[4].equals("null")) {
                    horsepower = Integer.parseInt(strings[4]);
                }

                int weight = Integer.parseInt(strings[5]);
                double accelerate = Double.parseDouble(strings[6]);
                int year = Integer.parseInt(strings[7]);

                // рандомно не находит одну любую запись. get() уже не срабатывает
                CarName carName = CarNameCollectionSingleton
                        .getInstance()
                        .stream()
                        .filter(car -> car.getId() == id)
                        .findFirst().get();
                if (carName != null) {
                    Car car = new Car(carName, MPG, cylinders, edispl, horsepower, weight, accelerate, year);
                    cars.add(car);
                }


                line = bufferedReader.readLine();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return cars;
    }

    public static List<CarName> readCarNames(String fileName) {
        List<CarName> carNames;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(fileName)
                    )
            );
            carNames = new LinkedList<>();

            String line = bufferedReader.readLine();
            line = bufferedReader.readLine();
            while (line != null) {
                try {
                    String[] strings = line.split(",");

                    int id = Integer.parseInt(strings[0]);
                    String modelName = trim(strings[1]);
                    String make = trim(strings[2]);

                    Model model = ModelCollectionSingleton.getInstance()
                            .stream()
                            .filter((model1 -> model1.getModel().equals(modelName))).findAny().get();

                    CarName carName = new CarName(id, model, make);
                    carNames.add(carName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            carNames = null;
            e.printStackTrace();
        }
        return carNames;


    }
}
