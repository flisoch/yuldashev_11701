package entities;

import collection.singletons.CarCollectionSingleton;

public class Model {
    private int modelId;
    private CarMaker maker;
    private String model;

    public Model(int modelId, CarMaker maker, String model) {
        this.modelId = modelId;
        this.maker = maker;
        this.model = model;
    }

    public int getModelId() {
        return modelId;
    }

    public CarMaker getMaker() {
        return maker;
    }

    public String getModel() {
        return model;
    }

    public double averageHorsePower(){
        double averHP = (int) CarCollectionSingleton.getInstance()
                .stream()
                .filter(car -> car.getCarName().getModel().getModelId() == this.getModelId())
                .mapToInt(car -> car.getHorsepower())
                .average()
                .getAsDouble();

        return averHP;
    }
}
