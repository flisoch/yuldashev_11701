package entities;

import collection.singletons.CountryCollectionSingleton;

import java.util.List;

public class Continent {
    private int id;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    private String name;

    public Continent(int id, String name) {
        this.id = id;
        this.name = name;
    }

   /* public int CountriesCount() {
        List<Country> countries = CountryCollectionSingleton.getInstance();
        int count = 0;
        for (Country country:countries) {
            if(country.continent.id == this.id) {
                count++;
            }
        }
        return count;
    }*/

    public int CountriesCount() {
        List<Country> countries = CountryCollectionSingleton.getInstance();
        return (int)countries
                .stream()
                .filter(country -> country.continent.id ==this.id)
                .count();


    }


}
