package entities;

import collection.singletons.CarMakerCollectionSingleton;
import collection.singletons.ModelCollectionSingleton;

import java.util.List;
import java.util.stream.Collectors;


public class Country {
    public int id;
    public String name;
    public Continent continent;


    public Country(int id, String name, Continent continent) {
        this.id = id;
        this.name = name;
        this.continent = continent;
    }

    public int countModels(){
        int count = (int) ModelCollectionSingleton.getInstance()
                .stream()
                .filter(model -> model.getMaker().getCountry().id == this.id)
                .count();
        return count;
    }



    List<CarMaker> carMakersList;
    public List<CarMaker> getCarMakersList() {
        if (carMakersList == null) {
            carMakersList = CarMakerCollectionSingleton.getInstance()
                    .stream()
                    .filter(carMaker -> carMaker.getCountry().id == this.id)
                    .collect(Collectors.toList());

        }
        return carMakersList;

    }


}
