package entities;

public class Car {
    private CarName carName;
    private double MPG;
    private int cylinders;
    private double edispl;
    private int horsepower;
    private int weight;
    private double accelerate;
    private int year;

    public Car(CarName carName, double MPG, int cylinders, double edispl, int horsepower, int weight, double accelerate, int year) {
        this.carName = carName;
        this.MPG = MPG;
        this.cylinders = cylinders;
        this.edispl = edispl;
        this.horsepower = horsepower;
        this.weight = weight;
        this.accelerate = accelerate;
        this.year = year;
    }

    public CarName getCarName() {
        return carName;
    }

    public double getMPG() {
        return MPG;
    }

    public int getCylinders() {
        return cylinders;
    }

    public double getEdispl() {
        return edispl;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public int getWeight() {
        return weight;
    }

    public double getAccelerate() {
        return accelerate;
    }

    public int getYear() {
        return year;
    }
}
