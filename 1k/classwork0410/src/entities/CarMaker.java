package entities;

import collection.singletons.CarCollectionSingleton;
import collection.singletons.ModelCollectionSingleton;

import java.util.List;
import java.util.stream.Collectors;

public class CarMaker {
    private int id;
    private String name;
    private String fullName;

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public Country getCountry() {
        return country;
    }

    public int getId() {

        return id;
    }

    private Country country;

    public CarMaker(int id, String name, String fullName, Country country) {
        this.id = id;
        this.name = name;
        this.fullName = fullName;
        this.country = country;
    }


    List<Model> modelList;

    public List<Model> getModelList() {
        if (modelList == null) {
            modelList = ModelCollectionSingleton.getInstance()
                    .stream()
                    .filter(model -> model.getMaker().id == this.id)
                    .collect(Collectors.toList());
        }
        return modelList;
    }

    public int countModelsBeforeYear(int year){
        int count = (int) CarCollectionSingleton.getInstance()
                .stream()
                .filter(car -> car.getYear() <= year)
                .filter(car ->car.getCarName().getModel().getMaker().id == this.id)
                .count();
        return count;
    }

    public double averageHorsePower(){
        // не все модели существуют в базе, поэтому не подсчитается для какого-то мейкера ср.Хорспава
        double averHP =  CarCollectionSingleton.getInstance()
                .stream()
                .filter(car -> car.getCarName().getModel().getMaker().id == this.id)
                .mapToInt(car -> car.getHorsepower())
                .average()
                .getAsDouble();

        return averHP;
    }
}
