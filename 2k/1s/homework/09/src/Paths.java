public class Paths {

    public static String templatesPath = "res/templates/";
    public static String DbPath = "res/DB/";

    public static String feedHtmlPath = templatesPath + "feed.html";
    public static String messagesHtmlPath = templatesPath + "messages.html";
    public static String profileHtmlPath = templatesPath + "profile.html";
    public static String messagesPath = DbPath + "messages.txt";
    public static String postsPath = DbPath + "posts.txt";
    public static String usersPath = DbPath + "users.txt";
}
