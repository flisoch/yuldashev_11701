import entities.Post;

import java.io.IOException;
import java.util.List;

public class Posts{

	private static List<Post> posts;

	public static List<Post> getInstance() throws IOException {
		if(posts == null){
			posts = Mapper.readPosts(Paths.postsPath);
		}
		return posts;
	}
}