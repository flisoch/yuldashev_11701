import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandHandler {

    public void run() throws IOException {

        HtmlGenerator generator = new HtmlGenerator();

        Scanner sc = new Scanner(System.in);
        String command = sc.nextLine();

        Pattern idPattern = Pattern.compile("/id[1-9][0-9]*");

        while(!command.equals("exit()")){

            switch(command){
                case "/feed":
                    generator.generateFeed();
                    break;
                case "/messages":
                    generator.generateMessages();
                    break;

                default:
                    Matcher m = idPattern.matcher(command);
                    if(m.matches()){
                        Integer id = Integer.parseInt(command.split("/id")[1]);
                        generator.generateProfile(id);
                    }
                    else{
                        showCommands();
                    }
            }
            command = sc.nextLine();
        }
    }


    public void showCommands(){

        System.out.println("/feed \n"+
                "/id{number} \n"+
                "/messages \n");
    }
}
