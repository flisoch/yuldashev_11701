import entities.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Mapper{

	//todo: Mapper with 1 method, usingreflection
	
	public static List<User> readUsers(String fileName) throws IOException {

		List<User> users = new ArrayList<>();

		BufferedReader bf = new BufferedReader(new FileReader(fileName));
		while(bf.ready()){
			String line = bf.readLine();
			String[] user = line.split(" ");
			Integer id = Integer.parseInt(user[0]);
			String name = user[1];
			users.add(new User(id,name));
		}

		return users;
	}

	public static List<Message> readMessages(String fileName) throws IOException {

		List<Message> messages = new ArrayList<>();

		BufferedReader bf = new BufferedReader(new FileReader(fileName));
		while(bf.ready()){
			String line = bf.readLine();
			String[] message = line.split(" ");
			Integer id = Integer.parseInt(message[0]);
			Integer receiverId = Integer.parseInt(message[1]);
			Integer senderId = Integer.parseInt(message[2]);
			String text = message[3];
			User receiver = Users.findById(receiverId,"anon");
			User sender = Users.findById(senderId,"anon");

			messages.add(new Message(id, receiver, sender, text));
		}

		return messages;
	}

	public static List<Post> readPosts(String fileName) throws IOException {

		List<Post> posts = new ArrayList<>();

		BufferedReader bf = new BufferedReader(new FileReader(fileName));
		while(bf.ready()){
			String line = bf.readLine();
			String[] post = line.split(" ");
			Integer id = Integer.parseInt(post[0]);
			Integer userId = Integer.parseInt(post[1]);
			User user = Users.findById(userId, "anon");
			String content = post[2];
			posts.add(new Post(id, user, content));
		}

		return posts;
	}
}