import entities.*;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class HtmlGenerator{

	public static boolean MessagesAreGenerated = false;
	public static boolean FeedIsGenerated = false;
	
	public void generateFeed() throws IOException {

		if(!FeedIsGenerated){
			FileWriter fw = new FileWriter(Paths.feedHtmlPath);
			List<Post> posts = Posts.getInstance();
			for(Post post: posts){
				fw.write(post.toString() + "\n");
			}
			fw.close();

			FeedIsGenerated = true;
			System.out.println("Posts are generated");
		}
		else {
			System.out.println("Posts have already been generated");
		}

	}


	public void generateMessages() throws IOException {

		if(!MessagesAreGenerated){
			FileWriter fw = new FileWriter(Paths.messagesHtmlPath);
			List<Message> messages = Messages.getInstance();
			for(Message message: messages){
				fw.write(message.toString() + "\n");
			}
			fw.close();

			MessagesAreGenerated = true;
			System.out.println("Messages are generated");
		}
		else{
			System.out.println("Messages have already been generated");
		}
	}


	public void generateProfile(int id) throws IOException {

		User user = Users.findById(id, "anon");
		FileWriter fw = new FileWriter(Paths.profileHtmlPath);
		
		if(user.getName().equals("anon")){fw.write("404 FILE NOT FOUND");}
		else{fw.write(user.toString());}
		fw.close();

		System.out.println("Profile is generated");

	}	


	
}