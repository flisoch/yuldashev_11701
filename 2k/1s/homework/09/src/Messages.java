import entities.Message;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class Messages{

	private static List<Message> messages;

	public static List<Message> getInstance() throws IOException {
		if(messages == null){
			messages = Mapper.readMessages(Paths.messagesPath);
		}
		return messages;
	}
}