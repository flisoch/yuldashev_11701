import entities.User;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class Users{

	private static List<User> users;

	public static List<User> getInstance() throws IOException {
		if(users == null){
			users = Mapper.readUsers(Paths.usersPath);
		}
		return users;
	}

	public static User findById(int id, String exeptName) throws IOException {
		//Todo: find without loop
		//Todo: find without exeptName
		for(User user:getInstance()){
			if(user.getId() == id){
				return user;
			}
		}
		return new User(id,exeptName);
	}
}