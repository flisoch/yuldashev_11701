package entities;

public class Message{

	Integer id;
	User receiver;
	User sender;
	String text;

	public Message(Integer id, User receiver, User sender, String text){
		this.id = id;
		this.receiver = receiver;
		this.sender = sender;
		this.text = text;
	}

	@Override
	public String toString() {
		return "Message{" +
				"id=" + id +
				", receiver=" + receiver +
				", sender=" + sender +
				", text='" + text + '\'' +
				'}';
	}
}