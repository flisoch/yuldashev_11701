package entities;

public class Post{

	Integer id;
	User user;
	String content;

	@Override
	public String toString() {
		return "Post{" +
				"id=" + id +
				", user=" + user +
				", content='" + content + '\'' +
				'}';
	}

	public Post(Integer id, User user, String content){
		this.id = id;
		this.user = user;
		this.content = content;
	}

}