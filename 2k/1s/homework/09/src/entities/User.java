package entities;

public class User{

	private Integer id;
	private String name;

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", name='" + name + '\'' +
				'}';
	}

	public User(Integer id, String name){
		this.id = id;
		this.name = name;

	}

	public Integer getId(){
		return id;
	}
	public String getName(){ return name; }
}