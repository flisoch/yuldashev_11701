package services;

import dao.implementations.SimplePostDao;
import dao.implementations.SimpleUserDao;
import dao.interfaces.PostDao;
import dao.interfaces.UserDao;
import entities.Post;
import entities.User;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;


public class UserService {

    private static CookieService cookieService = new CookieService();
    private UserDao userDao = new SimpleUserDao();
    private PostDao postDao = new SimplePostDao();

    private final static String USER_COOKIE = "remember_me";


    public User getCurrentUser(HttpServletRequest request, HttpServletResponse response){
        User user = (User)request.getSession().getAttribute("current_user");

        if(user == null){
            user = getUserByCookie(request,response);
        }
        return user;
    }

    private User getUserByCookie(HttpServletRequest request, HttpServletResponse response) {

        User user = null;
        Cookie rememberCookie = cookieService.getCookieByName(request,USER_COOKIE);

        if(rememberCookie!= null){

            String username = rememberCookie.getValue();
            if(username != null) {

                user = userDao.getUserByName(username);
                if(user!= null){
                    authorize(request,response,user);
                }
            }
        }

        return user;
    }

    public User authenticate(HttpServletRequest request) {

        String username = request.getParameter("username");

        if(username != null) {

            String password = request.getParameter("password");
            User user = userDao.getUserByName(username);

            if (user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }


    public void authorize(HttpServletRequest request, HttpServletResponse response, User current_user) {

        request.getSession().setAttribute("current_user",current_user);

        if(request.getParameter("remember_me") != null){

            Cookie cookie = new Cookie("remember_me",  current_user.getName());
            cookie.setMaxAge(24*60*60);
            response.addCookie(cookie);}
    }


    public void publishPost(HttpServletRequest request, HttpServletResponse response) {

        String title = request.getParameter("title");
        String text = request.getParameter("text");
        Date publishTime = new Date();
        User user = getCurrentUser(request,response);
        Post post = new Post(user,title,publishTime,text);
        postDao.save(post);

    }

    public List<Post> getPosts(Integer userId){
        List<Post>posts = postDao.getPostsByUserId(userId);
        return posts;
    }
}
