package entities;

import java.util.Date;

public class Post {

    private User user;
    private String title;
    private Date publishTime;
    private String text;


    public Post(User user, String title, Date publishTime, String text) {
        this.user = user;
        this.title = title;
        this.publishTime = publishTime;
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public String getTitle() {
        return title;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "Post{" +
                "user=" + user +
                ", title='" + title + '\'' +
                ", publishTime=" + publishTime +
                ", text='" + text + '\'' +
                '}';
    }
}
