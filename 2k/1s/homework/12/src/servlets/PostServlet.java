package servlets;

import entities.User;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.Principal;
import java.sql.Time;
import java.util.Date;


public class PostServlet extends HttpServlet {

    private UserService userService = new UserService();


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        User user = userService.getCurrentUser(request,response);
        if(user == null) {
            response.sendRedirect("/login");
            return;
        }
        userService.publishPost(request,response);
        response.sendRedirect("/profile");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        User user = userService.getCurrentUser(request,response);
        if(user == null) {
            response.sendRedirect("/login");
            return;
        }

        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        writer.print("Create a post: ");
        writer.print("<form method='post' action='/post'>" +
                "<input type='text' name='title'>title" +
                "<input type='text' name='text'>text" +
                "<button type='submit'>publish</button>"
        );
        writer.close();

    }
}
