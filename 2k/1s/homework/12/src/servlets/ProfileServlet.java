package servlets;

import entities.Post;
import entities.User;
import services.CookieService;
import services.UserService;

import javax.jms.Session;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class ProfileServlet extends HttpServlet {


    private UserService userService = new UserService();

    private void printHello(User userName, PrintWriter writer) {
        writer.write("<p>Hello, " + userName + "</p>" +
                "<p>This is your profile page</p>");
    }

    private void printLogOutBtn(PrintWriter writer) {

        //print button logout
        writer.write("<div><form action='/logout' method='post'>" +
                "<button type='submit'>Logout</button></div>");
    }

    private void printToProfileBtn(PrintWriter writer) {
        writer.write("<div><form action='/profile' method='get'>" +
                "<button type='submit'>MyProfile</button></div>");
    }


    private void printPosts(Integer userId, PrintWriter writer) {
        List<Post>posts = userService.getPosts(userId);
        for(Post post:posts){
            writer.write("<p>"+post.toString()+"</p>");
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter writer = response.getWriter();

        User user = userService.getCurrentUser(request,response);
        if(user == null) {
            response.sendRedirect("/login");
        }

        else{
            String id = request.getParameter("id");
            if(id!=null){
                printPosts(Integer.parseInt(id),writer);
                printToProfileBtn(writer);
                printLogOutBtn(writer);

            }
            else{
                printHello(user,writer);
                printPosts(user.getId(),writer);
                printLogOutBtn(writer);
            }

        }

        response.setContentType("text/html");
        writer.close();

    }

}
