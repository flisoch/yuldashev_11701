package servlets;

import entities.User;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends HttpServlet {


    private static UserService userService = new UserService();


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        User current_user = userService.getCurrentUser(request,response);

        if(current_user != null) {
            response.sendRedirect("/profile");
            return;
        }

        else {

            current_user = userService.authenticate(request);
            if(current_user != null){
                userService.authorize(request,response,current_user);
                response.sendRedirect("/profile");
                return;
            }

            response.sendRedirect("/login");
        }



    }

    protected void doGet(HttpServletRequest request,
     HttpServletResponse response) throws ServletException, IOException {

        PrintWriter writer = response.getWriter();

        User user = userService.getCurrentUser(request,response);

        if(user != null){

            response.sendRedirect("/profile");
            return;

        }
        else{

            response.setContentType("text/html");

            writer.print("hello,anonimus");
            writer.print("<form method='post' action='/login'>" +
                    "<input type='text' name='username'>" +
                    "<input type='password' name='password'>" +
                    "<button type='submit'>login</button>" +
                    "<input type='checkbox' name='remember_me'>remember me"
            );
            writer.close();
        }
    }
}
