package dao.implementations;

import dao.interfaces.UserDao;
import db.UsersDataBase;
import entities.User;

import java.util.List;

public class SimpleUserDao implements UserDao {

    List<User> userList = UsersDataBase.getUsers();


    @Override
    public User getUserByName(String username) {

        for(User user: userList){
            if(user.getName().equals(username)) {
                return user;
            }
        }
        return null;
    }

    @Override
    public User getUserById(Integer id) {

        for(User user: userList){
            if(user.getId() == id) {
                return user;
            }
        }
        return null;
    }
}
