package dao.interfaces;

import entities.Post;

import java.util.List;

public interface PostDao {
    public List<Post>getPostsByUserId(Integer id);
    public void save(Post post);
}
