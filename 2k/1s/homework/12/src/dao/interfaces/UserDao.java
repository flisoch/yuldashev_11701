package dao.interfaces;

import entities.User;

public interface UserDao {

    User getUserByName(String username);
    User getUserById(Integer id);
}
