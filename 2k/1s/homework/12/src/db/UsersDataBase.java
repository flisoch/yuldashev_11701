package db;

import entities.User;

import java.util.ArrayList;
import java.util.List;

public class UsersDataBase {
    private static List<User> users;


    public static List<User> getUsers(){

        if(users == null) {

            users = new ArrayList<>();
            users.add(new User(1,"vasya","pass"));
            users.add(new User(2,"tolya","pass"));

        }
        return users;
    }
}
