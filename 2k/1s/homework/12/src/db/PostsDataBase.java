package db;

import entities.Post;
import entities.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PostsDataBase {

    private static List<Post> posts;
    private static List<User> users = UsersDataBase.getUsers();

    public static List<Post> getPosts(){

        if(posts == null) {
            posts = new ArrayList<>();
            posts.add(new Post(users.get(0),"post01",new Date(),"text01"));
            posts.add(new Post(users.get(0),"post02",new Date(),"text02"));
            posts.add(new Post(users.get(1),"post11",new Date(),"text11"));
            posts.add(new Post(users.get(1),"post12",new Date(),"text12"));
        }
        return posts;
    }
}
