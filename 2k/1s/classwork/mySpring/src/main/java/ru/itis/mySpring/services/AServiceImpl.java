package ru.itis.mySpring.services;

import lombok.NoArgsConstructor;
import ru.itis.mySpring.repositories.UserRepository;

@NoArgsConstructor
public class AServiceImpl implements AService {
    UserRepository userRepository;

    public AServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }
}
