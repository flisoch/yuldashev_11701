package ru.itis.mySpring;

import jdk.internal.org.objectweb.asm.Type;
import ru.itis.mySpring.context.ApplicationContext;
import ru.itis.mySpring.context.ApplicationContextImpl;
import ru.itis.mySpring.repositories.UserRepository;
import ru.itis.mySpring.repositories.UserRepositoryImpl;
import ru.itis.mySpring.services.AService;
import ru.itis.mySpring.services.UserService;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.TypeVariable;
import java.text.Format;

public class App {
    public static void main(String[] args) {
        ApplicationContext context = ApplicationContextImpl.getContext();


        AService aService = context.getComponent(AService.class);
        UserService userService = context.getComponent(UserService.class);
        UserRepository userRepository = context.getComponent(UserRepository.class);

    }
}
