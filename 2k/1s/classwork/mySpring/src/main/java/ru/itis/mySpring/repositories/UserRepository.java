package ru.itis.mySpring.repositories;

import javax.sql.DataSource;

public interface UserRepository extends Repository {
    DataSource getDataSource();
}
