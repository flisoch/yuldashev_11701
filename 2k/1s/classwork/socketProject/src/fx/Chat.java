package fx;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.text.Text;
import javafx.scene.control.Button;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class Chat extends Application{

    public static void main(String[] args) {

        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Stage primaryStage = new Stage();
        // Set Window's Title
        primaryStage.setTitle("JavaFX Welcome!");
        GridPane gridPane    = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        Text scenetitle = new Text("Welcome");
        scenetitle.setFont(Font.font ("Tahoma",
                FontWeight.NORMAL, 20));
        gridPane.add(scenetitle, 0, 0, 2, 1);
        Label userName = new Label( "User Name:" );
                gridPane.add(userName, 0, 1);
        TextField userTextField = new TextField();
        gridPane.add(userTextField, 1, 1);
        Label pw = new Label("Password:");
        gridPane.add(pw, 0, 2);
        PasswordField pwBox = new PasswordField();
        gridPane.add(pwBox, 1, 2);
        Button btn = new Button( "Sign in" );
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btn);
        gridPane.add(hbBtn, 1, 4);
        final Text actiontarget = new Text();
        gridPane.add(actiontarget, 1, 6);
        btn.setOnAction( new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
            }
        });
        Scene scene = new Scene(gridPane, 300, 275);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}