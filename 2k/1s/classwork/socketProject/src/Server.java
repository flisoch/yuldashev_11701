import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {

    private static int PORT = 3456;
    private  static List<Socket> socketList = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        ServerSocket s = new ServerSocket(PORT);

        while(true){
            acceptClient(s);

        }

    }



    void sendToAll(String message){

        for (Socket socket: socketList){
            try {
                PrintWriter writer = new PrintWriter(socket.getOutputStream());
                writer.println(message);
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static void acceptClient(ServerSocket serverSocket){
        Socket client = null;
        try {
            client = serverSocket.accept();
            socketList.add(client);
            System.out.println("accepted!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
