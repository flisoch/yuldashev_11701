import java.sql.*;

public class Main {

    private static final String URL = "jdbc:postgresql://localhost:5432/abrWork";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "postgres";

    public static void main(String[] args) throws SQLException {
        Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM subject");
        while (resultSet.next()){
            System.out.println(resultSet.getString("title"));
        }

        //statement.execute("INSERT INTO subject(title)VALUES ('inf2'),('inf3')");

    }
}
