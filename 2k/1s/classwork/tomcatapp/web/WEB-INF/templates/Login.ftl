<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${title}</title>
</head>
<body>

    <ul>
        <#list users as user>
            <li>${user}</li>
        </#list>
    </ul>

    <form method='post' action='/login'>
        <input type='text' name='username'>username
        <div>
            <input type='password' name='password'>password
        </div>
        <button type='submit'>login</button>
        <input type='checkbox' name='remember_me'>remember me
</body>
</html>