
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile</title>
</head>
<body>

    <#if username?? >
        <p>Hello, ${username}
            <br>This is your profile page
        </p>

    <#else>
        <div>
            <form action='/profile' method='get'>
            <button type='submit'>MyProfile</button>
        </div>
    </#if>


    <div>
        <form action='/logout' method='post'>
            <button type='submit'>Logout</button>
    </div>


    <#if posts?has_content>
        <#list posts as post>
            <li>
                <a href="/post/${post.id}">${post.title}</a>
            </li>
        </#list>
    </#if>


</body>
</html>
