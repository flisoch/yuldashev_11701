package services;

import config.ConnectionSingleton;
import dao.implementations.collectionDao.SimplePostDao;
import dao.implementations.dbDao.SimpleDbPostDao;
import dao.interfaces.PostDao;
import entities.Post;
import entities.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class PostService{

    private Connection connection = ConnectionSingleton.getInstance();
    private UserService userService = new UserService();
    private PostDao postDao = new SimpleDbPostDao(connection);

    public void publishPost(HttpServletRequest request, HttpServletResponse response) throws SQLException {

        String title = request.getParameter("title");
        String text = request.getParameter("text");
        Date publishTime = new Date();
        User user = userService.getCurrentUser(request,response);
        Post post = new Post(user,title,publishTime,text);
        postDao.save(post);

    }

    public List<Post> getPosts(Integer userId) throws SQLException {
        return postDao.getPostsByUserId(userId);
    }

    public Post getPost(Integer postId) throws SQLException {
        return postDao.getPostById(postId);
    }
    public void deletePost(Integer id) throws SQLException {
        postDao.deleteById(id);
    }

    public void updatePost(Integer id, User user,HttpServletRequest request) throws SQLException {
        String title = request.getParameter("title");
        String text = request.getParameter("text");
        if(text == null){
            text = " ";
        }
        if(title == null){
            title = " ";
        }
        Post post = new Post(id,user,
                title,
                new Date(),
                text);
        postDao.update(post);
    }

}
