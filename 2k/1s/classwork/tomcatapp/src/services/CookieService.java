package services;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public class CookieService {


    public Cookie getCookieByName(HttpServletRequest request, String cookieName){

        try{
            for (Cookie cookie : request.getCookies()) {

                if (cookie.getName().equals(cookieName)) {
                    return cookie;
                }
            }
        }catch (Exception e){
            return null;
        }
        return null;
    }

    public boolean hasCookie(HttpServletRequest request, String cookieName){
        for (Cookie cookie : request.getCookies()) {

            if (cookie.getName().equals(cookieName)) {
                return true;
            }
        }
        return false;
    }

}
