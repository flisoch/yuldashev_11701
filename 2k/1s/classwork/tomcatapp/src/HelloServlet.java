import java.io.IOException;
import java.io.PrintWriter;

public class HelloServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {


        PrintWriter writer = response.getWriter();
        String name = request.getParameter("name");

        if(name != null && !name.equals("")){
            writer.print(name);
        }
        else{
            writer.print("hello,anonimus");
            writer.print("<form method=\"get\" action=\"/hello\">" +
                    "<input type=\"text\" name=\"name\">"+
                    "<input type=\"submit\">");
        }



        response.setContentType("text/html");
        writer.close();


    }
}
