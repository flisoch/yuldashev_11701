package entities;

public class User {

    private Integer id;

    private String name;

    private String password;
    public User(String name, String password) {

        this.name = name;
        this.password = password;
    }

    public User(Integer id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public String getName(){ return name; }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
