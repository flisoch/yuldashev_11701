package servlets;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogoutServlet extends HttpServlet {

    private void deleteRememberMeCookie(HttpServletRequest request, HttpServletResponse response) {

        for (Cookie cookie : request.getCookies()) {

            if (cookie.getName().equals("remember_me")) {

                cookie.setMaxAge(0);
                response.addCookie(cookie);
            }
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getSession().invalidate();
        deleteRememberMeCookie(request,response);

        response.sendRedirect("/login");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
