package servlets;

import config.ConfigSingleton;
import entities.Post;
import entities.User;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import services.CookieService;
import services.PostService;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileServlet extends HttpServlet {


    private UserService userService = new UserService();
    private PostService postService = new PostService();


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        User user = null;
        try {
            user = userService.getCurrentUser(request,response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(user == null) {
            response.sendRedirect("/login");
            return;
        }
        else {
            String path = request.getPathInfo();
            if(path!=null){

                Integer id = Integer.parseInt(path.split("/")[1]);
                Map parameterMap = request.getParameterMap();
                if(parameterMap.isEmpty()){
                    try {
                        userService.deleteProfile(id);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    try {
                        userService.updateProfile(id,parameterMap);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter writer = response.getWriter();

        User user = null;
        try {
            user = userService.getCurrentUser(request,response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(user == null) {
            response.sendRedirect("/login");
        }

        else{

            Configuration cfg = ConfigSingleton.getConfig(getServletContext());
            Template tmpl = cfg.getTemplate("Profile.ftl");
            HashMap<String, Object> root = new HashMap<>();

            String path = request.getPathInfo();
            Integer userId = null;

            if(path!=null){
                try {
                    userId = Integer.parseInt(path.split("/")[1]);
                }catch (Exception e){
                    System.out.println(e);
                }
            }
            else{
                userId = user.getId();
                root.put("username", user.getName());
            }
            try {
                root.put("posts", postService.getPosts(userId));
                tmpl.process(root,writer);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (TemplateException e) {
                e.printStackTrace();
            }finally {
                writer.close();
            }
        }
    }
}
