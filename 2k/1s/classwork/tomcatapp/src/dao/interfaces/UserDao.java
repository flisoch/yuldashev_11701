package dao.interfaces;

import entities.User;

import java.sql.SQLException;
import java.util.List;

public interface UserDao {

    User getUserByName(String username) throws SQLException;
    User getUserById(Integer id) throws SQLException;
    void update(User user) throws SQLException;
    void deleteById(Integer id) throws SQLException;

    List<User> getUsers() throws SQLException;
}
