package dao.interfaces;

import entities.Post;

import java.sql.SQLException;
import java.util.List;

public interface PostDao {
    List<Post>getPostsByUserId(Integer id) throws SQLException;
    void save(Post post) throws SQLException;
    void update(Post post) throws SQLException;
    void deleteById(Integer id) throws SQLException;

    Post getPostById(Integer postId) throws SQLException;
}
