package dao.implementations.dbDao;

import dao.interfaces.UserDao;
import entities.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SimpleDbUserDao implements UserDao {

    private Connection connection;

    private static final String SQL_SELECT_BY_USERNAME = "SELECT * FROM profile WHERE username = ?;";
    private static final String SQL_SELECT_BY_ID = "SELECT * FROM profile WHERE profile_id = ?;";
    private static final String SQL_UPDATE_QUERY = "UPDATE profile SET username = ?, password = ? WHERE id = ?;";
    private static final String SQL_DELETE_QUERY = "DELETE FROM profile WHERE id = ?;";


    public SimpleDbUserDao(Connection connection){
        this.connection = connection;
    }

    @Override
    public User getUserByName(String username) throws SQLException {
        User user = null;
        PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_USERNAME);
        statement.setString(1,username);
        ResultSet resultSet = statement.executeQuery();
        System.out.println(username);
        if(resultSet.next()){
            System.out.println("inside");
            user = new User(resultSet.getInt("profile_id"),
                    resultSet.getString("username"),
                    resultSet.getString("password"));
        }

        return user;
    }

    @Override
    public User getUserById(Integer id) throws SQLException {
        User user;
        PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID);
        statement.setInt(1,id);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        user = new User(resultSet.getInt("profile_id"),
                resultSet.getString("username"),
                resultSet.getString("password"));
        return user;

    }

    @Override
    public void update(User user) throws SQLException {

        PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_QUERY);
        statement.setString(1,user.getName());
        statement.setString(2,user.getPassword());
        statement.setInt(3,user.getId());
        ResultSet resultSet = statement.executeQuery();
    }

    @Override
    public void deleteById(Integer id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SQL_DELETE_QUERY);
        statement.setInt(1,id);
        ResultSet resultSet = statement.executeQuery();
    }

    @Override
    public List<User> getUsers() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM profile");
        List<User> users = new ArrayList<>();
        while (resultSet.next()){
            users.add(new User(resultSet.getInt("profile_id"),
                    resultSet.getString("username"),
                    resultSet.getString("password")));
        }

        return users;
    }
}
