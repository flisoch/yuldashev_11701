package dao.implementations.dbDao;

import dao.interfaces.PostDao;
import entities.Post;
import entities.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SimpleDbPostDao implements PostDao {
    private Connection connection;

    private static final String SQL_SELECT_BY_USER_ID = "SELECT * FROM post p JOIN profile pr on " +
            "p.profile_id_fk = pr.profile_id WHERE p.profile_id_fk = ?;";
    private static final String SQL_SELECT_POST_BY_USER_AND_POST_ID = "SELECT * FROM post p JOIN profile pr on " +
            "p.profile_id_fk = pr.profile_id WHERE p.profile_id_fk = ? AND p.post_id = ?;";
    private static final String SQL_SELECT_BY_ID = "SELECT * FROM post p JOIN profile pr on " +
            "p.profile_id_fk = pr.profile_id WHERE p.post_id = ?";
    private static final String SQL_UPDATE_QUERY = "UPDATE post SET title = ?, publish_time = ?, text = ? WHERE post_id = ?;";
    private static final String SQL_DELETE_QUERY = "DELETE FROM post WHERE post_id = ?;";
    private static final String SQL_INSERT_QUERY = "INSERT INTO post(profile_id_fk, title, text, publish_time) VALUES (?,?,?,?);";


    public SimpleDbPostDao(Connection connection){
        this.connection = connection;
    }


    @Override
    public List<Post> getPostsByUserId(Integer id) throws SQLException {

        PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_USER_ID);
        statement.setInt(1,id);
        ResultSet resultSet = statement.executeQuery();

        List<Post> posts = new ArrayList<>();

        while (resultSet.next()){

            User user = new User(resultSet.getInt("profile_id"),
                    resultSet.getString("username"),
                    resultSet.getString("password"));
            Post post = new Post(resultSet.getInt("post_id"),
                    user,
                    resultSet.getString("title"),
                    resultSet.getDate("publish_time"),
                    resultSet.getString("text"));
            posts.add(post);

        }
        return posts;
    }

    @Override
    public void save(Post post) throws SQLException {
        System.out.println("to db:  "+post.getPublishTime());
        PreparedStatement statement = connection.prepareStatement(SQL_INSERT_QUERY);
        statement.setInt(1,post.getUser().getId());
        statement.setString(2,post.getTitle());
        statement.setString(3,post.getText());
        statement.setTimestamp(4,new Timestamp(post.getPublishTime().getTime()));
        statement.execute();

    }

    @Override
    public void update(Post post) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_QUERY);
        statement.setString(1,post.getTitle());
        statement.setTimestamp(2,new Timestamp(post.getPublishTime().getTime()));
        statement.setString(3,post.getText());
        statement.setInt(4,post.getId());
        statement.execute();
    }

    @Override
    public void deleteById(Integer id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SQL_DELETE_QUERY);
        statement.setInt(1,id);
        statement.execute();
    }

    @Override
    public Post getPostById(Integer postId) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID);
        statement.setInt(1,postId);
        ResultSet resultSet = statement.executeQuery();

        resultSet.next();

        User user = new User(resultSet.getInt("profile_id"),
                resultSet.getString("username"),
                resultSet.getString("password"));
        Post post = new Post(resultSet.getInt("post_id"),
                user,
                resultSet.getString("title"),
                resultSet.getDate("publish_time"),
                resultSet.getString("text"));
        System.out.println(post);

        return post;
    }
}
