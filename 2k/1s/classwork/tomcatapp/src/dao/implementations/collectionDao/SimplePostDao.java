package dao.implementations.collectionDao;

import dao.interfaces.PostDao;
import db.PostsDataBase;
import entities.Post;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SimplePostDao implements PostDao {

    List<Post> posts = PostsDataBase.getPosts();

    @Override
    public List<Post> getPostsByUserId(Integer id) {
        List<Post> userPosts = new ArrayList<>();
        for(Post post: posts){
            if(post.getUser().getId() == id){
                userPosts.add(post);
            }
        }
        return userPosts;
    }

    @Override
    public void save(Post post) {
        posts.add(post);
    }

    @Override
    public void update(Post post) {

    }

    @Override
    public void deleteById(Integer id) {

    }

    @Override
    public Post getPostById(Integer postId) throws SQLException {
        return null;
    }
}
