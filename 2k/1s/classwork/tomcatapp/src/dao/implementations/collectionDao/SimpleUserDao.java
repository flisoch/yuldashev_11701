package dao.implementations.collectionDao;

import dao.interfaces.UserDao;
import db.UsersDataBase;
import entities.User;

import java.sql.SQLException;
import java.util.List;

public class SimpleUserDao implements UserDao {

    List<User> userList = UsersDataBase.getUsers();


    @Override
    public User getUserByName(String username) {

        for(User user: userList){
            if(user.getName().equals(username)) {
                return user;
            }
        }
        return null;
    }

    @Override
    public User getUserById(Integer id) {

        for(User user: userList){
            if(user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    @Override
    public void update(User user) {
    }

    @Override
    public void deleteById(Integer id) {

    }

    @Override
    public List<User> getUsers() throws SQLException {
        return null;
    }
}
