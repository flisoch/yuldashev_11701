package ru.itis.infa._022819mvc.services;

import ru.itis.infa._022819mvc.models.Post;

import java.util.List;

public interface PostsService {
    List<Post> getAll();

    void save(String text);
}
