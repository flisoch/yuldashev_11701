package ru.itis.infa._022819mvc.services;

import org.springframework.beans.factory.annotation.Autowired;
import ru.itis.infa._022819mvc.models.Post;
import ru.itis.infa._022819mvc.repositories.PostRepository;

import java.util.List;

public class PostServiceImpl implements PostsService {

    @Autowired
    PostRepository postRepository;

    @Override
    public List<Post> getAll() {
        return postRepository.getAll();
    }

    @Override
    public void save(String text) {
        postRepository.save(new Post(text));
    }
}
