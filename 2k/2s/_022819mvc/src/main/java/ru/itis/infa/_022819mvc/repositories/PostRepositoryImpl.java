package ru.itis.infa._022819mvc.repositories;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.itis.infa._022819mvc.models.Post;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class PostRepositoryImpl implements PostRepository {

    DataSource dataSource;
    JdbcTemplate jdbcTemplate;

    private static final String SELECT_ALL_POSTS = "SELECT * FROM post";
    private static final String INSERT_POST = "INSERT INTO post(text) VALUES(?)";

    @Autowired
    public PostRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    RowMapper<Post> postRowMapper = (resultSet, i) -> Post.builder().text(resultSet.getString("text")).build();


    @Override
    public void save(Post post) {
        jdbcTemplate.update(INSERT_POST, post.getText());
    }

    @Override
    public List<Post> getAll() {
        return jdbcTemplate.query(SELECT_ALL_POSTS, postRowMapper);
    }
}
