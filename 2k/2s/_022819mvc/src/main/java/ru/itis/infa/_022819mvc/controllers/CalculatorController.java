package ru.itis.infa._022819mvc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itis.infa._022819mvc.services.CalculatorService;
import ru.itis.infa._022819mvc.services.CalculatorServiceImpl;

@Controller
@RequestMapping("/calc")
public class CalculatorController {

    @Autowired
    CalculatorService calculatorService;

    @RequestMapping(method = RequestMethod.GET)
    public String calculate(ModelMap model,
                            @RequestParam(required = false) String operator,
                            @RequestParam(required = false, name="first") String firstOperand,
                            @RequestParam(required = false, name="second")String secondOperand,
                            @RequestParam(name = "result", defaultValue = "") String result){
        if(operator != null){
            calculatorService.calculate(model,operator,firstOperand,secondOperand);
        }
        return "calculator";
    }

}
