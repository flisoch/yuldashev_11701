package ru.itis.infa._022819mvc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itis.infa._022819mvc.services.PostsService;

@Controller
@RequestMapping("/posts")
public class PostController {

    @Autowired
    PostsService postsService;

    @RequestMapping(method = RequestMethod.GET)
    String posts(ModelMap model){

        model.put("posts", postsService.getAll());
        return "posts";
    }

    @RequestMapping(method = RequestMethod.POST)
    String savePost(@RequestParam String text){
        postsService.save(text);
        return  "redirect:/posts";
    }
}
