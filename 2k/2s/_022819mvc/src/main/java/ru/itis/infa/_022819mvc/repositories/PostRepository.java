package ru.itis.infa._022819mvc.repositories;

import ru.itis.infa._022819mvc.models.Post;

import java.util.List;

public interface PostRepository {
    void save(Post post);
    List<Post> getAll();
}
