package ru.itis.infa._022819mvc.services;

import org.springframework.ui.ModelMap;

public interface CalculatorService {
    boolean calculate(ModelMap model, String operator, String firstOperand, String secondOperand);
}
