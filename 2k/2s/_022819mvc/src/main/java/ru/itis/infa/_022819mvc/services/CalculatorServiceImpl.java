package ru.itis.infa._022819mvc.services;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import java.util.HashMap;

@Service
public class CalculatorServiceImpl implements CalculatorService{

    public boolean calculate(ModelMap model, String operator, String firstOperand, String secondOperand) {
        boolean calculated = false;
        Double first = Double.parseDouble(firstOperand);
        Double second = Double.parseDouble(secondOperand);
        Double result = null;
        switch (operator) {
            case "+":
                result = first + second;
                calculated = true;
                break;

            case "-":
                result = first - second;
                calculated = true;
                break;

            case "*":
                result = first * second;
                calculated = true;
                break;

            case "/":
                result = first / second;
                calculated = true;
                break;
        }
        model.put("result", result);
        return calculated;
    }
}
