package ru.itis.infa._022819mvc.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class Post {
    //Todo: date
    String text;
}
