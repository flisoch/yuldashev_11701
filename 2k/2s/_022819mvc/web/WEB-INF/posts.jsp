<%--
  Created by IntelliJ IDEA.
  User: flisoch
  Date: 28.02.19
  Time: 17:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Posts</title>
</head>

<body>
    <c:forEach var = "post" items="${posts}">
        <div>
            <p>${post}</p>
        </div>
    </c:forEach>

    <div>
        <form method="post">
            <textarea name="text"></textarea>
            <button type="submit">publish</button>
        </form>
    </div>
</body>
</html>