package ru.itis.infa._022019.dao.postgres;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import ru.itis.infa._022019.dao.UserDao;
import ru.itis.infa._022019.entities.User;

import javax.sql.DataSource;
import java.util.List;


public class UserDaoImp implements UserDao {
    private static final String SELECT_USER_QUERY = "SELECT * FROM \"user\" WHERE id = ?";
    private static final String SELECT_ALL = "SELECT * FROM \"user\"";
    private JdbcTemplate jdbcTemplate;

    public UserDaoImp(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private RowMapper<User> userRowMapper = (resultSet, i) -> User.builder()
            .id(resultSet.getInt("id"))
            .username(resultSet.getString("username"))
            .build();

    @Override
    public User getUser(int id) {
        return jdbcTemplate.queryForObject(SELECT_USER_QUERY, userRowMapper, id);
    }

    @Override
    public List<User> getAll() {
        return jdbcTemplate.query(SELECT_ALL, userRowMapper);
    }
}
