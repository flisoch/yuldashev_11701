package ru.itis.infa._022019.views;

import com.google.gson.Gson;
import ru.itis.infa._022019.entities.User;

import java.util.List;

public class ViewJsonUserImpl implements View<User> {

    @Override
    public String show(List users) {
        return new Gson().toJson(users);
    }

    @Override
    public String show(User user) {
        return new Gson().toJson(user);
    }
}
