package ru.itis.infa._022019.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import ru.itis.infa._022019.entities.Developer;

@Aspect
public class LogWork {
    // Developer здесь из произвольного пакета
// Параметр JoinPoint – вся информация о вызванном методе
    @Before("execution(* *..Developer.work())")
    public void loggingWork(JoinPoint jp){
        System.out.println(((Developer) jp.getThis()).getName()
                + " is ready to work");
    }
}
