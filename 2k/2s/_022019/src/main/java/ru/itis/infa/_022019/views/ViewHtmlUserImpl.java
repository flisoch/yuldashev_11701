package ru.itis.infa._022019.views;

import ru.itis.infa._022019.entities.User;

import java.util.List;


public class ViewHtmlUserImpl implements View<User> {
    @Override
    public String show(User user) {
        return "<h1>" + user.getUsername() + "</h1>";
    }

    @Override
    public String show(List<User> users) {
        final String[] table = {"<table>"};
        users.forEach(user -> table[0] = table[0].concat("<tr>").concat(((User)user).getUsername()).concat("<tr>"));
        return table[0];
    }
}
