package ru.itis.infa._022019;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.itis.infa._022019.handlers.CommandHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;



public class Application {
    public static void main(String[] args) throws IOException {

        ApplicationContext ac = new ClassPathXmlApplicationContext("application-config.xml");
        CommandHandler handler = ac.getBean(CommandHandler.class);

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String userInput = reader.readLine();
        while (!"exit".equals(userInput)) {
            System.out.println(handler.respond(userInput));
            userInput = reader.readLine();
        }
    }
}
