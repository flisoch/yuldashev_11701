package ru.itis.infa._022019.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class AuthAspect {

    @Around("execution(* ru.itis.infa._022019.handlers.*.*(..))")
    private Object authorize(ProceedingJoinPoint joinPoint) throws Throwable {
        if (!isAllowed()) {
            System.out.println("Forbidden call: " + joinPoint.getSignature());
            return null;
        } else {
            System.out.println("Allowed call: " + joinPoint.getSignature());
            return joinPoint.proceed(joinPoint.getArgs());
        }
    }

    private boolean isAllowed() {
        return true;
    }
}
