package ru.itis.infa._022019.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import java.security.Timestamp;
import java.util.Date;

@Aspect
public class LoggerAspect {

    @Before("execution(* ru.itis.infa._022019.*.*.*(..))")
    public void log(JoinPoint joinPoint){
        System.out.println(new Date(System.currentTimeMillis()) +"\nMETHOD CALLED: " + joinPoint.getSignature() +"\n");
    }
}
