package ru.itis.infa._022019.dao;

import ru.itis.infa._022019.entities.User;

public abstract class RowMapper<T> {
    public abstract User rowMap(String userInFile);
}
