package ru.itis.infa._022019.dao.fileDb;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import ru.itis.infa._022019.dao.RowMapper;
import ru.itis.infa._022019.dao.UserDao;
import ru.itis.infa._022019.entities.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class UserDaoImp implements UserDao {

    private String fileName;

    private RowMapper<User> userRowMapper = new RowMapper<User>() {
        @Override
        public User rowMap(String userRowInFile) {
            String[] attrs = userRowInFile.split(",");

            return User.builder()
                    .id(Integer.parseInt(attrs[0]))
                    .username(attrs[1])
                    .build();
        }
    };

    public UserDaoImp(String fileName) {
        this.fileName = fileName;
    }

    @Override
    @SneakyThrows
    public User getUser(int userId) {
        User user = null;
        String userRowInFile = findInFile(userId);
        if(userRowInFile != null){
            user = userRowMapper.rowMap(userRowInFile);
        }
        return user;
    }

    @SneakyThrows
    private String findInFile(int userId) {
        BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
        String userRow = reader.readLine();
        while (userRow != null) {
            String id = userRow.split(",")[0];
            if (userId == Integer.parseInt(id)) {
                return userRow;
            }
            userRow = reader.readLine();
        }
        return null;
    }

    @Override
    @SneakyThrows
    public List<User> getAll() {
        List<User> users = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
        String userRow = reader.readLine();
        while (userRow != null) {
            users.add(userRowMapper.rowMap(userRow));
            userRow = reader.readLine();
        }
        return users;
    }
}
