package ru.itis.infa._022019.dao;

import ru.itis.infa._022019.entities.User;

import java.util.List;

public interface UserDao {
    User getUser(int id);
    List<User> getAll();
}
