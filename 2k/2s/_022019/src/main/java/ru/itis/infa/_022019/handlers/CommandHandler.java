package ru.itis.infa._022019.handlers;

public interface CommandHandler {
    String respond(String command);
}
