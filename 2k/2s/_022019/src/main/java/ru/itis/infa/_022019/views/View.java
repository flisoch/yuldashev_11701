package ru.itis.infa._022019.views;

import java.util.List;

public interface View<T> {
    String show(T object);
    String show(List<T> objects);
}
