package ru.itis.infa._022019.entities;

public class Developer {
    public String getName() {
        return name;
    }

    private String name;

    public Developer(String name) {
        this.name = name;
    }

    public void work() {
        System.out.println("I am working");
    }

    public void learnLanguage(String language) {
        System.out.println("I am learning language " + language);
    }
}