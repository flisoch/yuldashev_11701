package ru.itis.infa._022019.handlers;

import javafx.util.Pair;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis.infa._022019.dao.UserDao;
import ru.itis.infa._022019.entities.User;
import ru.itis.infa._022019.views.View;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Component
public class CommandHandlerImpl implements CommandHandler {
    private List<Pair<Pattern, Method>> commandPatterns;
    private final UserDao userDao;
    private final View<User> view;

    @Autowired
    @SneakyThrows
    public CommandHandlerImpl(UserDao userDao, View<User> view) {
        this.userDao = userDao;
        this.view = view;
    }

    @Override
    @SneakyThrows
    public String respond(String command) {


        String[] split = command.split("/");
        if (split.length == 2) {
            return view.show(
                    userDao.getAll()
            );
        } else {
            int id = Integer.parseInt(split[2]);
            return view.show(
                    userDao.getUser(id)
            );
        }
    }

}
