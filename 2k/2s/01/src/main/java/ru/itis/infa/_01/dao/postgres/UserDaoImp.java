package ru.itis.infa._01.dao.postgres;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import ru.itis.infa._01.dao.UserDao;
import ru.itis.infa._01.entities.User;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

public class UserDaoImp implements UserDao {
    private static final String SELECT_USER_QUERY = "SELECT * FROM user WHERE id = ?";
    private static final String SQL_SELECT_ALL = "SELECT * FROM user";
    JdbcTemplate jdbcTemplate;

    public UserDaoImp(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private RowMapper<User> userRowMapper = (resultSet, i) -> User.builder()
            .id(resultSet.getLong("id"))
            .name(resultSet.getString("name"))
            .city(resultSet.getString("city"))
            .birthYear(resultSet.getInt("bithYear"))
            .build();

    @Override
    public Optional<User> getUser(long id) {
        return Optional.of(jdbcTemplate.queryForObject(SELECT_USER_QUERY, userRowMapper,id));
    }

    @Override
    public List<User> getUsers() {
        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
    }
}
