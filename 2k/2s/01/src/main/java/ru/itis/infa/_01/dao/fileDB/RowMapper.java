package ru.itis.infa._01.dao.fileDB;

public abstract class RowMapper<T> {
    abstract T rowMap(String userFileRepresentation);
}
