package ru.itis.infa._01.view;

import ru.itis.infa._01.entities.User;

import java.util.List;

public interface Representer<T> {
    String convertObject(T object);
    String convertObjects(List<T> objects);
}
