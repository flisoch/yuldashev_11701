package ru.itis.infa._01.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.itis.infa._01.dao.UserDao;
import ru.itis.infa._01.entities.User;
import ru.itis.infa._01.view.Representer;

import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("config.xml");
        Representer representer = context.getBean(Representer.class);
        UserDao userDao = (UserDao) context.getBean("UserDao");


        User user1 = User.builder().id(1).name("Tolya").city("Kazan").birthYear(1999).build();
        User user2 = User.builder().id(2).name("Petya").city("Kazan").birthYear(1999).build();
        //write users, then read from dao. but 'write' functionality isn't written yet xd
        //and we just put the into new list :D
        List<User> users = new LinkedList<>();
        users.add(user1);
        users.add(user2);

        // there we read users and have list not just created but read from dao
        List<User> dbUsers = userDao.getUsers();

        //common users vs converted users
        dbUsers.forEach(System.out::println);
        System.out.println(representer.convertObjects(users));

        System.out.println(userDao.getUser(1).get().getName());
        System.out.println(representer.convertObject(user2));
    }
}
