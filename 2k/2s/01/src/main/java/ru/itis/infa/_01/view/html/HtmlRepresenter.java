package ru.itis.infa._01.view.html;


import ru.itis.infa._01.entities.User;
import ru.itis.infa._01.view.Representer;

import java.util.List;

public class HtmlRepresenter implements Representer<User> {

    @Override
    public String convertObject(User user) {
        return "<h1>" + user.getName() + "</h1>";

    }

    @Override
    public String convertObjects(List users) {
        final String[] table = {"<table>"};
        users.forEach(user -> table[0] = table[0].concat("<tr>").concat(((User)user).getName()).concat("<tr>"));
        return table[0];
    }
}
