package ru.itis.infa._01.dao;

import ru.itis.infa._01.entities.User;

import java.util.List;
import java.util.Optional;

public interface UserDao {

    Optional<User> getUser(long id);

    List<User> getUsers();
}
