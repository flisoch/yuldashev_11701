package ru.itis.infa._01.dao.fileDB;

import ru.itis.infa._01.dao.UserDao;
import ru.itis.infa._01.entities.User;

import java.io.*;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserDaoImp implements UserDao {
    String dbFileName;
    BufferedReader reader;
    Writer writer;

    private RowMapper<User> userRowMapper = new RowMapper<User>() {
        @Override
        User rowMap(String userInFile) {
            String[] attrs = userInFile.split(",");

            return User.builder()
                    .id(Long.parseLong(attrs[0]))
                    .name(attrs[1])
                    .city(attrs[2])
                    .birthYear(Integer.parseInt(attrs[3]))
                    .build();
        }
    };

    public UserDaoImp(String dbFileName) {
        this.dbFileName = dbFileName;
    }

    @Override
    public Optional<User> getUser(long id) {
        try {
            reader = new BufferedReader(new FileReader(new File(dbFileName)));
            String userRow = findUserInFile(reader, id);
            if (userRow != null) {
                return Optional.of(userRowMapper.rowMap(userRow));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public List<User> getUsers() {
        ArrayList users = new ArrayList();
        try {
            reader = new BufferedReader(new FileReader(new File(dbFileName)));
            String userRow = reader.readLine();
            while (userRow != null) {
                users.add(Optional.of(userRowMapper.rowMap(userRow)));
                userRow = reader.readLine();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return users;
    }

    private String findUserInFile(BufferedReader reader, long id) throws IOException {
        String userRow = reader.readLine();
        while (userRow != null) {
            String[] attrs = userRow.split(",");
            if (Integer.parseInt(attrs[0]) == id) {
                return userRow;
            }
            userRow = reader.readLine();
        }
        return null;
    }
}
