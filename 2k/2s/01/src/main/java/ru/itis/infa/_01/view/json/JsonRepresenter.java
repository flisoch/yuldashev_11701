package ru.itis.infa._01.view.json;

import com.google.gson.Gson;
import ru.itis.infa._01.entities.User;
import ru.itis.infa._01.view.Representer;

import java.util.List;

public class JsonRepresenter implements Representer<User> {

    @Override
    public String convertObjects(List users) {
        return new Gson().toJson(users);
    }

    @Override
    public String convertObject(User user) {
        return new Gson().toJson(user);
    }
}
