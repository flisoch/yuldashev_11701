package ru.itis.infa._01.entities;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    long id;
    String name;
    String city;
    int birthYear;
}
