package ru.itis.infa._02.entities;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@Builder
public class Message {
    String text;
    Date date;
    User user;
}
