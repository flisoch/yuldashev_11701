package ru.itis.infa._02;

import ru.itis.infa._02.entities.Message;
import ru.itis.infa._02.entities.User;
import ru.itis.infa._02.converter.Converter;
import ru.itis.infa._02.converter.html.HtmlConverter;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Converter converter = new HtmlConverter();
        User user = User.builder()
                .name("vasya")
                .city("kazan")
                .build();
        Message message = Message.builder()
                .date(new Date(System.currentTimeMillis()))
                .text("asd")
                .user(user)
                .build();

        String htmlUser = (String) converter.build(user);
        String htmlMessage = (String) converter.build(message);

        System.out.println((htmlUser));
        System.out.println(htmlMessage);
    }
}
