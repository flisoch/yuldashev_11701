package ru.itis.infa._02.converter.html;

import ru.itis.infa._02.converter.Converter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class HtmlConverter implements Converter {

    public Object build(Object object) {
        final String[] htmlConvertedObject = {"<table><tr>"};

        Class objectClass = object.getClass();
        Object[] getters = getGetters(objectClass);

        Arrays.stream(getters).forEach(getter -> {
            try {
                Object attr = ((Method)getter).invoke(object);
                htmlConvertedObject[0] = htmlConvertedObject[0].concat(wrapWithTag(attr, "td"));
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        });
        htmlConvertedObject[0] = htmlConvertedObject[0].concat("</tr></table>");
        return htmlConvertedObject[0];
    }

    private Object[] getGetters(Class objectClass) {
        Method[] methods = objectClass.getDeclaredMethods();
        Object[] getters =  Arrays.stream(methods)
                .filter(method -> method.getName().startsWith("get"))  //not only this condition, but doesn't matter
                .toArray();
        return getters;
    }

    private String wrapWithTag(Object attr, String tag) {
        return "<" + tag + ">" + attr.toString() + "</" + tag + ">";
    }

}
