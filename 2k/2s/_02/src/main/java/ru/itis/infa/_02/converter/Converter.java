package ru.itis.infa._02.converter;

public interface Converter {
    Object build(Object object);
}
