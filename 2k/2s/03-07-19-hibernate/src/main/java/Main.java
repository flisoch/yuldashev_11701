import models.Mail;
import models.Mailbox;
import org.hibernate.*;
import org.hibernate.query.Query;
import org.hibernate.cfg.Configuration;

import javax.persistence.metamodel.EntityType;

import java.util.List;
import java.util.Map;
import java.util.Random;

public class Main {
    private static final SessionFactory ourSessionFactory;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            ourSessionFactory = configuration.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    public static void main(final String[] args) throws Exception {

        Session session = getSession();

        session.beginTransaction();
        Mailbox mailbox1 = session.load(Mailbox.class, 1L);
        Mailbox mailbox2 = session.load(Mailbox.class, 2L);
        Mail newMail = Mail.builder().from(mailbox1).to(mailbox2).subject("asd").text("asdasdasdasd").build();

        List<Mail> emails = session.createQuery("from Mail mail", Mail.class).getResultList();
        System.out.println("count before insert :" + emails.size() + "\n");
        session.save(newMail);
        session.getTransaction().commit();

        session.beginTransaction();
        emails = session.createQuery("from Mail mail", Mail.class).getResultList();
        System.out.println("count after insert:" + emails.size() + "\n");
        session.delete(newMail);
        session.getTransaction().commit();

        emails = session.createQuery("from Mail mail", Mail.class).getResultList();
        System.out.println("count after delete:" + emails.size() + "\n");

        System.out.println("mailbox N1 address before update:" + mailbox1.getEmailAddress());
        mailbox1.setEmailAddress(new Random().nextInt() + "vasya@mail.ru");
        System.out.println("mailbox N1 address after update: " + session.load(Mailbox.class, 1L).getEmailAddress());


    }

}