package models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "mailbox")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "inbox")
public class Mailbox {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "email_address")
    private String emailAddress;

    @OneToMany
    @JoinTable(name = "mail")
    private List<Mail> inbox;

}
