package models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "mail")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Mail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "subject")
    private String subject;
    @ManyToOne
    @JoinColumn(name = "from_mailbox_id")
    private Mailbox from;
    @ManyToOne
    @JoinColumn(name = "to_mailbox_id")
    private Mailbox to;
    @Column(name = "text")
    private String text;

}
