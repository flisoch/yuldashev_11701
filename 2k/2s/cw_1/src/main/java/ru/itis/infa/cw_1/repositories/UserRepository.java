package ru.itis.infa.cw_1.repositories;

import ru.itis.infa.cw_1.models.User;

import java.util.List;

public interface UserRepository {

    void create(User user);

    List<User> getAll();
}
