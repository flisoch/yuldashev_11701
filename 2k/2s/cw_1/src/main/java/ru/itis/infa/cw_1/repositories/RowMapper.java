package ru.itis.infa.cw_1.repositories;

public abstract class RowMapper<T> {
    abstract T rowMap(String userFileRepresentation);
}
