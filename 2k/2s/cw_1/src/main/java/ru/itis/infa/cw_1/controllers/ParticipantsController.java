package ru.itis.infa.cw_1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itis.infa.cw_1.services.UserService;

@Controller
@RequestMapping("/participants")
public class ParticipantsController {

    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    String showParticipants(ModelMap model){

        model.put("participants", userService.getAll());
        return  "participants";
    }
}
