package ru.itis.infa.cw_1.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class User {
    String fio;
    String city;
    String email;
}
