package ru.itis.infa.cw_1.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import java.util.Date;

@Aspect
public class LoggerAspect {

    @Before("execution(* ru.itis.infa.cw_1.controllers.*.*.*(..))")
    public void log(JoinPoint joinPoint){
        System.out.println(new Date(System.currentTimeMillis()) +"\nPAGE VISITED: " + joinPoint.getSourceLocation().getFileName() +"\n");
    }
}

