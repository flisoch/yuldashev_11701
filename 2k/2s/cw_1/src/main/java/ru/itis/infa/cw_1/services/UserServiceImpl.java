package ru.itis.infa.cw_1.services;

import org.springframework.beans.factory.annotation.Autowired;
import ru.itis.infa.cw_1.models.User;
import ru.itis.infa.cw_1.repositories.UserRepository;

import java.util.List;

public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public void register(String fio, String city, String email) {
        User new_user = User.builder().fio(fio).city(city).email(email).build();
        userRepository.create(new_user);

    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }
}
