package ru.itis.infa.cw_1.repositories;

import ru.itis.infa.cw_1.models.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {

    private String fileName;
    BufferedReader reader;
    PrintWriter writer;

    private RowMapper<User> userRowMapper = new RowMapper<User>() {
        @Override
        User rowMap(String userInFile) {
            String[] attrs = userInFile.split(",");

            return User.builder()
                    .fio(attrs[1])
                    .city(attrs[2])
                    .email(attrs[3])
                    .build();
        }
    };

    public UserRepositoryImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void create(User user) {
        try {
            writer = new PrintWriter(new FileOutputStream(new File(fileName)));
            writer.println(user.getFio() + "," + user.getCity() + "," + user.getEmail());
            writer.flush();
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> getAll() {

        ArrayList users = new ArrayList();

        try {
            reader = new BufferedReader(new FileReader(new File(fileName)));
            String userRow = reader.readLine();
            while (userRow != null) {
                users.add(userRowMapper.rowMap(userRow));
                userRow = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return users;
    }
}
