package ru.itis.infa.cw_1.services;

import ru.itis.infa.cw_1.models.User;

import java.util.List;

public interface UserService {

    void register(String fio, String city, String email);

    List<User> getAll();
}
