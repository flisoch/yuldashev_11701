package ru.itis.infa.cw_1.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itis.infa.cw_1.services.UserService;

@Controller
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    String getRegisterForm(){
        return  "register";
    }

    @RequestMapping(method = RequestMethod.POST)
    String savePost(@RequestParam String fio,
                    @RequestParam String city,
                    @RequestParam String email){
        userService.register(fio, city, email);
        return  "redirect:/participants";
    }

}
