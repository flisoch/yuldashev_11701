<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Posts</title>
</head>

<body>
<c:forEach var="participant" items="${participants}">
    <div>
        <p>${participant}</p>
    </div>
</c:forEach>

</body>
</html>